﻿using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.Views.HistoryViews;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BlackJack.Web.Controllers
{
    public class HistoryController : Controller
    {
        private IHistoryService _historyService;
        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAllGames()
        {
            GetAllGamesHistoryView allGames = await _historyService.GetAllGames();
            return View(allGames);
        }

        public async Task<ActionResult> GameDetails(int id)
        {
            GameDetailsHistoryView gameDetails = await _historyService.GameDetails(id);
            return View(gameDetails);
        }
    }
}