﻿using System.Threading.Tasks;
using System.Web.Mvc;
using BlackJack.ViewModels.Views.GameViews;
using System;
using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Services.Interfaces;

namespace BlackJack.Web.Controllers
{
    public class GameController : Controller
    {
        private IGameService _gameService;
        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        public async Task<ActionResult> StartGame()
        {
            StartGameGameView startGame = await _gameService.StartGame();
            return View(startGame);
        }

        [HttpPost]
        public async Task<ActionResult> CreateGame(RequestCreateGameGameView requestData)
        {
            if (ModelState.IsValid)
            {
                ResponseCreateGameGameView responseData = await _gameService.CreateGame(requestData);
                if (responseData.GameId != 0)
                {
                    return RedirectToAction("CurrentGame", new { id = responseData.GameId, param = GetActionInGame.FirstRound });
                }
            }
            return RedirectToAction("StartGame", "Game");
        }

        public async Task<ActionResult> CurrentGame(int id, GetActionInGame param)
        {
            ModelState.Clear();

            CurrentGameGameView round = await _gameService.CurrentGame(id, param);

            if ((round.IsDealerTakes && (param == GetActionInGame.FirstRound || param == GetActionInGame.NextRound)) || !round.IsDealerTakes && param == GetActionInGame.DealerTakes)
            {
                round = await _gameService.CurrentGame(id, GetActionInGame.DealerTakes);
                return View("_GameEnd", round);
            }

            if (!round.IsDealerTakes && param == GetActionInGame.FirstRound)
            {
                return View(round);
            }

            if (!round.IsDealerTakes && param == GetActionInGame.NextRound)
            {
                return PartialView(round);
            }

            return View("Error");
        }
    }
}