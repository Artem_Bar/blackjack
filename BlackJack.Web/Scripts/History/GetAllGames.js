﻿$("#grid").kendoGrid({
    dataSource: data,
    columns: [
        {
            field: "GameId",
            title: "Game #",
            width: "100px"
        },

        {
            field: "Date",
            title: "Date",
            type: "date",
            format: "{0: MMMM/dd/yyyy}",
            width: "200px"


        },
        {
            field: "Date",
            title: "Time",
            type: "date",
            format: "{0: H:mm}",
            width: "100px"

        },
        { command: { text: "Open", click: showDetails }, title: "Details", width: "120px" }
    ],
    scrollable: true
});


function showDetails(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    id = dataItem.GameId;
    window.open("GameDetails/" + id);
};
