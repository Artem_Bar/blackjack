﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BlackJack.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{param}",
                defaults: new { controller = "Game", action = "StartGame", id = UrlParameter.Optional, param = UrlParameter.Optional }
            );
        }
    }
}
