﻿using BlackJack.ViewModels.Enums;
using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.Views.HistoryViews
{
    public class GetAllGamesHistoryView
    {
        public List<GameGetAllGamesHistoryViewItem> Games { get; set; }
        public GetAllGamesHistoryView()
        {
            Games = new List<GameGetAllGamesHistoryViewItem>();
        }
    }

    public class GameGetAllGamesHistoryViewItem
    {
        public int GameId { get; set; }
        public DateTime Date { get; set; }
        public GameStateEnumView State { get; set; }
    }
}
