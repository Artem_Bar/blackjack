﻿using BlackJack.ViewModels.Enums;
using System.Collections.Generic;

namespace BlackJack.ViewModels.Views.HistoryViews
{
    public class GameDetailsHistoryView
    {
        public int GameId { get; set; }
        public List<PlayerGameDetailsHistoryViewItem> Players { get; set; }
        public GameDetailsHistoryView()
        {
            Players = new List<PlayerGameDetailsHistoryViewItem>();
        }
    }

    public class PlayerGameDetailsHistoryViewItem
    {
        public string Name { get; set; }
        public PlayerRoleEnumView Role { get; set; }
        public PlayerStateEnumView State { get; set; }
        public List<CardGameDetailsHistoryViewItem> Cards { get; set; }
        public int TotalPoints { get; set; }
    }

    public class CardGameDetailsHistoryViewItem
    {
        public CardValueEnumView Value { get; set; }
        public CardSuitEnumView Suit { get; set; }
        public int Points { get; set; }
    }
}
