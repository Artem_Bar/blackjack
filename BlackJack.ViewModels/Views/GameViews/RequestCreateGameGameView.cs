﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.Views.GameViews
{
    public class RequestCreateGameGameView
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Amount { get; set; }
    }
}
