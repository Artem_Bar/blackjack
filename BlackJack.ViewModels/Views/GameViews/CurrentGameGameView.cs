﻿using BlackJack.ViewModels.Enums;
using System.Collections.Generic;

namespace BlackJack.ViewModels.Views.GameViews
{
    public class CurrentGameGameView
    {
        public int GameId { get; set; }
        public bool IsDealerTakes { get; set; }
        public List<PlayerCurrentGameGameViewItem> Players { get; set; }
        public CurrentGameGameView()
        {
            Players = new List<PlayerCurrentGameGameViewItem>();
        }
    }

    public class PlayerCurrentGameGameViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PlayerRoleEnumView Role { get; set; }
        public PlayerStateEnumView State { get; set; }
        public List<CardCurrentGameGameViewItem> Cards { get; set; }
        public int TotalPoints { get; set; }
        public PlayerCurrentGameGameViewItem()
        {
            Cards = new List<CardCurrentGameGameViewItem>();
        }
    }

    public class CardCurrentGameGameViewItem
    {
        public CardValueEnumView Value { get; set; }
        public CardSuitEnumView Suit { get; set; }
        public int Points { get; set; }
    }
}

