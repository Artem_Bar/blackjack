﻿using System.Collections.Generic;

namespace BlackJack.ViewModels.Views.GameViews
{
    public class StartGameGameView
    {
        public List<string> Names { get; set; }

        public StartGameGameView()
        {
            Names = new List<string>();
        }
    }
}
