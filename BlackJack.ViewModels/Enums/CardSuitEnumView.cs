﻿namespace BlackJack.ViewModels.Enums
{
    public enum CardSuitEnumView
    {
        Diamonds = 0,
        Hearts = 1,
        Spades = 2,
        Clubs = 3
    }
}
