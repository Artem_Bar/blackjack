﻿namespace BlackJack.ViewModels.Enums
{
    public enum PlayerRoleEnumView
    {
        Bot = 0,
        Player = 1,
        Dealer = 2,
    }
}
