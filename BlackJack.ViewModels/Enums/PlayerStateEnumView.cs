﻿namespace BlackJack.ViewModels.Enums
{
    public enum PlayerStateEnumView
    {
        InGame = 0,
        Win = 1,
        Loss = 2,
        Draw = 3,
        BlackJack = 4
    }
}
