﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using BlackJack.Entities.Entities;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly string _connection;

        public GameRepository(string connection)
        {
            _connection = connection;
        }

        public async Task<Game> Create(Game game)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                int? gameId = await db.InsertAsync(game);
                game.Id = gameId ?? (int)gameId;
            }
            return game;
        }

        public async Task<Game> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return await db.QueryFirstOrDefaultAsync<Game>("SELECT*FROM Game WHERE Id=@Id", new { id });
            }
        }

        public async Task<List<Game>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return (await db.QueryAsync<Game>("SELECT*FROM Game")).ToList();
            }
        }

        public async Task Update(Game game)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.UpdateAsync(game);
            }
        }
    }
}
