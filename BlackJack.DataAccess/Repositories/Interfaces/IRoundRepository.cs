﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IRoundRepository
    {
        Task<List<Round>> GetRoundsByGameId(int gameId);

        Task Create(Round round);

        Task Create(List<Round> rounds);
    }
}
