﻿using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IPlayerInGameRepository
    {
        Task<List<PlayerInGame>> GetPlayersByGameId(int gameId);

        Task<List<PlayerInGame>> GetPlayersByRole(int gameId, PlayerRole role);

        Task<PlayerInGame> GetPlayerById(int gameId, int playerId);

        Task<int> GetPlayerTotalPoints(int gameId, PlayerRole role);

        Task<List<Player>> GetPlayerForNextRound(int gameId, PlayerState state);

        Task Create(PlayerInGame player);

        Task Create(List<PlayerInGame> players);

        Task Update(PlayerInGame player);

        Task Update(List<PlayerInGame> players);
    }
}
