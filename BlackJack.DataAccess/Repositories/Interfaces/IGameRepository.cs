﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IGameRepository
    {
        Task<List<Game>> GetAll();

        Task<Game> Get(int id);

        Task<Game> Create(Game game);

        Task Update(Game game);
    }
}
