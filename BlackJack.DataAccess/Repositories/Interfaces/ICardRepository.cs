﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface ICardRepository
    {
        Task<Card> Get(int id);

        Task<Card> GetAnyCard();

        Task Create(List<Card> cards);

        Task<int> Count();
    }
}
