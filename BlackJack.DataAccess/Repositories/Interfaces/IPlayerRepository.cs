﻿using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        Task<List<Player>> GetAll();

        Task<List<Player>> GetPlayersByRole(PlayerRole role);

        Task <Player> GetPlayerByName(string name);

        Task<Player> Get(int id);

        Task<Player> Create(Player player);

        Task Create(List<Player> players);

        Task<int> Count();
    }
}
