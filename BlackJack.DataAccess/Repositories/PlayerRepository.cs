﻿using System.Collections.Generic;
using System.Linq;
using BlackJack.Entities.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using BlackJack.Entities.Enums;
using BlackJack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly string _connection;

        public PlayerRepository(string connection)
        {
            _connection = connection;
        }

        public async Task Create(List<Player> players)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(players);
            }
        }

        public async Task<Player> Create(Player player)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                int? playerId = await db.InsertAsync(player);
                player.Id = playerId ?? (int)playerId;
            }
            return player;
        }

        public async Task<List<Player>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = "SELECT*FROM Player";
                return (await db.QueryAsync<Player>(query)).ToList();
            }
        }

        public async Task<List<Player>> GetPlayersByRole(PlayerRole role)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = "SELECT*FROM Player WHERE Role=@role";
                return (await db.QueryAsync<Player>(query, new { role })).ToList();
            }
        }

        public async Task<Player> GetPlayerByName(string name)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT*FROM Player
                            WHERE Name=@name";
                return (await db.QueryAsync<Player>(query, new { name })).FirstOrDefault();
            }
        }

        public async Task<Player> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = "SELECT*FROM Player WHERE id=@id";
                return await db.QueryFirstOrDefaultAsync<Player>(query, new { id });
            }
        }

        public async Task<int> Count()
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return await db.ExecuteScalarAsync<int>("SELECT COUNT(*)FROM Player");
            }
        }

    }
}
