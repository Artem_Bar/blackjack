﻿using System.Collections.Generic;
using System.Data;
using BlackJack.Entities.Entities;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly string _connection;

        public CardRepository(string connection)
        {
            _connection = connection;
        }

        public async Task Create(List<Card> cards)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(cards);
            }
        }

        public async Task<int> Count()
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return await db.ExecuteScalarAsync<int>("SELECT COUNT(*) FROM Card");
            }
        }

        public async Task<Card> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return await db.QueryFirstOrDefaultAsync<Card>("SELECT*FROM Card WHERE id=@id", new { id });
            }
        }

        public async Task<Card> GetAnyCard()
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                return (await db.QueryAsync<Card>("SELECT TOP 1 * FROM Card ORDER BY NEWID()")).FirstOrDefault();
            }
        }

    }
}
