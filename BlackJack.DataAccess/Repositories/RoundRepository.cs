﻿using System.Collections.Generic;
using System.Linq;
using BlackJack.Entities.Entities;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class RoundRepository : IRoundRepository
    {
        private readonly string _connection;

        public RoundRepository(string connection)
        {
            _connection = connection;
        }

        public async Task Create(Round round)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(round);
            }
        }

        public async Task Create(List<Round> rounds)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(rounds);
            }
        }

        public async Task<List<Round>> GetRoundsByGameId(int gameId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM Round  
                    JOIN Game ON Game.Id=Round.GameId 
                    JOIN Player ON Player.Id=Round.PlayerId 
                    JOIN Card ON Card.Id=Round.CardId
                    WHERE Round.gameId=@gameId";
                var data = await db.QueryAsync<Round, Game, Player, Card, Round>(query,
                       (round, game, player, card) =>
                       {
                           round.Game = game;
                           round.Player = player;
                           round.Card = card;
                           return round;
                       }, new { gameId });
                return data.ToList();
            }
        }
    }
}
