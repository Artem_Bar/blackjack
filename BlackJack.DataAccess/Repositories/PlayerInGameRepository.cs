﻿using System.Collections.Generic;
using System.Linq;
using BlackJack.Entities.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using BlackJack.Entities.Enums;
using BlackJack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class PlayerInGameRepository : IPlayerInGameRepository
    {
        private readonly string _connection;

        public PlayerInGameRepository(string connection)
        {
            _connection = connection;
        }

        public async Task Create(PlayerInGame playerInGame)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(playerInGame);
            }
        }

        public async Task Create(List<PlayerInGame> players)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.InsertAsync(players);
            }
        }

        public async Task<List<PlayerInGame>> GetPlayersByGameId(int gameId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM PlayerInGame  
                    JOIN Game ON Game.Id=PlayerInGame.GameId
                    JOIN Player ON Player.Id=PlayerInGame.PlayerId 
                    WHERE PlayerInGame.GameId=@gameId";
                var data = await db.QueryAsync<PlayerInGame, Game, Player, PlayerInGame>(query,
                       (playerInGame, game, player) =>
                       {
                           playerInGame.Game = game;
                           playerInGame.Player = player;
                           return playerInGame;
                       }, new { gameId });
                return data.ToList();
            }
        }

        public async Task<List<PlayerInGame>> GetPlayersByRole(int gameId, PlayerRole role)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM PlayerInGame  
                    JOIN Game ON Game.Id=PlayerInGame.GameId
                    JOIN Player ON Player.Id=PlayerInGame.PlayerId 
                    WHERE PlayerInGame.GameId=@gameId AND Player.Role=@playerRole";
                var data = await db.QueryAsync<PlayerInGame, Game, Player, PlayerInGame>(query,
                       (playerInGame, game, player) =>
                       {
                           playerInGame.Game = game;
                           playerInGame.Player = player;
                           return playerInGame;
                       }, new { gameId, role });
                return data.ToList();
            }
        }

        public async Task<PlayerInGame> GetPlayerById(int gameId, int playerId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM PlayerInGame  
                    JOIN Game ON Game.Id=PlayerInGame.GameId
                    JOIN Player ON Player.Id=PlayerInGame.PlayerId 
                    WHERE PlayerInGame.GameId=@gameId AND PlayerInGame.PlayerId=@playerId";
                var data = await db.QueryAsync<PlayerInGame, Game, Player, PlayerInGame>(query,
                       (playerInGame, game, player) =>
                       {
                           playerInGame.Game = game;
                           playerInGame.Player = player;
                           return playerInGame;
                       },
                       new { gameId, playerId });
                return data.FirstOrDefault();
            }
        }

        public async Task<int> GetPlayerTotalPoints(int gameId, PlayerRole role)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM PlayerInGame  
                    JOIN Game ON Game.Id=PlayerInGame.GameId
                    JOIN Player ON Player.Id=PlayerInGame.PlayerId 
                    WHERE PlayerInGame.GameId=@gameId AND Player.Role=@role";
                var data = await db.QueryAsync<PlayerInGame, Game, Player, PlayerInGame>(query,
                       (playerInGame, game, player) =>
                       {
                           playerInGame.Game = game;
                           playerInGame.Player = player;
                           return playerInGame;
                       },
                       new { gameId, role });
                return data.FirstOrDefault().TotalPoints;
            }
        }

        public async Task<List<Player>> GetPlayerForNextRound(int gameId, PlayerState state)
        {
            PlayerRole user = PlayerRole.Player;
            PlayerRole bot = PlayerRole.Bot;

            using (IDbConnection db = new SqlConnection(_connection))
            {
                var query = @"SELECT *  
                    FROM PlayerInGame  
                    JOIN Game ON Game.Id=PlayerInGame.GameId
                    JOIN Player ON Player.Id=PlayerInGame.PlayerId 
                    WHERE PlayerInGame.GameId=@gameId AND PlayerInGame.State=@state AND Player.Role IN(@user,@bot)";
                var data = await db.QueryAsync<PlayerInGame, Player, Player>(query,
                       (playerInGame, player) =>
                       {
                           playerInGame.Player = player;
                           return playerInGame.Player;
                       },
                       new { gameId, state, user, bot });
                return data.ToList();
            }
        }

        public async Task Update(PlayerInGame playerInGame)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.UpdateAsync(playerInGame);
            }
        }

        public async Task Update(List<PlayerInGame> players)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                await db.UpdateAsync(players);
            }
        }

    }
}
