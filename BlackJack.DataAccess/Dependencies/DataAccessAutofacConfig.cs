﻿using Autofac;
using BlackJack.DataAccess.Repositories;
using BlackJack.DataAccess.Repositories.Interfaces;

namespace BlackJack.DataAccess.Dependencies
{
    public class DataAccessAutofacConfig
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connection)
        {
            builder.RegisterType<GameRepository>().As<IGameRepository>().WithParameter("connection", connection);
            builder.RegisterType<RoundRepository>().As<IRoundRepository>().WithParameter("connection", connection);
            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>().WithParameter("connection", connection);
            builder.RegisterType<CardRepository>().As<ICardRepository>().WithParameter("connection", connection);
            builder.RegisterType<PlayerInGameRepository>().As<IPlayerInGameRepository>().WithParameter("connection", connection);
        }
    }
}