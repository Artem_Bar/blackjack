(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-start-game-start-game-module"],{

/***/ "./src/app/shared/models/game/request-create-game-game.view.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/models/game/request-create-game-game.view.ts ***!
  \*********************************************************************/
/*! exports provided: RequestCreateGameGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestCreateGameGameView", function() { return RequestCreateGameGameView; });
var RequestCreateGameGameView = /** @class */ (function () {
    function RequestCreateGameGameView() {
    }
    return RequestCreateGameGameView;
}());



/***/ }),

/***/ "./src/app/shared/models/game/start-game-game-view.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/models/game/start-game-game-view.ts ***!
  \************************************************************/
/*! exports provided: StartGameGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameGameView", function() { return StartGameGameView; });
var StartGameGameView = /** @class */ (function () {
    function StartGameGameView() {
        this.names = new Array();
    }
    return StartGameGameView;
}());



/***/ }),

/***/ "./src/app/start-game/start-game-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/start-game/start-game-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: StartGameRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameRoutingModule", function() { return StartGameRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_start_game_start_game_start_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/start-game/start-game/start-game.component */ "./src/app/start-game/start-game/start-game.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: src_app_start_game_start_game_start_game_component__WEBPACK_IMPORTED_MODULE_2__["StartGameComponent"]
    }
];
var StartGameRoutingModule = /** @class */ (function () {
    function StartGameRoutingModule() {
    }
    StartGameRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], StartGameRoutingModule);
    return StartGameRoutingModule;
}());



/***/ }),

/***/ "./src/app/start-game/start-game.module.ts":
/*!*************************************************!*\
  !*** ./src/app/start-game/start-game.module.ts ***!
  \*************************************************/
/*! exports provided: StartGameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameModule", function() { return StartGameModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js");
/* harmony import */ var src_app_start_game_start_game_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/start-game/start-game-routing.module */ "./src/app/start-game/start-game-routing.module.ts");
/* harmony import */ var src_app_start_game_start_game_start_game_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/start-game/start-game/start-game.component */ "./src/app/start-game/start-game/start-game.component.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var StartGameModule = /** @class */ (function () {
    function StartGameModule() {
    }
    StartGameModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                src_app_start_game_start_game_routing_module__WEBPACK_IMPORTED_MODULE_2__["StartGameRoutingModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_1__["DropDownsModule"],
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            declarations: [src_app_start_game_start_game_start_game_component__WEBPACK_IMPORTED_MODULE_3__["StartGameComponent"]]
        })
    ], StartGameModule);
    return StartGameModule;
}());



/***/ }),

/***/ "./src/app/start-game/start-game/start-game.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/start-game/start-game/start-game.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"parent\">\r\n    <div class=\"row\">\r\n        <h4>Enter Player</h4>\r\n        <kendo-combobox [data]=\"players.names\" [(ngModel)]=\"data.name\"></kendo-combobox>\r\n    </div>\r\n\r\n    <div class=\" row\">\r\n        <br />\r\n        <h4>Enter Quantity</h4>\r\n        <kendo-dropdownlist [data]=\"quantityBots\" [(ngModel)]=\"data.amount\"></kendo-dropdownlist>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <br />\r\n        <button kendoButton (click)=\"onClick()\">Execute</button>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/start-game/start-game/start-game.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/start-game/start-game/start-game.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  width: 175px;\n  height: 40px;\n  font-size: large; }\n\n.parent {\n  text-align: center;\n  margin-top: 100px;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  overflow: auto; }\n\nh4 {\n  font-size: x-large; }\n"

/***/ }),

/***/ "./src/app/start-game/start-game/start-game.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/start-game/start-game/start-game.component.ts ***!
  \***************************************************************/
/*! exports provided: StartGameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameComponent", function() { return StartGameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var src_app_shared_models_game_start_game_game_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/models/game/start-game-game-view */ "./src/app/shared/models/game/start-game-game-view.ts");
/* harmony import */ var src_app_shared_models_game_request_create_game_game_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/models/game/request-create-game-game.view */ "./src/app/shared/models/game/request-create-game-game.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StartGameComponent = /** @class */ (function () {
    function StartGameComponent(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.quantityBots = [1, 2, 3, 4, 5];
        this.players = new src_app_shared_models_game_start_game_game_view__WEBPACK_IMPORTED_MODULE_3__["StartGameGameView"]();
        this.data = new src_app_shared_models_game_request_create_game_game_view__WEBPACK_IMPORTED_MODULE_4__["RequestCreateGameGameView"]();
    }
    StartGameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.gameService.startGame().subscribe(function (p) {
            _this.players.names = p.names;
        });
    };
    StartGameComponent.prototype.onClick = function () {
        var _this = this;
        this.gameService.createGame(this.data).subscribe(function (result) {
            _this.router.navigate(["/currentGame", result.gameId]);
        });
    };
    StartGameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-start',
            template: __webpack_require__(/*! ./start-game.component.html */ "./src/app/start-game/start-game/start-game.component.html"),
            styles: [__webpack_require__(/*! ./start-game.component.scss */ "./src/app/start-game/start-game/start-game.component.scss")],
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], StartGameComponent);
    return StartGameComponent;
}());



/***/ })

}]);
//# sourceMappingURL=src-app-start-game-start-game-module.js.map