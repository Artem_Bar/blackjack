(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/shared/helpers/conversions.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/helpers/conversions.ts ***!
  \***********************************************/
/*! exports provided: ConversionHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConversionHelper", function() { return ConversionHelper; });
/* harmony import */ var src_app_shared_models_enums_get_action_in_game__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/shared/models/enums/get-action-in-game */ "./src/app/shared/models/enums/get-action-in-game.ts");
/* harmony import */ var src_app_shared_models_enums_card_suit_enum_view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/shared/models/enums/card-suit-enum.view */ "./src/app/shared/models/enums/card-suit-enum.view.ts");
/* harmony import */ var src_app_shared_models_enums_card_value_enum_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/models/enums/card-value-enum.view */ "./src/app/shared/models/enums/card-value-enum.view.ts");
/* harmony import */ var src_app_shared_models_enums_player_role_enum_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/models/enums/player-role-enum.view */ "./src/app/shared/models/enums/player-role-enum.view.ts");
/* harmony import */ var src_app_shared_models_enums_player_state_enum_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/models/enums/player-state-enum.view */ "./src/app/shared/models/enums/player-state-enum.view.ts");





var ConversionHelper = /** @class */ (function () {
    function ConversionHelper() {
    }
    ConversionHelper.prototype.getPlayerRole = function (item) {
        return src_app_shared_models_enums_player_role_enum_view__WEBPACK_IMPORTED_MODULE_3__["PlayerRoleEnumView"][item];
    };
    ConversionHelper.prototype.getPlayerState = function (item) {
        return src_app_shared_models_enums_player_state_enum_view__WEBPACK_IMPORTED_MODULE_4__["PlayerStateEnumView"][item];
    };
    ConversionHelper.prototype.getCardValue = function (item) {
        return src_app_shared_models_enums_card_value_enum_view__WEBPACK_IMPORTED_MODULE_2__["CardValueEnumView"][item];
    };
    ConversionHelper.prototype.getCardSuit = function (item) {
        return src_app_shared_models_enums_card_suit_enum_view__WEBPACK_IMPORTED_MODULE_1__["CardSuitEnumView"][item];
    };
    ConversionHelper.prototype.getActionInGame = function (item) {
        return src_app_shared_models_enums_get_action_in_game__WEBPACK_IMPORTED_MODULE_0__["GetActionInGame"][item];
    };
    return ConversionHelper;
}());



/***/ }),

/***/ "./src/app/shared/models/enums/card-suit-enum.view.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/models/enums/card-suit-enum.view.ts ***!
  \************************************************************/
/*! exports provided: CardSuitEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardSuitEnumView", function() { return CardSuitEnumView; });
var CardSuitEnumView;
(function (CardSuitEnumView) {
    CardSuitEnumView[CardSuitEnumView["diamonds"] = 0] = "diamonds";
    CardSuitEnumView[CardSuitEnumView["hearts"] = 1] = "hearts";
    CardSuitEnumView[CardSuitEnumView["spades"] = 2] = "spades";
    CardSuitEnumView[CardSuitEnumView["clubs"] = 3] = "clubs";
})(CardSuitEnumView || (CardSuitEnumView = {}));


/***/ }),

/***/ "./src/app/shared/models/enums/card-value-enum.view.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/models/enums/card-value-enum.view.ts ***!
  \*************************************************************/
/*! exports provided: CardValueEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardValueEnumView", function() { return CardValueEnumView; });
var CardValueEnumView;
(function (CardValueEnumView) {
    CardValueEnumView[CardValueEnumView["two"] = 0] = "two";
    CardValueEnumView[CardValueEnumView["three"] = 1] = "three";
    CardValueEnumView[CardValueEnumView["four"] = 2] = "four";
    CardValueEnumView[CardValueEnumView["five"] = 3] = "five";
    CardValueEnumView[CardValueEnumView["six"] = 4] = "six";
    CardValueEnumView[CardValueEnumView["seven"] = 5] = "seven";
    CardValueEnumView[CardValueEnumView["eight"] = 6] = "eight";
    CardValueEnumView[CardValueEnumView["nine"] = 7] = "nine";
    CardValueEnumView[CardValueEnumView["ten"] = 8] = "ten";
    CardValueEnumView[CardValueEnumView["jack"] = 9] = "jack";
    CardValueEnumView[CardValueEnumView["queen"] = 10] = "queen";
    CardValueEnumView[CardValueEnumView["king"] = 11] = "king";
    CardValueEnumView[CardValueEnumView["ace"] = 12] = "ace";
})(CardValueEnumView || (CardValueEnumView = {}));


/***/ }),

/***/ "./src/app/shared/models/enums/get-action-in-game.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/models/enums/get-action-in-game.ts ***!
  \***********************************************************/
/*! exports provided: GetActionInGame */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetActionInGame", function() { return GetActionInGame; });
var GetActionInGame;
(function (GetActionInGame) {
    GetActionInGame[GetActionInGame["firstRound"] = 0] = "firstRound";
    GetActionInGame[GetActionInGame["nextRound"] = 1] = "nextRound";
    GetActionInGame[GetActionInGame["dealerTakes"] = 2] = "dealerTakes";
})(GetActionInGame || (GetActionInGame = {}));


/***/ }),

/***/ "./src/app/shared/models/enums/player-role-enum.view.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/models/enums/player-role-enum.view.ts ***!
  \**************************************************************/
/*! exports provided: PlayerRoleEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerRoleEnumView", function() { return PlayerRoleEnumView; });
var PlayerRoleEnumView;
(function (PlayerRoleEnumView) {
    PlayerRoleEnumView[PlayerRoleEnumView["bot"] = 0] = "bot";
    PlayerRoleEnumView[PlayerRoleEnumView["player"] = 1] = "player";
    PlayerRoleEnumView[PlayerRoleEnumView["dealer"] = 2] = "dealer";
})(PlayerRoleEnumView || (PlayerRoleEnumView = {}));


/***/ }),

/***/ "./src/app/shared/models/enums/player-state-enum.view.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/models/enums/player-state-enum.view.ts ***!
  \***************************************************************/
/*! exports provided: PlayerStateEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerStateEnumView", function() { return PlayerStateEnumView; });
var PlayerStateEnumView;
(function (PlayerStateEnumView) {
    PlayerStateEnumView[PlayerStateEnumView["inGame"] = 0] = "inGame";
    PlayerStateEnumView[PlayerStateEnumView["win"] = 1] = "win";
    PlayerStateEnumView[PlayerStateEnumView["loss"] = 2] = "loss";
    PlayerStateEnumView[PlayerStateEnumView["draw"] = 3] = "draw";
    PlayerStateEnumView[PlayerStateEnumView["blackJack"] = 4] = "blackJack";
})(PlayerStateEnumView || (PlayerStateEnumView = {}));


/***/ })

}]);
//# sourceMappingURL=common.js.map