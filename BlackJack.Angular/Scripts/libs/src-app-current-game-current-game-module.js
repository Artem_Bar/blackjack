(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-current-game-current-game-module"],{

/***/ "./src/app/current-game/current-game-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/current-game/current-game-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: CurrentGameRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameRoutingModule", function() { return CurrentGameRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_current_game_current_game_current_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/current-game/current-game/current-game.component */ "./src/app/current-game/current-game/current-game.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: src_app_current_game_current_game_current_game_component__WEBPACK_IMPORTED_MODULE_2__["CurrentGameComponent"]
    }
];
var CurrentGameRoutingModule = /** @class */ (function () {
    function CurrentGameRoutingModule() {
    }
    CurrentGameRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CurrentGameRoutingModule);
    return CurrentGameRoutingModule;
}());



/***/ }),

/***/ "./src/app/current-game/current-game.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/current-game/current-game.module.ts ***!
  \*****************************************************/
/*! exports provided: CurrentGameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameModule", function() { return CurrentGameModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _current_game_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./current-game-routing.module */ "./src/app/current-game/current-game-routing.module.ts");
/* harmony import */ var src_app_current_game_current_game_current_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/current-game/current-game/current-game.component */ "./src/app/current-game/current-game/current-game.component.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CurrentGameModule = /** @class */ (function () {
    function CurrentGameModule() {
    }
    CurrentGameModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _current_game_routing_module__WEBPACK_IMPORTED_MODULE_1__["CurrentGameRoutingModule"],
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [src_app_current_game_current_game_current_game_component__WEBPACK_IMPORTED_MODULE_2__["CurrentGameComponent"]]
        })
    ], CurrentGameModule);
    return CurrentGameModule;
}());



/***/ }),

/***/ "./src/app/current-game/current-game/current-game.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/current-game/current-game/current-game.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent\">\r\n    <table id=\"main\" class=\"table table-bordered table-hover\" border=\"1\">\r\n        <caption *ngIf=\"!isDealerTake\">Current Game</caption>\r\n        <caption *ngIf=\"isDealerTake\">Game End</caption>\r\n        <thead>\r\n            <tr>\r\n                <th>Name</th>\r\n                <th>Role</th>\r\n                <th *ngIf=\"isDealerTake\">State</th>\r\n                <th>Cards</th>\r\n                <th>Points</th>\r\n                <th>Total points</th>\r\n            </tr>\r\n        </thead>\r\n        <tr *ngFor=\"let player of currentGame.players\">\r\n            <td>{{(player.name)}}</td>\r\n            <td *ngIf=\"player.role!=2 && isDealerTake\"> {{conversion.getPlayerRole(player.role)}}</td>\r\n            <td *ngIf=\"player.role!=2 && isDealerTake\"> {{conversion.getPlayerState(player.state)}}</td>\r\n            <td *ngIf=\"player.role==2 && isDealerTake\" colspan=\"2\">{{conversion.getPlayerRole(player.role)}}</td>\r\n            <td *ngIf=\"!isDealerTake\">{{conversion.getPlayerRole(player.role)}}</td>\r\n            <td>\r\n                <table align=\"center\">\r\n                    <tr *ngFor=\"let card of player.cards\">\r\n                        <td>{{conversion.getCardValue(card.value)}}  {{conversion.getCardSuit(card.suit)}}</td>\r\n                    </tr>\r\n                </table>\r\n            </td>\r\n            <td>\r\n                <table align=\"center\">\r\n                    <tr *ngFor=\"let card of player.cards\">\r\n                        <td>{{card.points}}</td>\r\n                    </tr>\r\n                </table>\r\n            </td>\r\n            <td>{{player.totalPoints}}</td>\r\n        </tr>\r\n    </table>\r\n\r\n\r\n    <div class=\"row\">\r\n        <br />\r\n        <br />\r\n        <div class=\"col-md-4 col-md-push-3\" *ngIf=\"!isDealerTake\">\r\n            <button id=\"kendoButtonTake\" kendoButton (click)=\"getRound(1)\">Take</button>\r\n        </div>\r\n        <div class=\"col-md-4 col-md-push-4\" *ngIf=\"!isDealerTake\">\r\n            <button id=\"kendoButtonEnought\" kendoButton (click)=\"getGameEnd()\">Enought</button>\r\n        </div>\r\n\r\n        <div class=\"col-md-4 col-md-push-3\" *ngIf=\"isDealerTake\">\r\n            <button id=\"kendoButtonNewGame\" kendoButton (click)=\"onClickToNewGame()\">New Game</button>\r\n        </div>\r\n        <div class=\"col-md-4 col-md-push-4\" *ngIf=\"isDealerTake\">\r\n            <button id=\"kendoButtonHistory\" kendoButton (click)=\"onClickToHistory()\">History</button>\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/current-game/current-game/current-game.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/current-game/current-game/current-game.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".parent {\n  margin-top: 100px; }\n\n#main {\n  border: 2px;\n  border-width: 1px;\n  width: 1050px;\n  height: 300px;\n  margin: auto;\n  text-align: center;\n  align-content: center; }\n\ntr {\n  font-size: medium; }\n\ncaption {\n  text-align: center;\n  font-size: xx-large; }\n\nth {\n  text-align: center;\n  font: large; }\n\nbutton {\n  font-size: medium;\n  width: 120px;\n  height: 40px; }\n"

/***/ }),

/***/ "./src/app/current-game/current-game/current-game.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/current-game/current-game/current-game.component.ts ***!
  \*********************************************************************/
/*! exports provided: CurrentGameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameComponent", function() { return CurrentGameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var src_app_shared_models_game_current_game_game_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/models/game/current-game-game.view */ "./src/app/shared/models/game/current-game-game.view.ts");
/* harmony import */ var src_app_shared_models_enums_get_action_in_game__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/models/enums/get-action-in-game */ "./src/app/shared/models/enums/get-action-in-game.ts");
/* harmony import */ var src_app_shared_helpers_conversions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/helpers/conversions */ "./src/app/shared/helpers/conversions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CurrentGameComponent = /** @class */ (function () {
    function CurrentGameComponent(gameService, activateRoute, router) {
        this.gameService = gameService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.conversion = new src_app_shared_helpers_conversions__WEBPACK_IMPORTED_MODULE_5__["ConversionHelper"]();
        this.currentGame = new src_app_shared_models_game_current_game_game_view__WEBPACK_IMPORTED_MODULE_3__["CurrentGameGameView"]();
    }
    ;
    CurrentGameComponent.prototype.ngOnInit = function () {
        this.getRound(src_app_shared_models_enums_get_action_in_game__WEBPACK_IMPORTED_MODULE_4__["GetActionInGame"].firstRound);
    };
    CurrentGameComponent.prototype.getRound = function (action) {
        var _this = this;
        this.gameService.currentGame(this.gameId, action).subscribe(function (round) {
            _this.currentGame = round;
            if (_this.currentGame.isDealerTakes) {
                _this.isDealerTake = true;
                _this.getGameEnd();
            }
        });
    };
    CurrentGameComponent.prototype.getGameEnd = function () {
        var _this = this;
        this.isDealerTake = true;
        this.gameService.currentGame(this.gameId, src_app_shared_models_enums_get_action_in_game__WEBPACK_IMPORTED_MODULE_4__["GetActionInGame"].dealerTakes).subscribe(function (round) { return _this.currentGame = round; });
    };
    CurrentGameComponent.prototype.onClickToNewGame = function () {
        this.router.navigate([""]);
    };
    CurrentGameComponent.prototype.onClickToHistory = function () {
        this.router.navigate(["/getAllGames"]);
    };
    CurrentGameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-current-game',
            template: __webpack_require__(/*! ./current-game.component.html */ "./src/app/current-game/current-game/current-game.component.html"),
            styles: [__webpack_require__(/*! ./current-game.component.scss */ "./src/app/current-game/current-game/current-game.component.scss")],
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CurrentGameComponent);
    return CurrentGameComponent;
}());



/***/ }),

/***/ "./src/app/shared/models/game/current-game-game.view.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/models/game/current-game-game.view.ts ***!
  \**************************************************************/
/*! exports provided: CurrentGameGameView, PlayerCurrentGameGameViewItem, CardCurrentGameGameViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameGameView", function() { return CurrentGameGameView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerCurrentGameGameViewItem", function() { return PlayerCurrentGameGameViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardCurrentGameGameViewItem", function() { return CardCurrentGameGameViewItem; });
var CurrentGameGameView = /** @class */ (function () {
    function CurrentGameGameView() {
    }
    return CurrentGameGameView;
}());

var PlayerCurrentGameGameViewItem = /** @class */ (function () {
    function PlayerCurrentGameGameViewItem() {
    }
    return PlayerCurrentGameGameViewItem;
}());

var CardCurrentGameGameViewItem = /** @class */ (function () {
    function CardCurrentGameGameViewItem() {
    }
    return CardCurrentGameGameViewItem;
}());



/***/ })

}]);
//# sourceMappingURL=src-app-current-game-current-game-module.js.map