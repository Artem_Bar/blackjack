(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-get-all-games-get-all-games-module~src-app-start-game-start-game-module"],{

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.component.js":
/*!******************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.component.js ***!
  \******************************************************************************************/
/*! exports provided: AUTOCOMPLETE_VALUE_ACCESSOR, AutoCompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AUTOCOMPLETE_VALUE_ACCESSOR", function() { return AUTOCOMPLETE_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteComponent", function() { return AutoCompleteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _searchbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./searchbar.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js");
/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony import */ var _navigation_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./navigation.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/merge */ "./node_modules/rxjs-compat/_esm5/observable/merge.js");
/* harmony import */ var rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators/filter */ "./node_modules/rxjs-compat/_esm5/operators/filter.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _navigation_action__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./navigation-action */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js");
/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _common_preventable_event__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./common/preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");
/* harmony import */ var _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @progress/kendo-angular-l10n */ "./node_modules/@progress/kendo-angular-l10n/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* tslint:disable:no-null-keyword */
/* tslint:disable:max-line-length */
/* tslint:disable:no-bitwise */


















var InternalState;
(function (InternalState) {
    InternalState[InternalState["None"] = 0] = "None";
    InternalState[InternalState["SetInitial"] = 1] = "SetInitial";
    InternalState[InternalState["UseInitial"] = 2] = "UseInitial";
})(InternalState || (InternalState = {}));
var NO_VALUE = "";
/**
 * @hidden
 */
var AUTOCOMPLETE_VALUE_ACCESSOR = {
    multi: true,
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    // tslint:disable-next-line:no-use-before-declare
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return AutoCompleteComponent; })
};
/**
 * Represents the Kendo UI AutoComplete component for Angular.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-autocomplete
 *      [data]="listItems"
 *      [placeholder]="placeholder"
 *  >
 * `
 * })
 * class AppComponent {
 *   public placeholder: string = 'Type "it" for suggestions';
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 */
var AutoCompleteComponent = /** @class */ (function () {
    function AutoCompleteComponent(localization, popupService, selectionService, navigationService, cdr, renderer, wrapper) {
        this.localization = localization;
        this.popupService = popupService;
        this.selectionService = selectionService;
        this.navigationService = navigationService;
        this.cdr = cdr;
        this.renderer = renderer;
        /**
         * @hidden
         */
        this.focusableId = "k-" + Object(_util__WEBPACK_IMPORTED_MODULE_11__["guid"])();
        /**
         * The hint displayed when the component is empty.
         *
         */
        this.placeholder = "";
        /**
         * Sets the height of the suggestions list. By default, `listHeight` is 200px.
         *
         * > The `listHeight` property affects only the list of suggestions and not the whole popup container.
         * > To set the height of the popup container, use `popupSettings.height`.
         */
        this.listHeight = 200;
        /**
         * @hidden
         *
         * If set to `true`, renders a button on hovering over the component. Clicking this button resets the value of the component to `undefined` and triggers the `change` event.
         */
        this.clearButton = true;
        /**
         * Sets the disabled state of the component.
         */
        this.disabled = false;
        /**
         * Sets the read-only state of the component.
         */
        this.readonly = false;
        /**
         * Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) of the component.
         */
        this.tabindex = 0;
        /**
         * Enables the filter functionality. If set to `true`, the component emits the `filterChange` event.
         */
        this.filterable = false;
        /**
         * Fires each time the value is changed&mdash;
         * when the component is blurred or the value is cleared through the **Clear** button.
         * For more details, refer to the example on [events]({% slug overview_autocomplete %}#toc-events).
         *
         * When the value of the component is programmatically changed through its API or form binding
         * to `ngModel` or `formControl`, the `valueChange` event is not triggered because it
         * might cause a mix-up with the built-in `valueChange` mechanisms of the `ngModel` or `formControl` bindings.
         */
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user types in the input field.
         * You can filter the source based on the passed filtration value.
         * For more details, refer to the example on [events]({% slug overview_autocomplete %}#toc-events).
         */
        this.filterChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to open.
         * This event is preventable. If you cancel it, the popup will remain closed.
         */
        this.open = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to close.
         * This event is preventable. If you cancel it, the popup will remain open.
         */
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user focuses the AutoComplete.
         */
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        /**
         * Fires each time the AutoComplete gets blurred.
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        this.listBoxId = Object(_util__WEBPACK_IMPORTED_MODULE_11__["guid"])();
        this.optionPrefix = Object(_util__WEBPACK_IMPORTED_MODULE_11__["guid"])();
        this.onChangeCallback = function (_value) { };
        this.onTouchedCallback = function (_) { };
        this.popupMouseDownHandler = function (event) { return event.preventDefault(); };
        this._popupSettings = { animate: true };
        this._open = false;
        this._modelValue = "";
        this._value = "";
        this._previousValue = NO_VALUE;
        this._state = InternalState.SetInitial;
        this._filtering = false;
        this.valueChangeSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_8__["Subject"]();
        this._isFocused = false;
        this.direction = localization.rtl ? 'rtl' : 'ltr';
        this.wrapper = wrapper.nativeElement;
        this.data = [];
        this.subscribeEvents();
        this.selectionService.resetSelection([-1]);
    }
    Object.defineProperty(AutoCompleteComponent.prototype, "width", {
        get: function () {
            var wrapperOffsetWidth = 0;
            if (Object(_util__WEBPACK_IMPORTED_MODULE_11__["isDocumentAvailable"])()) {
                wrapperOffsetWidth = this.wrapper.offsetWidth;
            }
            var width = this.popupSettings.width || wrapperOffsetWidth;
            var minWidth = isNaN(wrapperOffsetWidth) ? wrapperOffsetWidth : wrapperOffsetWidth + "px";
            var maxWidth = isNaN(width) ? width : width + "px";
            return { min: minWidth, max: maxWidth };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "height", {
        get: function () {
            var popupHeight = this.popupSettings.height;
            return Object(_util__WEBPACK_IMPORTED_MODULE_11__["isPresent"])(popupHeight) ? popupHeight + "px" : 'auto';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "listContainerClasses", {
        get: function () {
            var containerClasses = ['k-list-container', 'k-reset'];
            if (this.popupSettings.popupClass) {
                containerClasses.push(this.popupSettings.popupClass);
            }
            return containerClasses;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "suggestion", {
        get: function () {
            if (!this.text || !this.suggestedText) {
                this.suggestedText = undefined;
                return;
            }
            var hasMatch = this.suggestedText.toLowerCase().startsWith(this.text.toLowerCase());
            var shouldSuggest = this.suggest && !this.backspacePressed;
            if (shouldSuggest && hasMatch) {
                return this.suggestedText;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "appendTo", {
        get: function () {
            var appendTo = this.popupSettings.appendTo;
            if (!appendTo || appendTo === 'root') {
                return undefined;
            }
            return appendTo === 'component' ? this.container : appendTo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "popupOpen", {
        get: function () {
            return this._open;
        },
        set: function (open) {
            if (this.disabled || this.readonly || this.popupOpen === open) {
                return;
            }
            var eventArgs = new _common_preventable_event__WEBPACK_IMPORTED_MODULE_15__["PreventableEvent"]();
            if (open && !this.popupOpen) {
                this.open.emit(eventArgs);
            }
            if (!open && this.popupOpen) {
                this.close.emit(eventArgs);
            }
            if (eventArgs.isDefaultPrevented()) {
                return;
            }
            this._toggle(open);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "activeDescendant", {
        get: function () {
            return this.optionPrefix + "-" + this.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        /**
         * Sets the data of the AutoComplete.
         *
         * > The data has to be provided in an array-like list.
         */
        set: function (data) {
            this._data = data || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "value", {
        get: function () {
            return this._value || NO_VALUE;
        },
        /**
         * Sets the value of the AutoComplete.
         *
         */
        set: function (newValue) {
            this.verifySettings(newValue);
            this._value = newValue || NO_VALUE;
            this.cdr.markForCheck();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "popupSettings", {
        get: function () {
            return this._popupSettings;
        },
        /**
         * Configures the popup of the AutoComplete.
         *
         * The available options are:
         * - `animate: Boolean`&mdash;Controls the popup animation. By default, the open and close animations are enabled.
         * - `width: Number | String`&mdash;Sets the width of the popup container. By default, the width of the host element is used  If set to `auto`, the component automatically adjusts the width of the popup, so no item labels are wrapped.
         * - `height: Number`&mdash;Sets the height of the popup container.
         * - `popupClass: String`&mdash;Specifies a list of CSS classes that are used to style the popup.
         */
        set: function (settings) {
            this._popupSettings = Object.assign({ animate: true }, settings);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "tabIndex", {
        get: function () {
            return this.tabindex;
        },
        /**
         * @hidden
         */
        set: function (tabIndex) {
            this.tabindex = tabIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "widgetClasses", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "isFocused", {
        get: function () {
            return this._isFocused;
        },
        set: function (isFocused) {
            this._isFocused = isFocused;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "isDisabled", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AutoCompleteComponent.prototype, "dir", {
        get: function () {
            return this.direction;
        },
        enumerable: true,
        configurable: true
    });
    AutoCompleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderer.removeAttribute(this.wrapper, "tabindex");
        this.localizationChangeSubscription = this.localization
            .changes
            .subscribe(function (_a) {
            var rtl = _a.rtl;
            return _this.direction = rtl ? 'rtl' : 'ltr';
        });
    };
    AutoCompleteComponent.prototype.ngOnDestroy = function () {
        this._toggle(false);
        this.unsubscribeEvents();
        if (this.localizationChangeSubscription) {
            this.localizationChangeSubscription.unsubscribe();
        }
    };
    AutoCompleteComponent.prototype.ngOnChanges = function (changes) {
        var isInitial = Boolean(changes.value && changes.value.firstChange || (Object(_util__WEBPACK_IMPORTED_MODULE_11__["isChanged"])("value", changes) && (this._state & InternalState.SetInitial)));
        if (isInitial) {
            this._state |= InternalState.UseInitial;
        }
        if (changes.hasOwnProperty("value")) {
            this._modelValue = changes.value.currentValue;
        }
        var STATE_PROPS = /(data|value|textField|valueField|valuePrimitive)/g;
        if (STATE_PROPS.test(Object.keys(changes).join())) {
            this.setState();
        }
        var shouldSuggest = this.suggest && this.data && this.data.length && this.value;
        if (shouldSuggest) {
            this.suggestedText = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[0], this.valueField);
        }
    };
    /**
     * Toggles the visibility of the popup. If you use the `toggle` method to open or close the popup, the `open` and `close` events will not be fired.
     *
     * @param open - The state of the popup.
     */
    AutoCompleteComponent.prototype.toggle = function (open) {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this._toggle((open === undefined) ? !_this._open : open);
            _this.cdr.markForCheck();
        });
    };
    Object.defineProperty(AutoCompleteComponent.prototype, "isOpen", {
        /**
         * Returns the current open state of the popup.
         */
        get: function () {
            return this.popupOpen;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Resets the value of the AutoComplete.
     * If you use the `reset` method to clear the value of the component,
     * the model will not update automatically and the `selectionChange` and `valueChange` events will not be fired.
     */
    AutoCompleteComponent.prototype.reset = function () {
        this._modelValue = NO_VALUE;
        this.setState();
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.clearValue = function (event) {
        event.stopImmediatePropagation();
        this.focus();
        this._filtering = true;
        this.change(NO_VALUE);
        this._filtering = false;
        this.selectionService.resetSelection([]);
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.writeValue = function (value) {
        var isInitial = Boolean(value !== null && this._state & InternalState.SetInitial);
        if (value === null && this._state & InternalState.SetInitial) {
            return;
        }
        if (isInitial) {
            this._state |= InternalState.UseInitial;
        }
        this._modelValue = value;
        this.setState();
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.setDisabledState = function (isDisabled) {
        this.disabled = isDisabled;
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.popupOpened = function () {
        this.popupWidth = this.width.max;
        this.popupMinWidth = this.width.min;
    };
    /**
     * Focuses the AutoComplete.
     */
    AutoCompleteComponent.prototype.focus = function () {
        if (!this.disabled) {
            this.searchbar.focus();
        }
    };
    /**
     * Blurs the AutoComplete.
     */
    AutoCompleteComponent.prototype.blur = function () {
        if (!this.disabled) {
            this.searchbar.blur();
        }
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.onResize = function () {
        if (this._open) {
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
        }
    };
    AutoCompleteComponent.prototype.emitChange = function () {
        if (this.value === this._previousValue) {
            return;
        }
        this._modelValue = this.value;
        this._previousValue = this.value;
        this.onChangeCallback(this.value);
        this.valueChange.emit(this.value);
    };
    AutoCompleteComponent.prototype.verifySettings = function (newValue) {
        if (!Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["isDevMode"])()) {
            return;
        }
        if (Object(_util__WEBPACK_IMPORTED_MODULE_11__["isPresent"])(newValue) && typeof newValue !== "string") {
            throw new Error("Expected value of type string. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/autocomplete/#toc-value");
        }
    };
    AutoCompleteComponent.prototype.search = function (text) {
        var index = text.length ? this.data.findIndex(this.findIndexPredicate(text)) : -1;
        this.selectionService.focus(index);
        if (this.suggest) {
            this.suggestedText = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[index], this.valueField);
        }
    };
    AutoCompleteComponent.prototype.navigate = function (index) {
        if (!this.popupOpen) {
            return;
        }
        if (index < 0 || index > this.data.length) {
            index = 0;
        }
        this.selectionService.focus(index);
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.handleNavigate = function (event) {
        var focused = isNaN(this.selectionService.focused) ? 0 : this.selectionService.focused;
        var hasFocused = Object(_util__WEBPACK_IMPORTED_MODULE_11__["isPresent"])(focused);
        var offset = 0;
        if (this.disabled || this.readonly) {
            return;
        }
        if (!hasFocused) {
            if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_14__["Keys"].down) {
                offset = -1;
            }
            else if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_14__["Keys"].up) {
                offset = 1;
            }
        }
        var action = this.navigationService.process({
            current: focused + offset,
            max: this.data.length - 1,
            min: 0,
            originalEvent: event
        });
        if (action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Undefined &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Backspace &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Delete &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Home &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].End &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Left &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Right &&
            ((action === _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Enter && this.popupOpen) || action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Enter)) {
            event.preventDefault();
        }
    };
    AutoCompleteComponent.prototype.handleEnter = function (event) {
        var focused = this.selectionService.focused;
        var value;
        this._filtering = false;
        if (this.popupOpen) {
            event.originalEvent.preventDefault();
        }
        if (focused >= 0) {
            value = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[focused], this.valueField);
        }
        else {
            var match = this.suggest && this.suggestedText && this.data.length &&
                Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[0], this.valueField, true).toLowerCase() === this.searchbar.value.toLowerCase();
            if (this.popupOpen && match) {
                value = this.suggestedText;
            }
            else {
                value = this.searchbar.value;
            }
        }
        this.change(value);
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.searchBarChange = function (text) {
        var currentTextLength = this.text.length;
        this.backspacePressed = (text.length < currentTextLength) ? true : false;
        this.text = text;
        this.popupOpen = text.length > 0;
        this._filtering = true;
        if (this.filterable) {
            this.selectionService.focused = -1;
            this.filterChange.emit(text);
        }
        else {
            this.search(text);
        }
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.handleFocus = function () {
        this.isFocused = true;
        this.onFocus.emit();
    };
    /**
     * @hidden
     */
    AutoCompleteComponent.prototype.handleBlur = function () {
        this._filtering = false;
        var focused = this.filterable ? this.selectionService.focused : -1;
        var dataItem;
        var text;
        var value = this.value;
        if (focused !== -1) {
            dataItem = this.data[focused];
            text = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(dataItem, this.valueField, true) || "";
        }
        else {
            text = this.searchbar.value;
        }
        if (text === this.searchbar.value) {
            value = text;
        }
        else if (text && text.toLowerCase() === this.searchbar.value.toLowerCase()) {
            this.selectionService.resetSelection([]);
            value = this.searchbar.value;
        }
        this.change(value);
        this.popupOpen = false;
        this.isFocused = false;
        this.onBlur.emit();
        this.onTouchedCallback();
    };
    AutoCompleteComponent.prototype.setState = function () {
        if (this._filtering) {
            return;
        }
        if (this._state & InternalState.UseInitial) {
            this._state &= ~InternalState.UseInitial;
            this._state &= ~InternalState.SetInitial;
            this._previousValue = this._modelValue;
        }
        else {
            this._previousValue = this.value;
        }
        this.value = this._modelValue;
        this.text = this.value;
    };
    AutoCompleteComponent.prototype.change = function (value) {
        this.popupOpen = false;
        this.valueChangeSubject.next(value);
    };
    AutoCompleteComponent.prototype.subscribeEvents = function () {
        var _this = this;
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_11__["isDocumentAvailable"])()) {
            return;
        }
        this.valueChangeSubscription = this.valueChangeSubject.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_10__["filter"])(function (candidate) {
            return !(_this.value === candidate && _this.text === candidate);
        }))
            .subscribe(function (value) {
            _this.value = value;
            _this.text = value;
            if (_this.filterable && !_this.value && !_this.text) {
                _this.filterChange.emit("");
            }
            _this.emitChange();
        });
        this.changeSubscription = this.selectionService.onChange.subscribe(this.handleItemChange.bind(this));
        this.focusSubscription = this.selectionService.onFocus.subscribe(this.handleItemFocus.bind(this));
        this.navigationSubscription = Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_9__["merge"])(this.navigationService.up, this.navigationService.down).subscribe(function (event) { return _this.navigate(event.index); });
        this.closeSubscription = this.navigationService.close.subscribe(function () { return _this.popupOpen = false; });
        this.enterSubscription = this.navigationService.enter.subscribe(this.handleEnter.bind(this));
        this.escSubscription = this.navigationService.esc.subscribe(this.handleBlur.bind(this));
    };
    AutoCompleteComponent.prototype.unsubscribeEvents = function () {
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_11__["isDocumentAvailable"])()) {
            return;
        }
        this.changeSubscription.unsubscribe();
        this.navigationSubscription.unsubscribe();
        this.closeSubscription.unsubscribe();
        this.enterSubscription.unsubscribe();
        this.escSubscription.unsubscribe();
        this.valueChangeSubscription.unsubscribe();
        this.focusSubscription.unsubscribe();
    };
    AutoCompleteComponent.prototype.handleItemChange = function (event) {
        var index = event.indices.length ? event.indices[0] : undefined;
        this._filtering = false;
        this.selectionService.resetSelection([-1]);
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_11__["isPresent"])(index)) {
            return;
        }
        var text = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[index], this.valueField);
        this.change(text);
    };
    AutoCompleteComponent.prototype.handleItemFocus = function (_event) {
        var focused = this.selectionService.focused;
        var shouldSuggest = Boolean(this.suggest && this.data && this.data.length && focused >= 0);
        if (shouldSuggest) {
            this.suggestedText = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(this.data[focused], this.valueField);
        }
    };
    AutoCompleteComponent.prototype._toggle = function (open) {
        var _this = this;
        this._open = open;
        if (this.popupRef) {
            this.popupRef.popupElement
                .removeEventListener('mousedown', this.popupMouseDownHandler);
            this.popupRef.close();
            this.popupRef = null;
        }
        if (this._open) {
            this.popupRef = this.popupService.open({
                anchor: this.wrapper,
                animate: this.popupSettings.animate,
                appendTo: this.appendTo,
                content: this.popupTemplate,
                popupClass: this.listContainerClasses,
                positionMode: 'absolute'
            });
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.addEventListener('mousedown', this.popupMouseDownHandler);
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
            popupWrapper.style.height = this.height;
            popupWrapper.setAttribute("dir", this.direction);
            this.popupRef.popupAnchorViewportLeave.subscribe(function () { return _this.popupOpen = false; });
        }
    };
    AutoCompleteComponent.prototype.findIndexPredicate = function (text) {
        var _this = this;
        return function (item) {
            var itemText = Object(_util__WEBPACK_IMPORTED_MODULE_11__["getter"])(item, _this.valueField);
            itemText = itemText === undefined ? "" : itemText.toString().toLowerCase();
            return itemText.startsWith(text.toLowerCase());
        };
    };
    AutoCompleteComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    exportAs: 'kendoAutoComplete',
                    providers: [
                        AUTOCOMPLETE_VALUE_ACCESSOR,
                        _selection_service__WEBPACK_IMPORTED_MODULE_6__["SelectionService"],
                        _navigation_service__WEBPACK_IMPORTED_MODULE_7__["NavigationService"],
                        _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_16__["LocalizationService"],
                        {
                            provide: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_16__["L10N_PREFIX"],
                            useValue: 'kendo.autocomplete'
                        }
                    ],
                    selector: 'kendo-autocomplete',
                    template: "\n        <kendo-searchbar #searchbar\n            [role]=\"'textbox'\"\n            [id]=\"focusableId\"\n            [listId]=\"listBoxId\"\n            [activeDescendant]=\"activeDescendant\"\n            [userInput]=\"text\"\n            [suggestedText]=\"suggestion\"\n            [disabled]=\"disabled\"\n            [readonly]=\"readonly\"\n            [tabIndex]=\"tabIndex\"\n            [popupOpen]=\"popupOpen\"\n            [placeholder]=\"placeholder\"\n            (onNavigate)=\"handleNavigate($event)\"\n            (valueChange)=\"searchBarChange($event)\"\n            (onBlur)=\"handleBlur()\"\n            (onFocus)=\"handleFocus()\"\n        ></kendo-searchbar>\n        <span *ngIf=\"!loading && !readonly && (clearButton && text?.length)\" class=\"k-icon k-clear-value k-i-close\" title=\"clear\" role=\"button\" tabindex=\"-1\" (click)=\"clearValue($event)\" (mousedown)=\"$event.preventDefault()\">\n</span>\n        <span *ngIf=\"loading\" class=\"k-icon k-i-loading\"></span>\n        <ng-template #popupTemplate>\n            <!--header template-->\n            <ng-template *ngIf=\"headerTemplate\"\n                [templateContext]=\"{\n                    templateRef: headerTemplate.templateRef\n                }\">\n            </ng-template>\n            <!--list-->\n            <kendo-list\n                [id]=\"listBoxId\"\n                [optionPrefix]=\"optionPrefix\"\n                [data]=\"data\"\n                [textField]=\"valueField\"\n                [valueField]=\"valueField\"\n                [template]=\"template\"\n                [height]=\"listHeight\"\n                [show]=\"popupOpen\"\n            >\n            </kendo-list>\n            <!--no-data template-->\n            <div class=\"k-nodata\" *ngIf=\"data.length === 0\">\n                <ng-template [ngIf]=\"noDataTemplate\"\n                    [templateContext]=\"{\n                        templateRef: noDataTemplate?.templateRef\n                    }\">\n                </ng-template>\n                <ng-template [ngIf]=\"!noDataTemplate\">\n                    <div>NO DATA FOUND.</div>\n                </ng-template>\n            </div>\n            <!--footer template-->\n            <ng-template *ngIf=\"footerTemplate\"\n                [templateContext]=\"{\n                    templateRef: footerTemplate.templateRef\n                }\">\n            </ng-template>\n        </ng-template>\n        <ng-template [ngIf]=\"popupOpen\">\n            <kendo-resize-sensor (resize)=\"onResize()\"></kendo-resize-sensor>\n        </ng-template>\n        <ng-container #container></ng-container>\n  "
                },] },
    ];
    /** @nocollapse */
    AutoCompleteComponent.ctorParameters = function () { return [
        { type: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_16__["LocalizationService"], },
        { type: _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_17__["PopupService"], },
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_6__["SelectionService"], },
        { type: _navigation_service__WEBPACK_IMPORTED_MODULE_7__["NavigationService"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
    ]; };
    AutoCompleteComponent.propDecorators = {
        'focusableId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'placeholder': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'popupSettings': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'listHeight': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'loading': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'clearButton': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'suggest': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'readonly': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabindex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabIndex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ["tabIndex",] },],
        'filterable': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'filterChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'open': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'close': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['focus',] },],
        'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['blur',] },],
        'template': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_item_template_directive__WEBPACK_IMPORTED_MODULE_3__["ItemTemplateDirective"],] },],
        'headerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_header_template_directive__WEBPACK_IMPORTED_MODULE_4__["HeaderTemplateDirective"],] },],
        'footerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__["FooterTemplateDirective"],] },],
        'noDataTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_13__["NoDataTemplateDirective"],] },],
        'container': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['container', { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] },] },],
        'popupTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['popupTemplate',] },],
        'searchbar': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_searchbar_component__WEBPACK_IMPORTED_MODULE_2__["SearchBarComponent"],] },],
        'widgetClasses': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-widget',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-autocomplete',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-header',] },],
        'isFocused': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-state-focused',] },],
        'isDisabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-state-disabled',] },],
        'dir': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.dir',] },],
    };
    return AutoCompleteComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.module.js":
/*!***************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.module.js ***!
  \***************************************************************************************/
/*! exports provided: AutoCompleteModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteModule", function() { return AutoCompleteModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _autocomplete_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autocomplete.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.component.js");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js");
/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");




var AUTOCOMPLETE_DIRECTIVES = [
    _autocomplete_component__WEBPACK_IMPORTED_MODULE_1__["AutoCompleteComponent"]
];
/**
 * @hidden
 *
 * The exported package module.
 *
 * The package exports:
 * - `AutoCompleteComponent`&mdash;The AutoComplete component class.
 * - `ItemTemplateDirective`&mdash;The item template directive.
 * - `HeaderTemplateDirective`&mdash;The header template directive.
 * - `FooterTemplateDirective`&mdash;The footer template directive.
 */
var AutoCompleteModule = /** @class */ (function () {
    function AutoCompleteModule() {
    }
    AutoCompleteModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [AUTOCOMPLETE_DIRECTIVES],
                    exports: [AUTOCOMPLETE_DIRECTIVES, _shared_directives_module__WEBPACK_IMPORTED_MODULE_3__["SharedDirectivesModule"]],
                    imports: [_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]]
                },] },
    ];
    /** @nocollapse */
    AutoCompleteModule.ctorParameters = function () { return []; };
    return AutoCompleteModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.component.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.component.js ***!
  \**************************************************************************************/
/*! exports provided: COMBOBOX_VALUE_ACCESSOR, ComboBoxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMBOBOX_VALUE_ACCESSOR", function() { return COMBOBOX_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComboBoxComponent", function() { return ComboBoxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _searchbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./searchbar.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js");
/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony import */ var _navigation_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navigation.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js");
/* harmony import */ var rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/merge */ "./node_modules/rxjs-compat/_esm5/observable/merge.js");
/* harmony import */ var rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators/catchError */ "./node_modules/rxjs-compat/_esm5/operators/catchError.js");
/* harmony import */ var rxjs_operators_distinctUntilChanged__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators/distinctUntilChanged */ "./node_modules/rxjs-compat/_esm5/operators/distinctUntilChanged.js");
/* harmony import */ var rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators/filter */ "./node_modules/rxjs-compat/_esm5/operators/filter.js");
/* harmony import */ var rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators/map */ "./node_modules/rxjs-compat/_esm5/operators/map.js");
/* harmony import */ var rxjs_operators_partition__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators/partition */ "./node_modules/rxjs-compat/_esm5/operators/partition.js");
/* harmony import */ var rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs/operators/tap */ "./node_modules/rxjs-compat/_esm5/operators/tap.js");
/* harmony import */ var rxjs_operators_throttleTime__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! rxjs/operators/throttleTime */ "./node_modules/rxjs-compat/_esm5/operators/throttleTime.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_Subscription__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! rxjs/Subscription */ "./node_modules/rxjs-compat/_esm5/Subscription.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _navigation_action__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./navigation-action */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./common/preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");
/* harmony import */ var _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @progress/kendo-angular-l10n */ "./node_modules/@progress/kendo-angular-l10n/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* harmony import */ var _touch_enabled__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./touch-enabled */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/touch-enabled.js");
/* harmony import */ var _error_messages__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./error-messages */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/error-messages.js");
/* tslint:disable:no-null-keyword */
/* tslint:disable:max-line-length */
/* tslint:disable:no-bitwise */



























var InternalState;
(function (InternalState) {
    InternalState[InternalState["None"] = 0] = "None";
    InternalState[InternalState["SetInitial"] = 1] = "SetInitial";
    InternalState[InternalState["UseModel"] = 2] = "UseModel";
    InternalState[InternalState["UseExisting"] = 4] = "UseExisting";
    InternalState[InternalState["UseCustom"] = 8] = "UseCustom";
    InternalState[InternalState["UseFilter"] = 16] = "UseFilter";
    InternalState[InternalState["UseEmpty"] = 32] = "UseEmpty";
})(InternalState || (InternalState = {}));
/**
 * @hidden
 */
var COMBOBOX_VALUE_ACCESSOR = {
    multi: true,
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    // tslint:disable-next-line:no-use-before-declare
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ComboBoxComponent; })
};
/**
 * Represents the Kendo UI ComboBox component for Angular.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-combobox [data]="listItems">
 *  </kendo-combobox>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 */
var ComboBoxComponent = /** @class */ (function () {
    function ComboBoxComponent(localization, popupService, selectionService, navigationService, cdr, renderer, wrapper, touchEnabled) {
        this.localization = localization;
        this.popupService = popupService;
        this.selectionService = selectionService;
        this.navigationService = navigationService;
        this.cdr = cdr;
        this.renderer = renderer;
        this.touchEnabled = touchEnabled;
        this.selected = [];
        /**
         * @hidden
         */
        this.focusableId = "k-" + Object(_util__WEBPACK_IMPORTED_MODULE_19__["guid"])();
        /**
         * Specifies whether the ComboBox allows user-defined values that are not present in the dataset.
         * The default value is `false`.
         *
         * For more information, refer to the article on [custom values]({% slug custom_values_combobox %}).
         */
        this.allowCustom = false;
        /**
         * A user-defined callback which returns normalized custom values. Typically used when the data items are different from type `string`.
         * @param { Any } value - The custom value defined by the user.
         * @returns { Any }
         *
         * @example
         * ```ts
         * import { map } from 'rxjs/operators/map';
         *
         * _@Component({
         * selector: 'my-app',
         * template: `
         *   <kendo-combobox
         *       [allowCustom]="true"
         *       [data]="listItems"
         *       [textField]="'text'"
         *       [valueField]="'value'"
         *       [valueNormalizer]="valueNormalizer"
         *       (valueChange)="onValueChange($event)"
         *   >
         *   </kendo-combobox>
         * `
         * })
         *
         * class AppComponent {
         *   public listItems: Array<{ text: string, value: number }> = [
         *       { text: "Small", value: 1 },
         *       { text: "Medium", value: 2 },
         *       { text: "Large", value: 3 }
         *   ];
         *
         *   public onValueChange(value) {
         *       console.log("valueChange : ", value);
         *   }
         *
         *   public valueNormalizer = (text$: Observable<string>) => text$.pipe(map((text: string) => {
         *      return { ProductID: null, ProductName: text };
         *   }));
         *
         * }
         * ```
         */
        this.valueNormalizer = function (text) {
            return text.pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (userInput) { return userInput; }));
        };
        /**
         * The hint displayed when the component is empty.
         *
         */
        this.placeholder = "";
        /**
         * Sets the height of the suggestions list. By default, `listHeight` is 200px.
         *
         * > The `listHeight` property affects only the list of suggestions and not the whole popup container.
         * > To set the height of the popup container, use `popupSettings.height`.
         */
        this.listHeight = 200;
        /**
         * @hidden
         *
         * Enables the auto-completion of the text based on the first data item.
         */
        this.suggest = false;
        /**
         * If set to `true`, renders a button on hovering over the component. Clicking this button resets the value of the component to `undefined` and triggers the `change` event.
         */
        this.clearButton = true;
        /**
         * Sets the disabled state of the component.
         */
        this.disabled = false;
        /**
         * Sets the readonly state of the component.
         */
        this.readonly = false;
        /**
         * Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) of the component.
         */
        this.tabindex = 0;
        /**
         * Enables the filtering functionality. If set to `true`, the component emits the `filterChange` event.
         */
        this.filterable = false;
        /**
         * Fires each time the value is changed&mdash;
         * when the component is blurred or the value is cleared through the **Clear** button.
         * For more details, refer to the example on [events]({% slug overview_combobox %}#toc-events).
         *
         * When the value of the component is programmatically changed through its API or form binding
         * to `ngModel` or `formControl`, the `valueChange` event is not triggered because it
         * might cause a mix-up with the built-in `valueChange` mechanisms of the `ngModel` or `formControl` bindings.
         */
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time an item selection is changed.
         * For more details, refer to the example on [events]({% slug overview_combobox %}#toc-events).
         */
        this.selectionChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user types in the input field.
         * You can filter the source based on the passed filtration value.
         * For more details, refer to the example on [events]({% slug overview_combobox %}#toc-events).
         */
        this.filterChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to open.
         * This event is preventable. If you cancel it, the popup will remain closed.
         */
        this.open = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to close.
         * This event is preventable. If you cancel it, the popup will remain open.
         */
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user focuses the ComboBox.
         */
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        /**
         * Fires each time the ComboBox gets blurred.
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        this.isFocused = false;
        this.listBoxId = Object(_util__WEBPACK_IMPORTED_MODULE_19__["guid"])();
        this.optionPrefix = Object(_util__WEBPACK_IMPORTED_MODULE_19__["guid"])();
        this.onChangeCallback = function (_) { };
        this.onTouchedCallback = function (_) { };
        this.observableSubscriptions = new rxjs_Subscription__WEBPACK_IMPORTED_MODULE_18__["Subscription"]();
        this._state = InternalState.SetInitial;
        this._filtering = false;
        this._text = "";
        this._open = false;
        this._popupSettings = { animate: true };
        this.popupMouseDownHandler = function (event) { return event.preventDefault(); };
        this.customValueSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_17__["Subject"]();
        this.valueSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_17__["Subject"]();
        this.selectionSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_17__["Subject"]();
        this.direction = localization.rtl ? 'rtl' : 'ltr';
        this.wrapper = wrapper.nativeElement;
        this.data = [];
        this.subscribeEvents();
    }
    Object.defineProperty(ComboBoxComponent.prototype, "width", {
        get: function () {
            var wrapperOffsetWidth = 0;
            if (Object(_util__WEBPACK_IMPORTED_MODULE_19__["isDocumentAvailable"])()) {
                wrapperOffsetWidth = this.wrapper.offsetWidth;
            }
            var width = this.popupSettings.width || wrapperOffsetWidth;
            var minWidth = isNaN(wrapperOffsetWidth) ? wrapperOffsetWidth : wrapperOffsetWidth + "px";
            var maxWidth = isNaN(width) ? width : width + "px";
            return { min: minWidth, max: maxWidth };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "height", {
        get: function () {
            var popupHeight = this.popupSettings.height;
            return Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(popupHeight) ? popupHeight + "px" : 'auto';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "text", {
        get: function () {
            return this._text;
        },
        set: function (text) {
            var textCandidate = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(text, this.textField, true);
            this._text = Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(textCandidate) ? textCandidate.toString() : "";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "popupOpen", {
        get: function () {
            return this._open;
        },
        set: function (open) {
            if (this.disabled || this.readonly || this.popupOpen === open) {
                return;
            }
            var eventArgs = new _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__["PreventableEvent"]();
            if (open) {
                this.open.emit(eventArgs);
            }
            else {
                this.close.emit(eventArgs);
            }
            if (eventArgs.isDefaultPrevented()) {
                return;
            }
            this._toggle(open);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "activeDescendant", {
        get: function () {
            return this.optionPrefix + "-" + Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this.value, this.valueField);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "appendTo", {
        get: function () {
            var appendTo = this.popupSettings.appendTo;
            if (!appendTo || appendTo === 'root') {
                return undefined;
            }
            return appendTo === 'component' ? this.container : appendTo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        /**
         * Sets the data of the ComboBox.
         *
         * > The data has to be provided in an array-like list.
         */
        set: function (data) {
            this._data = data || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        /**
         * Sets the value of the ComboBox. It can either be of the primitive (string, numbers) or of the complex (objects) type. To define the type, use the `valuePrimitive` option.
         *
         * > All selected values which are not present in the dataset are considered custom values. When the `Enter` key is pressed or the component loses focus, custom values get dismissed unless `allowCustom` is set to `true`.
         */
        set: function (newValue) {
            this.verifySettings(newValue);
            this._value = newValue;
            this.cdr.markForCheck();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "popupSettings", {
        get: function () {
            return this._popupSettings;
        },
        /**
         * Configures the popup of the ComboBox.
         *
         * The available options are:
         * - `animate: Boolean`&mdash;Controls the popup animation. By default, the open and close animations are enabled.
         * - `width: Number | String`&mdash;Sets the width of the popup container. By default, the width of the host element is used  If set to `auto`, the component automatically adjusts the width of the popup, so no item labels are wrapped.
         * - `height: Number`&mdash;Sets the height of the popup container.
         * - `popupClass: String`&mdash;Specifies a list of CSS classes that are used to style the popup.
         */
        set: function (settings) {
            this._popupSettings = Object.assign({ animate: true }, settings);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "tabIndex", {
        get: function () {
            return this.tabindex;
        },
        /**
         * @hidden
         */
        set: function (tabIndex) {
            this.tabindex = tabIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "widgetClasses", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.wrapperClasses = function () {
        return {
            'k-dropdown-wrap': true,
            'k-state-default': !this.disabled,
            'k-state-disabled': this.disabled,
            'k-state-focused': this.isFocused
        };
    };
    Object.defineProperty(ComboBoxComponent.prototype, "clearable", {
        get: function () {
            return this.clearButton;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComboBoxComponent.prototype, "dir", {
        get: function () {
            return this.direction;
        },
        enumerable: true,
        configurable: true
    });
    ComboBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderer.removeAttribute(this.wrapper, "tabindex");
        this.createSelectionStream();
        this.createValueStream();
        this.localizationChangeSubscription = this.localization
            .changes.subscribe(function (_a) {
            var rtl = _a.rtl;
            return _this.direction = rtl ? 'rtl' : 'ltr';
        });
    };
    ComboBoxComponent.prototype.createSelectionStream = function () {
        var _this = this;
        if (this.selectionSubscription) {
            this.selectionSubscription.unsubscribe();
        }
        this.selectionSubscription =
            this.selectionSubject.pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (index) {
                return {
                    dataItem: _this.data[index],
                    value: Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(_this.data[index], _this.valueField)
                };
            }), Object(rxjs_operators_distinctUntilChanged__WEBPACK_IMPORTED_MODULE_11__["distinctUntilChanged"])(function (prev, next) {
                return prev.value === next.value;
            }))
                .subscribe(function (args) {
                _this.selectionChange.emit(args.dataItem);
            });
    };
    ComboBoxComponent.prototype.createValueStream = function () {
        var _this = this;
        var valueStream = this.valueSubject.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_12__["filter"])(function (candidate) {
            var current = _this.valuePrimitive ? _this.value : Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(_this.value, _this.valueField);
            var newValue = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(candidate, _this.valueField);
            var newText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(candidate, _this.textField);
            if (Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(newText)) {
                newText = newText.toString();
            }
            if (current === newValue && _this.text === newText) {
                return false;
            }
            else {
                return true;
            }
        }), Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (candidate) {
            var newValue = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(candidate, _this.valueField);
            var newText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(candidate, _this.textField);
            return {
                dataItem: candidate,
                text: newText,
                value: _this.valuePrimitive ? newValue : candidate
            };
        }));
        var customValueStreams = Object(rxjs_operators_partition__WEBPACK_IMPORTED_MODULE_14__["partition"])(function () { return _this.allowCustom; })(this.customValueSubject.pipe(Object(rxjs_operators_throttleTime__WEBPACK_IMPORTED_MODULE_16__["throttleTime"])(300)));
        var allowCustomValueStream = customValueStreams[0].pipe(Object(rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_15__["tap"])(function () {
            _this.loading = true;
            _this.disabled = true;
        }), Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_12__["filter"])(function () {
            var hasChange = _this.text !== Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(_this.value, _this.textField);
            _this.loading = hasChange;
            _this.disabled = hasChange;
            return hasChange;
        }), this.valueNormalizer, Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (normalizedValue) {
            return {
                dataItem: normalizedValue,
                text: _this.text,
                value: normalizedValue
            };
        }));
        var disableCustomValueStream = customValueStreams[1].pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function () {
            return {
                dataItem: undefined,
                text: undefined,
                value: undefined
            };
        }));
        if (this.valueSubscription) {
            this.valueSubscription.unsubscribe();
        }
        var merged = Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_9__["merge"])(valueStream, allowCustomValueStream, disableCustomValueStream);
        this.valueSubscription = merged.pipe(Object(rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_10__["catchError"])(function () {
            _this.dataItem = undefined;
            _this.value = undefined;
            _this.text = undefined;
            _this.loading = false;
            _this.disabled = false;
            _this.emitChange();
            return merged;
        }))
            .subscribe(function (state) {
            _this.dataItem = state.dataItem;
            _this.value = state.value;
            _this.text = state.text;
            _this.loading = false;
            _this.disabled = false;
            if (_this.filterable && !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(state.value) && !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(state.text) && !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(_this._previousValue)) {
                _this.filterChange.emit("");
            }
            _this.emitChange();
        });
    };
    ComboBoxComponent.prototype.subscribeEvents = function () {
        var _this = this;
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_19__["isDocumentAvailable"])()) {
            return;
        }
        [
            this.selectionService.onChange.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_12__["filter"])(function (event) { return Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(event.indices[0]); }), Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (event) { return event.indices[0]; }))
                .subscribe(this.handleItemChange.bind(this)),
            this.selectionService.onSelect.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_12__["filter"])(function (event) { return Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(event.indices[0]); }), Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_13__["map"])(function (event) { return event.indices[0]; }))
                .subscribe(this.handleItemSelect.bind(this)),
            Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_9__["merge"])(this.navigationService.up, this.navigationService.down, this.navigationService.home, this.navigationService.end).subscribe(function (event) { return _this.navigate(event.index); }),
            this.navigationService.open.subscribe(function () { return _this.popupOpen = true; }),
            this.navigationService.close.subscribe(function () { return _this.popupOpen = false; }),
            this.navigationService.enter.subscribe(function (event) {
                if (_this.popupOpen) {
                    event.originalEvent.preventDefault();
                }
                _this.confirmSelection();
            }),
            this.navigationService.esc.subscribe(this.handleBlur.bind(this))
        ].forEach(function (s) { return _this.observableSubscriptions.add(s); });
    };
    ComboBoxComponent.prototype.unsubscribeEvents = function () {
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_19__["isDocumentAvailable"])()) {
            return;
        }
        this.observableSubscriptions.unsubscribe();
        if (this.valueSubscription) {
            this.valueSubscription.unsubscribe();
        }
        if (this.selectionSubscription) {
            this.selectionSubscription.unsubscribe();
        }
    };
    ComboBoxComponent.prototype.handleItemChange = function (index) {
        this._filtering = false;
        this.change(this.data[index]);
    };
    ComboBoxComponent.prototype.handleItemSelect = function (index) {
        this._filtering = false;
        if (!this.popupOpen) {
            this.change(this.data[index]);
        }
        else {
            this.selectionSubject.next(index);
        }
    };
    ComboBoxComponent.prototype.ngOnDestroy = function () {
        this._toggle(false);
        this.unsubscribeEvents();
        if (this.localizationChangeSubscription) {
            this.localizationChangeSubscription.unsubscribe();
        }
    };
    ComboBoxComponent.prototype.ngOnChanges = function (changes) {
        if (changes.hasOwnProperty("value")) {
            this._state |= InternalState.UseModel;
            this._modelValue = changes.value.currentValue;
        }
        if (Object(_util__WEBPACK_IMPORTED_MODULE_19__["isChanged"])("valueNormalizer", changes)) {
            this.createSelectionStream();
            this.createValueStream();
        }
        if (this.valuePrimitive === undefined) {
            this.valuePrimitive = this.valueField ? false : true;
        }
        var STATE_PROPS = /(data|value|textField|valueField|valuePrimitive)/g;
        if (STATE_PROPS.test(Object.keys(changes).join())) {
            this.setState();
        }
        var wasFiltered = Object(_util__WEBPACK_IMPORTED_MODULE_19__["isChanged"])("data", changes) && this._filtering;
        if (wasFiltered) {
            if (this.text.length > 0) {
                this.search(this.text);
                if (this.selectionService.focused === -1) {
                    this.selectionService.focused = 0;
                }
            }
            else {
                this.selectionService.focused = -1;
            }
        }
        if (this.suggest && this.data && this.data.length && this.text) {
            this.suggestedText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this.data[0], this.textField);
        }
    };
    /**
     * Focuses the ComboBox.
     */
    ComboBoxComponent.prototype.focus = function () {
        if (!this.disabled) {
            this.searchbar.focus();
        }
    };
    /**
     * Blurs the ComboBox.
     */
    ComboBoxComponent.prototype.blur = function () {
        if (!this.disabled) {
            this.searchbar.blur();
        }
    };
    /**
     * Toggles the visibility of the popup. If you use the `toggle` method to open or close the popup,
     * the `open` and `close` events will not be fired.
     *
     * @param open - The state of the popup.
     */
    ComboBoxComponent.prototype.toggle = function (open) {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this._toggle((open === undefined) ? !_this._open : open);
            _this.cdr.markForCheck();
        });
    };
    Object.defineProperty(ComboBoxComponent.prototype, "isOpen", {
        /**
         * Returns the current open state of the popup.
         */
        get: function () {
            return this.popupOpen;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Resets the value of the ComboBox.
     * If you use the `reset` method to clear the value of the component,
     * the model will not update automatically and the `selectionChange` and `valueChange` events will not be fired.
     */
    ComboBoxComponent.prototype.reset = function () {
        this._modelValue = undefined;
        this.setState();
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.clearValue = function (event) {
        event.stopImmediatePropagation();
        this.focus();
        this._filtering = true;
        this._previousValue = undefined;
        this.change(undefined);
        this._filtering = false;
        this.selectionService.resetSelection([]);
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.writeValue = function (value) {
        if (value === null && this._state & InternalState.SetInitial) {
            return;
        }
        this._state |= InternalState.UseModel;
        this.text = "";
        this._modelValue = value;
        this.setState();
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.setDisabledState = function (isDisabled) {
        this.disabled = isDisabled;
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.popupOpened = function () {
        this.popupWidth = this.width.max;
        this.popupMinWidth = this.width.min;
    };
    Object.defineProperty(ComboBoxComponent.prototype, "buttonClasses", {
        /**
         * @hidden
         */
        get: function () {
            return _a = {},
                _a[this.iconClass] = !this.loading && this.iconClass,
                _a['k-i-arrow-s'] = !this.loading && !this.iconClass,
                _a['k-i-loading'] = this.loading,
                _a['k-icon'] = true,
                _a;
            var _a;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.onResize = function () {
        if (this._open) {
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
        }
    };
    ComboBoxComponent.prototype.verifySettings = function (newValue) {
        var valueOrText = !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(this.valueField) !== !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(this.textField);
        if (!Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["isDevMode"])()) {
            return;
        }
        if (this.valuePrimitive === true && Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(newValue) && typeof newValue === "object") {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["ComboBoxMessages"].primitive);
        }
        if (this.valuePrimitive === false && Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(newValue) && typeof newValue !== "object") {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["ComboBoxMessages"].object);
        }
        if (valueOrText) {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["ComboBoxMessages"].textAndValue);
        }
    };
    ComboBoxComponent.prototype.resolveState = function () {
        var primitiveValue = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this._modelValue, this.valueField, true);
        var existing;
        var state;
        if (this._filtering) {
            state = InternalState.UseFilter;
        }
        else {
            state = this.data.length ? InternalState.UseExisting : (this.allowCustom ? InternalState.UseCustom : InternalState.UseEmpty);
        }
        if (state === InternalState.UseExisting) {
            existing = Object(_util__WEBPACK_IMPORTED_MODULE_19__["resolveValue"])({ data: this.data, value: primitiveValue, valueField: this.valueField });
            if (existing.dataItem !== undefined) {
                state = InternalState.UseExisting;
            }
            else {
                state = this.allowCustom ? InternalState.UseCustom : InternalState.UseEmpty;
            }
        }
        state |= this._state & InternalState.SetInitial | this._state & InternalState.UseModel;
        this._state = state;
        return existing;
    };
    ComboBoxComponent.prototype.setState = function () {
        var resolved = this.resolveState();
        var newState = { value: undefined, text: undefined, dataItem: undefined, selected: [-1] };
        if (this._state & InternalState.UseFilter) {
            return;
        }
        else if (this._state & InternalState.UseEmpty) {
            newState.selected = undefined;
            newState.value = undefined;
            newState.text = undefined;
            newState.dataItem = undefined;
        }
        else if (this._state & InternalState.UseExisting) {
            newState.value = Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(this._modelValue) ? this._modelValue : resolved.dataItem;
            newState.text = resolved.dataItem;
            newState.dataItem = resolved.dataItem;
            newState.selected = resolved.selected;
        }
        else if (this._state & InternalState.UseCustom) {
            newState.value = this._modelValue;
            newState.text = this._modelValue;
            newState.dataItem = this._modelValue;
        }
        if (this._state & InternalState.UseModel) {
            this._state &= ~InternalState.UseModel;
            this._previousValue = newState.dataItem;
        }
        else {
            this._previousValue = this.dataItem;
        }
        this._state &= ~InternalState.SetInitial;
        this.text = newState.text;
        this.value = this.valuePrimitive ? Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(newState.value, this.valueField, true) : newState.value;
        this.dataItem = newState.dataItem;
        this.selectionService.resetSelection(newState.selected);
    };
    ComboBoxComponent.prototype.search = function (text) {
        var _this = this;
        var index = this.data.findIndex(function (item) {
            var itemText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(item, _this.textField);
            itemText = itemText === undefined ? "" : itemText.toString().toLowerCase();
            return itemText.startsWith(text.toLowerCase());
        });
        this.selectionService.focused = index;
        this.suggestedText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this.data[index], this.textField);
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.getSuggestion = function () {
        var hasSelected = !!this.selectionService.selected.length;
        var shouldSuggest = this.suggest && !this.backspacePressed && this.suggestedText && this.text;
        if (!hasSelected && shouldSuggest && this.suggestedText.toLowerCase().startsWith(this.text.toLowerCase())) {
            return this.suggestedText;
        }
        else {
            this.suggestedText = undefined;
        }
    };
    ComboBoxComponent.prototype.navigate = function (index) {
        if (this.data.length === 0) {
            return;
        }
        this.text = this.data[index];
        this.selectionService.select(index);
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.handleNavigate = function (event) {
        var hasSelected = Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(this.selectionService.selected[0]);
        var focused = isNaN(this.selectionService.focused) ? 0 : this.selectionService.focused;
        var offset = 0;
        if (this.disabled || this.readonly) {
            return;
        }
        if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_21__["Keys"].home || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_21__["Keys"].end) {
            return;
        }
        if (!hasSelected) {
            if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_21__["Keys"].down) {
                offset = -1;
            }
            else if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_21__["Keys"].up) {
                offset = 1;
            }
        }
        var current = offset + focused;
        if (current < -1) {
            current = -1;
        }
        var action = this.navigationService.process({
            current: current,
            max: this.data.length - 1,
            min: 0,
            originalEvent: event
        });
        if (action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Undefined &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Left &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Right &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Backspace &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Delete &&
            ((action === _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Enter && this.popupOpen) || action !== _navigation_action__WEBPACK_IMPORTED_MODULE_20__["NavigationAction"].Enter)) {
            event.preventDefault();
        }
    };
    ComboBoxComponent.prototype.confirmSelection = function () {
        var focused = this.selectionService.focused;
        var previousText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this._previousValue, this.textField) || "";
        var hasChange = this.text !== previousText;
        var isCustom = false;
        this._filtering = false;
        if (this.allowCustom) {
            isCustom = this.text !== Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this.data[focused], this.textField);
        }
        else {
            isCustom = focused === -1 || focused === undefined;
        }
        if (!isCustom && this.popupOpen) {
            this.selectionService.select(focused);
            this.change(this.data[focused] || this.text, isCustom);
            return;
        }
        if (hasChange) {
            this.change(this.text, isCustom);
        }
        else {
            this.popupOpen = false;
        }
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.handleBlur = function () {
        this._filtering = false;
        var currentText = this.searchbar.value; // The value is updated, but the Angular `change` event is not emitted yet and `this.text` is not updated. Fails on suggested text.
        if (!currentText && !Object(_util__WEBPACK_IMPORTED_MODULE_19__["isPresent"])(this._previousValue)) {
            this.popupOpen = false;
            this.isFocused = false;
            this.onBlur.emit();
            this.onTouchedCallback();
            return;
        }
        var focused = this.selectionService.focused;
        var itemText;
        if (focused !== -1 && focused !== undefined) {
            itemText = Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(this.data[focused], this.textField);
            itemText = itemText === undefined ? "" : itemText.toString().toLowerCase();
        }
        if (itemText === currentText.toLowerCase()) {
            this.selectionService.change(focused);
        }
        else {
            this.change(currentText, true);
        }
        this.popupOpen = false;
        this.isFocused = false;
        this.onBlur.emit();
        this.onTouchedCallback();
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.searchBarChange = function (text) {
        var currentTextLength = this.text ? this.text.length : 0;
        this.backspacePressed = (text.length < currentTextLength) ? true : false;
        this.text = text;
        // Reset the selection prior to filter. If a match is present, it will be resolved. If a match is not present, it is not needed.
        this.selectionService.resetSelection([]);
        this.popupOpen = true;
        this._filtering = true;
        if (this.filterable) {
            this.filterChange.emit(text);
        }
        else {
            this.search(text);
        }
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.handleFocus = function () {
        this.isFocused = true;
        this.onFocus.emit();
    };
    ComboBoxComponent.prototype.change = function (candidate, isCustom) {
        if (isCustom === void 0) { isCustom = false; }
        this.popupOpen = false;
        if (isCustom) {
            this.customValueSubject.next(candidate);
        }
        else {
            this.valueSubject.next(candidate);
        }
    };
    ComboBoxComponent.prototype.emitChange = function () {
        var _this = this;
        this._modelValue = this.value;
        this._previousValue = this._state & InternalState.UseCustom ? this.value : this.dataItem;
        this.selectionSubject.next(this.data.findIndex(function (element) {
            return Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(element, _this.valueField) === Object(_util__WEBPACK_IMPORTED_MODULE_19__["getter"])(_this.value, _this.valueField, _this.valuePrimitive);
        }));
        this.onChangeCallback(this.value);
        this.valueChange.emit(this.value);
    };
    /**
     * @hidden
     */
    ComboBoxComponent.prototype.togglePopup = function () {
        if (!this.touchEnabled) {
            this.searchbar.focus();
        }
        if (this.popupOpen) {
            this.confirmSelection();
            this.popupOpen = false;
        }
        else {
            this.popupOpen = true;
        }
    };
    Object.defineProperty(ComboBoxComponent.prototype, "listContainerClasses", {
        get: function () {
            var containerClasses = ['k-list-container', 'k-reset'];
            if (this.popupSettings.popupClass) {
                containerClasses.push(this.popupSettings.popupClass);
            }
            return containerClasses;
        },
        enumerable: true,
        configurable: true
    });
    ComboBoxComponent.prototype._toggle = function (open) {
        var _this = this;
        this._open = open;
        if (this.popupRef) {
            this.popupRef.popupElement
                .removeEventListener('mousedown', this.popupMouseDownHandler);
            this.popupRef.close();
            this.popupRef = null;
        }
        if (this._open) {
            this.popupRef = this.popupService.open({
                anchor: this.wrapper,
                animate: this.popupSettings.animate,
                appendTo: this.appendTo,
                content: this.popupTemplate,
                popupClass: this.listContainerClasses,
                positionMode: 'absolute'
            });
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.addEventListener('mousedown', this.popupMouseDownHandler);
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
            popupWrapper.style.height = this.height;
            popupWrapper.setAttribute("dir", this.direction);
            this.popupRef.popupAnchorViewportLeave.subscribe(function () { return _this.popupOpen = false; });
        }
    };
    ComboBoxComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    exportAs: 'kendoComboBox',
                    providers: [
                        COMBOBOX_VALUE_ACCESSOR,
                        _selection_service__WEBPACK_IMPORTED_MODULE_7__["SelectionService"],
                        _navigation_service__WEBPACK_IMPORTED_MODULE_8__["NavigationService"],
                        _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["LocalizationService"],
                        {
                            provide: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["L10N_PREFIX"],
                            useValue: 'kendo.combobox'
                        }
                    ],
                    selector: 'kendo-combobox',
                    template: "\n        <span #wrapper unselectable=\"on\" [ngClass]=\"wrapperClasses()\">\n          <kendo-searchbar #searchbar\n              [role]=\"'listbox'\"\n              [id]=\"focusableId\"\n              [listId]=\"listBoxId\"\n              [activeDescendant]=\"activeDescendant\"\n              [userInput]=\"text\"\n              [suggestedText]=\"getSuggestion()\"\n              [disabled]=\"disabled\"\n              [readonly]=\"readonly\"\n              [tabIndex]=\"tabIndex\"\n              [popupOpen]=\"popupOpen\"\n              [placeholder]=\"placeholder\"\n              (onNavigate)=\"handleNavigate($event)\"\n              (valueChange)=\"searchBarChange($event)\"\n              (onBlur)=\"handleBlur()\"\n              (onFocus)=\"handleFocus()\"\n          ></kendo-searchbar>\n          <span *ngIf=\"!loading && !readonly && (clearButton && text?.length)\" class=\"k-icon k-clear-value k-i-close\" title=\"clear\" role=\"button\" tabindex=\"-1\" (click)=\"clearValue($event)\" (mousedown)=\"$event.preventDefault()\"></span>\n          <span unselectable=\"on\"\n              [ngClass]=\"{ 'k-select': true }\"\n              (click)=\"togglePopup()\"\n              (mousedown)=\"$event.preventDefault()\" >\n              <span [ngClass]=\"buttonClasses\"></span>\n          </span>\n          <ng-template #popupTemplate>\n              <!--header template-->\n              <ng-template *ngIf=\"headerTemplate\"\n                  [templateContext]=\"{\n                      templateRef: headerTemplate.templateRef\n                  }\">\n              </ng-template>\n              <!--list-->\n              <kendo-list\n                  [id]=\"listBoxId\"\n                  [optionPrefix]=\"optionPrefix\"\n                  [data]=\"data\"\n                  [textField]=\"textField\"\n                  [valueField]=\"valueField\"\n                  [template]=\"template\"\n                  [height]=\"listHeight\"\n                  [show]=\"popupOpen\"\n              >\n              </kendo-list>\n              <!--no-data template-->\n              <div class=\"k-nodata\" *ngIf=\"data.length === 0\">\n                  <ng-template [ngIf]=\"noDataTemplate\"\n                      [templateContext]=\"{\n                          templateRef: noDataTemplate ? noDataTemplate.templateRef : undefined\n                      }\">\n                  </ng-template>\n                  <ng-template [ngIf]=\"!noDataTemplate\">\n                      <div>NO DATA FOUND.</div>\n                  </ng-template>\n              </div>\n              <!--footer template-->\n              <ng-template *ngIf=\"footerTemplate\"\n                  [templateContext]=\"{\n                      templateRef: footerTemplate.templateRef\n                  }\">\n              </ng-template>\n          </ng-template>\n        </span>\n        <ng-template [ngIf]=\"popupOpen\">\n            <kendo-resize-sensor (resize)=\"onResize()\"></kendo-resize-sensor>\n        </ng-template>\n        <ng-container #container></ng-container>\n  "
                },] },
    ];
    /** @nocollapse */
    ComboBoxComponent.ctorParameters = function () { return [
        { type: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["LocalizationService"], },
        { type: _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__["PopupService"], },
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_7__["SelectionService"], },
        { type: _navigation_service__WEBPACK_IMPORTED_MODULE_8__["NavigationService"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_touch_enabled__WEBPACK_IMPORTED_MODULE_25__["TOUCH_ENABLED"],] },] },
    ]; };
    ComboBoxComponent.propDecorators = {
        'focusableId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'allowCustom': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'textField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valuePrimitive': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueNormalizer': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'placeholder': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'popupSettings': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'listHeight': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'iconClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'loading': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'suggest': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'clearButton': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'readonly': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabindex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabIndex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ["tabIndex",] },],
        'filterable': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'selectionChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'filterChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'open': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'close': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['focus',] },],
        'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['blur',] },],
        'template': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_item_template_directive__WEBPACK_IMPORTED_MODULE_3__["ItemTemplateDirective"],] },],
        'headerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_header_template_directive__WEBPACK_IMPORTED_MODULE_4__["HeaderTemplateDirective"],] },],
        'footerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__["FooterTemplateDirective"],] },],
        'noDataTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_6__["NoDataTemplateDirective"],] },],
        'container': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['container', { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] },] },],
        'popupTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['popupTemplate',] },],
        'searchbar': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_searchbar_component__WEBPACK_IMPORTED_MODULE_2__["SearchBarComponent"],] },],
        'widgetClasses': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-widget',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-combobox',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-header',] },],
        'clearable': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-combobox-clearable',] },],
        'dir': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.dir',] },],
    };
    return ComboBoxComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.module.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.module.js ***!
  \***********************************************************************************/
/*! exports provided: ComboBoxModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComboBoxModule", function() { return ComboBoxModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _combobox_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./combobox.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.component.js");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js");
/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");




var COMBOBOX_DIRECTIVES = [
    _combobox_component__WEBPACK_IMPORTED_MODULE_1__["ComboBoxComponent"]
];
/**
 * @hidden
 *
 * The exported package module.
 *
 * The package exports:
 * - `ComboBoxComponent`&mdash;The ComboBox component class.
 * - `ItemTemplateDirective`&mdash;The item template directive.
 * - `HeaderTemplateDirective`&mdash;The header template directive.
 * - `FooterTemplateDirective`&mdash;The footer template directive.
 */
var ComboBoxModule = /** @class */ (function () {
    function ComboBoxModule() {
    }
    ComboBoxModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [COMBOBOX_DIRECTIVES],
                    exports: [COMBOBOX_DIRECTIVES, _shared_directives_module__WEBPACK_IMPORTED_MODULE_3__["SharedDirectivesModule"]],
                    imports: [_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]]
                },] },
    ];
    /** @nocollapse */
    ComboBoxModule.ctorParameters = function () { return []; };
    return ComboBoxModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js ***!
  \*******************************************************************************/
/*! exports provided: Keys */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Keys", function() { return Keys; });
/**
 * @hidden
 */
var Keys;
(function (Keys) {
    Keys[Keys["backspace"] = 8] = "backspace";
    Keys[Keys["tab"] = 9] = "tab";
    Keys[Keys["enter"] = 13] = "enter";
    Keys[Keys["shift"] = 16] = "shift";
    Keys[Keys["ctrl"] = 17] = "ctrl";
    Keys[Keys["alt"] = 18] = "alt";
    Keys[Keys["pause/break"] = 19] = "pause/break";
    Keys[Keys["caps lock"] = 20] = "caps lock";
    Keys[Keys["esc"] = 27] = "esc";
    Keys[Keys["space"] = 32] = "space";
    Keys[Keys["page up"] = 33] = "page up";
    Keys[Keys["page down"] = 34] = "page down";
    Keys[Keys["end"] = 35] = "end";
    Keys[Keys["home"] = 36] = "home";
    Keys[Keys["left"] = 37] = "left";
    Keys[Keys["up"] = 38] = "up";
    Keys[Keys["right"] = 39] = "right";
    Keys[Keys["down"] = 40] = "down";
    Keys[Keys["insert"] = 45] = "insert";
    Keys[Keys["delete"] = 46] = "delete";
    Keys[Keys["command"] = 91] = "command";
    Keys[Keys["left command"] = 91] = "left command";
    Keys[Keys["right command"] = 93] = "right command";
    Keys[Keys["numpad *"] = 106] = "numpad *";
    Keys[Keys["numpad +"] = 107] = "numpad +";
    Keys[Keys["numpad -"] = 109] = "numpad -";
    Keys[Keys["numpad ."] = 110] = "numpad .";
    Keys[Keys["numpad /"] = 111] = "numpad /";
    Keys[Keys["num lock"] = 144] = "num lock";
    Keys[Keys["scroll lock"] = 145] = "scroll lock";
    Keys[Keys["my computer"] = 182] = "my computer";
    Keys[Keys["my calculator"] = 183] = "my calculator";
    Keys[Keys[";"] = 186] = ";";
    Keys[Keys["="] = 187] = "=";
    Keys[Keys[","] = 188] = ",";
    Keys[Keys["-"] = 189] = "-";
    Keys[Keys["."] = 190] = ".";
    Keys[Keys["/"] = 191] = "/";
    Keys[Keys["`"] = 192] = "`";
    Keys[Keys["["] = 219] = "[";
    Keys[Keys["\\"] = 220] = "\\";
    Keys[Keys["]"] = 221] = "]";
    Keys[Keys["'"] = 222] = "'";
})(Keys || (Keys = {}));


/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js ***!
  \********************************************************************************************/
/*! exports provided: PreventableEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreventableEvent", function() { return PreventableEvent; });
/**
 * @hidden
 */
var PreventableEvent = /** @class */ (function () {
    function PreventableEvent() {
        this.prevented = false;
    }
    /**
     * Prevents the default action for a specified event.
     * In this way, the source component suppresses the built-in behavior that follows the event.
     */
    PreventableEvent.prototype.preventDefault = function () {
        this.prevented = true;
    };
    /**
     * If the event is prevented by any of its subscribers, returns `true`.
     *
     * @returns `true` if the default action was prevented. Otherwise, returns `false`.
     */
    PreventableEvent.prototype.isDefaultPrevented = function () {
        return this.prevented;
    };
    return PreventableEvent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/remove-tag-event.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/remove-tag-event.js ***!
  \*******************************************************************************************/
/*! exports provided: RemoveTagEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemoveTagEvent", function() { return RemoveTagEvent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _preventable_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");


/**
 * Fires when a tag is about to the removed. If you cancel the event, the removal is prevented.
 *
 * Arguments for the `remove` event:
 */
var RemoveTagEvent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RemoveTagEvent, _super);
    /**
     * Constructs the event arguments for the `remove` event.
     * @param dataItem - The dataItem(s) that will be removed.
     */
    function RemoveTagEvent(dataItem) {
        var _this = _super.call(this) || this;
        _this.dataItem = dataItem;
        return _this;
    }
    return RemoveTagEvent;
}(_preventable_event__WEBPACK_IMPORTED_MODULE_1__["PreventableEvent"]));



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.component.js":
/*!******************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.component.js ***!
  \******************************************************************************************/
/*! exports provided: DROPDOWNLIST_VALUE_ACCESSOR, DropDownListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DROPDOWNLIST_VALUE_ACCESSOR", function() { return DROPDOWNLIST_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownListComponent", function() { return DropDownListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/observable/merge */ "./node_modules/rxjs-compat/_esm5/observable/merge.js");
/* harmony import */ var rxjs_observable_interval__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/observable/interval */ "./node_modules/rxjs-compat/_esm5/observable/interval.js");
/* harmony import */ var rxjs_operators_concatMap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators/concatMap */ "./node_modules/rxjs-compat/_esm5/operators/concatMap.js");
/* harmony import */ var rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators/filter */ "./node_modules/rxjs-compat/_esm5/operators/filter.js");
/* harmony import */ var rxjs_operators_map__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators/map */ "./node_modules/rxjs-compat/_esm5/operators/map.js");
/* harmony import */ var rxjs_operators_skipWhile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators/skipWhile */ "./node_modules/rxjs-compat/_esm5/operators/skipWhile.js");
/* harmony import */ var rxjs_operators_take__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators/take */ "./node_modules/rxjs-compat/_esm5/operators/take.js");
/* harmony import */ var rxjs_operators_takeUntil__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators/takeUntil */ "./node_modules/rxjs-compat/_esm5/operators/takeUntil.js");
/* harmony import */ var rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators/tap */ "./node_modules/rxjs-compat/_esm5/operators/tap.js");
/* harmony import */ var rxjs_Subscription__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/Subscription */ "./node_modules/rxjs-compat/_esm5/Subscription.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony import */ var _navigation_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./navigation.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js");
/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony import */ var _templates_value_template_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./templates/value-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/value-template.directive.js");
/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");
/* harmony import */ var _navigation_action__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./navigation-action */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js");
/* harmony import */ var _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./common/preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");
/* harmony import */ var _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @progress/kendo-angular-l10n */ "./node_modules/@progress/kendo-angular-l10n/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* harmony import */ var _touch_enabled__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./touch-enabled */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/touch-enabled.js");
/* harmony import */ var _error_messages__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./error-messages */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/error-messages.js");
/* tslint:disable:max-line-length */
/* tslint:disable:no-bitwise */



























/**
 * @hidden
 */
var DROPDOWNLIST_VALUE_ACCESSOR = {
    multi: true,
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    // tslint:disable-next-line:no-use-before-declare
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return DropDownListComponent; })
};
/**
 * Represents the Kendo UI DropDownList component for Angular.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-dropdownlist [data]="listItems">
 *  </kendo-dropdownlist>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 */
var DropDownListComponent = /** @class */ (function () {
    function DropDownListComponent(localization, popupService, selectionService, navigationService, _zone, renderer, hostElement, cdr, touchEnabled) {
        this.localization = localization;
        this.popupService = popupService;
        this.selectionService = selectionService;
        this.navigationService = navigationService;
        this._zone = _zone;
        this.renderer = renderer;
        this.hostElement = hostElement;
        this.cdr = cdr;
        this.touchEnabled = touchEnabled;
        /**
         * @hidden
         */
        this.focusableId = "k-" + Object(_util__WEBPACK_IMPORTED_MODULE_13__["guid"])();
        /**
         * Sets the height of the options list. By default, `listHeight` is 200px.
         *
         * > The `listHeight` property affects only the list of options and not the whole popup container.
         * > To set the height of the popup container, use `popupSettings.height`.
         */
        this.listHeight = 200;
        /**
         * Sets the disabled state of the component.
         */
        this.disabled = false;
        /**
         * Sets the readonly state of the component.
         */
        this.readonly = false;
        /**
         * Enables the [filtering]({% slug filtering_ddl %}) functionality of the DropDownList.
         */
        this.filterable = false;
        /**
         * Enables a case-insensitive search. When filtration is disabled, use this option.
         */
        this.ignoreCase = true;
        /**
         * Sets the delay before an item search is performed. When filtration is disabled, use this option.
         */
        this.delay = 500;
        /**
         * Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) of the component.
         */
        this.tabindex = 0;
        /**
         * Fires each time the value is changed&mdash;
         * when the component is blurred or the value is cleared through the **Clear** button.
         * For more details, refer to the example on [events]({% slug overview_ddl %}#toc-events).
         */
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user types in the input field.
         * You can filter the source based on the passed filtration value.
         * For more details, refer to the example on [events]({% slug overview_ddl %}#toc-events).
         *
         * When the value of the component is programmatically changed through its API or form binding
         * to `ngModel` or `formControl`, the `valueChange` event is not triggered because it
         * might cause a mix-up with the built-in `valueChange` mechanisms of the `ngModel` or `formControl` bindings.
         */
        this.filterChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the item selection is changed.
         * For more details, refer to the example on [events]({% slug overview_ddl %}#toc-events).
         */
        this.selectionChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to open.
         * This event is preventable. If you cancel it, the popup will remain closed.
         */
        this.open = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the popup is about to close.
         * This event is preventable. If you cancel it, the popup will remain open.
         */
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires each time the user focuses the DropDownList.
         */
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        /**
         * Fires each time the DropDownList gets blurred.
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); //tslint:disable-line:no-output-rename
        this.listBoxId = Object(_util__WEBPACK_IMPORTED_MODULE_13__["guid"])();
        this.optionPrefix = Object(_util__WEBPACK_IMPORTED_MODULE_13__["guid"])();
        this.filterText = "";
        this.isFocused = false;
        this.onTouchedCallback = function (_) { };
        this.onChangeCallback = function (_) { };
        this.word = "";
        this.last = "";
        this.filterFocused = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.filterBlurred = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.wrapperFocused = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.wrapperBlurred = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectionSubscription = new rxjs_Subscription__WEBPACK_IMPORTED_MODULE_11__["Subscription"]();
        this._open = false;
        this._popupSettings = { animate: true };
        this.direction = localization.rtl ? 'rtl' : 'ltr';
        this.data = [];
        this.subscribeEvents();
        this.hostElement = hostElement.nativeElement;
        this.popupMouseDownHandler = this.onMouseDown.bind(this);
    }
    Object.defineProperty(DropDownListComponent.prototype, "width", {
        get: function () {
            var wrapperWidth = Object(_util__WEBPACK_IMPORTED_MODULE_13__["isDocumentAvailable"])() ? this.wrapper.nativeElement.offsetWidth : 0;
            var width = this.popupSettings.width || wrapperWidth;
            var minWidth = isNaN(wrapperWidth) ? wrapperWidth : wrapperWidth + "px";
            var maxWidth = isNaN(width) ? width : width + "px";
            return { min: minWidth, max: maxWidth };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "height", {
        get: function () {
            var popupHeight = this.popupSettings.height;
            return Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(popupHeight) ? popupHeight + "px" : 'auto';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "popupOpen", {
        get: function () {
            return this._open;
        },
        set: function (open) {
            if (this.disabled || this.readonly || this.popupOpen === open) {
                return;
            }
            var eventArgs = new _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__["PreventableEvent"]();
            if (open) {
                this.open.emit(eventArgs);
            }
            else {
                this.close.emit(eventArgs);
                if (this.filterable && this.isFocused) {
                    this.focus();
                }
            }
            if (eventArgs.isDefaultPrevented()) {
                return;
            }
            this._toggle(open);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "widgetTabIndex", {
        get: function () {
            return this.disabled ? undefined : this.tabIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "ariaHasPopup", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "ariaExpanded", {
        get: function () {
            return this.popupOpen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "ariaOwns", {
        get: function () {
            return this.listBoxId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "ariaActivedescendant", {
        get: function () {
            return this.optionPrefix + "-" + Object(_util__WEBPACK_IMPORTED_MODULE_13__["getter"])(this.value, this.valueField);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "appendTo", {
        get: function () {
            var appendTo = this.popupSettings.appendTo;
            if (!appendTo || appendTo === 'root') {
                return undefined;
            }
            return appendTo === 'component' ? this.container : appendTo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        /**
         * Sets the data of the DropDownList.
         *
         * > The data has to be provided in an array-like list.
         */
        set: function (data) {
            this._data = data || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        /**
         * Sets the value of the DropDownList. It can either be of the primitive (string, numbers) or of the complex (objects) type. To define the type, use the `valuePrimitive` option.
         *
         * > All selected values which are not present in the source are ignored.
         */
        set: function (newValue) {
            this._value = newValue;
            this.cdr.markForCheck();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "popupSettings", {
        get: function () {
            return this._popupSettings;
        },
        /**
         * Configures the popup of the DropDownList.
         *
         * The available options are:
         * - `animate: Boolean`&mdash;Controls the popup animation. By default, the open and close animations are enabled.
         * - `width: Number`&mdash;Sets the width of the popup container. By default, the width of the host element is used.
         * - `height: Number`&mdash;Sets the height of the popup container.
         * - `popupClass: String`&mdash;Specifies a list of CSS classes that are used to style the popup.
         */
        set: function (settings) {
            this._popupSettings = Object.assign({ animate: true }, settings);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "tabIndex", {
        get: function () {
            return this.tabindex;
        },
        /**
         * @hidden
         */
        set: function (tabIndex) {
            this.tabindex = tabIndex;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    DropDownListComponent.prototype.blurComponent = function () {
        this.wrapperBlurred.emit();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.blurFilterInput = function () {
        this.filterBlurred.emit();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.focusComponent = function () {
        this.wrapperFocused.emit();
        if (!this.isFocused) {
            this.isFocused = true;
            this.onFocus.emit();
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.keydown = function (event) {
        var hasSelected = Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(this.selectionService.selected[0]);
        var focused = isNaN(this.selectionService.focused) ? 0 : this.selectionService.focused;
        var offset = 0;
        if (this.disabled || this.readonly) {
            return;
        }
        if (this.filterInput && this.filterInput.nativeElement === document.activeElement && (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].home || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].end)) {
            return;
        }
        if (!hasSelected) {
            if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].down || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].right) {
                offset = -1;
            }
            else if (event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].up || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].left) {
                offset = 1;
            }
        }
        var eventData = event;
        var action = this.navigationService.process({
            current: focused + offset,
            max: this.data.length - 1,
            min: this.defaultItem ? -1 : 0,
            originalEvent: eventData
        });
        var leftRightKeys = (action === _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Left) || (action === _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Right);
        if (action !== _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Undefined &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Tab &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Backspace &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Delete &&
            !(leftRightKeys && this.filterable) &&
            action !== _navigation_action__WEBPACK_IMPORTED_MODULE_21__["NavigationAction"].Enter //enter when popup is opened is handled before `handleEnter`
        ) {
            eventData.preventDefault();
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.keypress = function (event) {
        if (!this.filterable) {
            this.onKeyPress(event);
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.click = function () {
        this.wrapper.nativeElement.focus();
        this.popupOpen = !this.popupOpen;
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.onResize = function () {
        if (this._open) {
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
        }
    };
    Object.defineProperty(DropDownListComponent.prototype, "widgetClasses", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "dir", {
        get: function () {
            return this.direction;
        },
        enumerable: true,
        configurable: true
    });
    DropDownListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderer.removeAttribute(this.hostElement, "tabindex");
        this.localizationChangesSubscription = this.localization
            .changes.subscribe(function (_a) {
            var rtl = _a.rtl;
            return _this.direction = rtl ? 'rtl' : 'ltr';
        });
    };
    /**
     * @hidden
     * Used by the TextBoxContainer to determine if the component is empty.
     */
    DropDownListComponent.prototype.isEmpty = function () {
        return !this.value && !this.defaultItem;
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.onFilterFocus = function () {
        this.filterFocused.emit();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.popupOpened = function () {
        if (this.filterInput && !this.touchEnabled) {
            var nativeElement_1 = this.filterInput.nativeElement;
            var text_1 = this.filterText || nativeElement_1.value;
            this.nextTick(function () {
                nativeElement_1.focus();
                nativeElement_1.setSelectionRange(text_1.length, text_1.length);
            });
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.ngOnDestroy = function () {
        this._toggle(false);
        this.unsubscribeEvents();
        if (this.localizationChangesSubscription) {
            this.localizationChangesSubscription.unsubscribe();
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.ngOnChanges = function (_changes) {
        if (this.valuePrimitive === undefined) {
            this.valuePrimitive = this.valueField ? false : true;
        }
        if (_changes.hasOwnProperty("value")) {
            this.verifySettings(_changes.value.currentValue);
            if (!Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(this.value)) {
                this._previousDataItem = undefined;
            }
        }
        var STATE_PROPS = /(data|value|textField|valueField|valuePrimitive)/g;
        if (STATE_PROPS.test(Object.keys(_changes).join())) {
            this.setState();
        }
    };
    /**
     * Focuses the DropDownList.
     */
    DropDownListComponent.prototype.focus = function () {
        if (!this.disabled) {
            this.wrapper.nativeElement.focus();
        }
    };
    /**
     * Blurs the DropDownList.
     */
    DropDownListComponent.prototype.blur = function () {
        if (!this.disabled) {
            this.wrapper.nativeElement.blur();
        }
    };
    /**
     * Toggles the visibility of the popup. If you use the `toggle` method to open or close the popup, the `open` and `close` events will not be fired.
     *
     * @param open - The state of the popup.
     */
    DropDownListComponent.prototype.toggle = function (open) {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this._toggle((open === undefined) ? !_this._open : open);
            _this.cdr.markForCheck();
        });
    };
    Object.defineProperty(DropDownListComponent.prototype, "isOpen", {
        /**
         * Returns the current open state of the popup.
         */
        get: function () {
            return this.popupOpen;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Resets the value of the DropDownList.
     * If you use the `reset` method to clear the value of the component,
     * the model will not update automatically and the `selectionChange` and `valueChange` events will not be fired.
     */
    DropDownListComponent.prototype.reset = function () {
        this.value = undefined;
        this.setState();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.writeValue = function (value) {
        this.verifySettings(value);
        this.value = value === null ? undefined : value;
        if (value === null) {
            this._previousDataItem = undefined;
        }
        this.setState();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.setDisabledState = function (isDisabled) {
        this.disabled = isDisabled;
    };
    Object.defineProperty(DropDownListComponent.prototype, "listContainerClasses", {
        /**
         * @hidden
         */
        get: function () {
            var containerClasses = ['k-list-container', 'k-reset'];
            if (this.popupSettings.popupClass) {
                containerClasses.push(this.popupSettings.popupClass);
            }
            return containerClasses;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropDownListComponent.prototype, "buttonClasses", {
        /**
         * @hidden
         */
        get: function () {
            return _a = {},
                _a[this.iconClass] = !this.loading && this.iconClass,
                _a['k-i-arrow-s'] = !this.loading && !this.iconClass,
                _a['k-i-loading'] = this.loading,
                _a['k-icon'] = true,
                _a;
            var _a;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    DropDownListComponent.prototype.setDefaultItemClasses = function () {
        return {
            'k-list-optionlabel': true
        };
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.getText = function () {
        return this.text;
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.getDefaultItemText = function () {
        return Object(_util__WEBPACK_IMPORTED_MODULE_13__["getter"])(this.defaultItem, this.textField);
    };
    DropDownListComponent.prototype._toggle = function (open) {
        var _this = this;
        this._open = open;
        if (this.popupRef) {
            this.popupRef.popupElement
                .removeEventListener('mousedown', this.popupMouseDownHandler);
            this.popupRef.close();
            this.popupRef = null;
        }
        if (this._open) {
            var horizontalAlign = this.direction === "rtl" ? "right" : "left";
            var anchorPosition = { horizontal: horizontalAlign, vertical: "bottom" };
            var popupPosition = { horizontal: horizontalAlign, vertical: "top" };
            this.popupRef = this.popupService.open({
                anchor: this.wrapper,
                anchorAlign: anchorPosition,
                animate: this.popupSettings.animate,
                appendTo: this.appendTo,
                content: this.popupTemplate,
                popupAlign: popupPosition,
                popupClass: this.listContainerClasses,
                positionMode: 'absolute'
            });
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.addEventListener('mousedown', this.popupMouseDownHandler);
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
            popupWrapper.style.height = this.height;
            popupWrapper.setAttribute("dir", this.direction);
            this.popupRef.popupOpen.subscribe(this.popupOpened.bind(this));
            this.popupRef.popupAnchorViewportLeave.subscribe(function () { return _this.popupOpen = false; });
        }
    };
    DropDownListComponent.prototype.updateState = function (_a) {
        var dataItem = _a.dataItem, _b = _a.confirm, confirm = _b === void 0 ? false : _b;
        this.dataItem = dataItem;
        this.text = this.prop(this.textField, this.valuePrimitive)(dataItem);
        if (confirm) {
            this._previousDataItem = dataItem;
        }
    };
    DropDownListComponent.prototype.clearState = function () {
        this.text = undefined;
        this.dataItem = undefined;
    };
    DropDownListComponent.prototype.resetSelection = function (index) {
        var clear = !Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(index);
        this.selectionService.resetSelection(clear ? [] : [index]);
        this.selectionService.focused = clear ? 0 : index;
    };
    DropDownListComponent.prototype.onSelectionChange = function (_a) {
        var dataItem = _a.dataItem;
        this.updateState({ dataItem: dataItem });
        this.selectionChange.emit(dataItem);
    };
    DropDownListComponent.prototype.subscribeEvents = function () {
        var _this = this;
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_13__["isDocumentAvailable"])()) {
            return;
        }
        // item selection when popup is open
        this.selectionSubscription.add(this.selectionService.onSelect.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (_) { return _this.popupOpen; }), Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_6__["map"])(this.itemFromEvent.bind(this)))
            .subscribe(this.onSelectionChange.bind(this)));
        // item selection when popup is closed | clicked | enter etc.
        this.selectionSubscription.add(Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_2__["merge"])(this.selectionService.onSelect.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (_) { return !_this.popupOpen; })), this.selectionService.onChange).pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_6__["map"])(this.itemFromEvent.bind(this)), Object(rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_10__["tap"])(function (_) { return _this.popupOpen = false; }))
            .subscribe(function (_a) {
            var dataItem = _a.dataItem, index = _a.index, newValue = _a.value;
            var isIndexSelected = Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(index) && !_this.selectionService.isSelected(index);
            if (isIndexSelected) {
                _this.onSelectionChange({ dataItem: dataItem });
            }
            var shouldUsePrevious = !Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(dataItem) && _this._previousDataItem;
            var shouldUseNewValue = newValue !== _this.prop(_this.valueField, _this.valuePrimitive)(_this.value);
            if (shouldUsePrevious) {
                _this.updateState({ dataItem: _this._previousDataItem });
                _this.resetSelection();
            }
            else if (shouldUseNewValue) {
                _this.value = _this.valuePrimitive ? newValue : dataItem;
                _this._previousDataItem = dataItem;
                _this.emitChange(_this.value);
            }
            if (_this.filterable && _this.filterText) {
                _this.filterText = "";
                _this.cdr.markForCheck();
                _this.filterChange.emit(_this.filterText);
            }
        }));
        this.navigationSubscription = Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_2__["merge"])(this.navigationService.up, this.navigationService.down, this.navigationService.left.pipe(Object(rxjs_operators_skipWhile__WEBPACK_IMPORTED_MODULE_7__["skipWhile"])(function () { return _this.filterable; })), this.navigationService.right.pipe(Object(rxjs_operators_skipWhile__WEBPACK_IMPORTED_MODULE_7__["skipWhile"])(function () { return _this.filterable; })), this.navigationService.home, this.navigationService.end)
            .subscribe(function (event) { return _this.selectionService.select(event.index); });
        this.openSubscription = this.navigationService.open.subscribe(function () { return _this.popupOpen = true; });
        this.closeSubscription = this.navigationService.close.subscribe(function () {
            _this.popupOpen = false;
            _this.wrapper.nativeElement.focus();
        });
        this.enterSubscription =
            Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_2__["merge"])(this.navigationService.enter.pipe(Object(rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_10__["tap"])(function (event) {
                if (_this.popupOpen) {
                    event.originalEvent.preventDefault();
                }
            })), this.navigationService.esc)
                .subscribe(this.handleEnter.bind(this));
        this.filterBlurredSubscription = this.filterBlurred.pipe(Object(rxjs_operators_concatMap__WEBPACK_IMPORTED_MODULE_4__["concatMap"])(function () { return Object(rxjs_observable_interval__WEBPACK_IMPORTED_MODULE_3__["interval"])(10).pipe(Object(rxjs_operators_take__WEBPACK_IMPORTED_MODULE_8__["take"])(1), Object(rxjs_operators_takeUntil__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(_this.wrapperFocused)); }))
            .subscribe(function () {
            _this.wrapperBlurred.emit();
        });
        this._zone.runOutsideAngular(function () {
            _this.componentBlurredSubscription =
                Object(rxjs_observable_merge__WEBPACK_IMPORTED_MODULE_2__["merge"])(_this.wrapperBlurred.pipe(Object(rxjs_operators_concatMap__WEBPACK_IMPORTED_MODULE_4__["concatMap"])(function () { return Object(rxjs_observable_interval__WEBPACK_IMPORTED_MODULE_3__["interval"])(10).pipe(Object(rxjs_operators_take__WEBPACK_IMPORTED_MODULE_8__["take"])(1), Object(rxjs_operators_takeUntil__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(_this.filterFocused)); })), _this.navigationService.tab).pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_5__["filter"])(function () { return _this.isFocused; }))
                    .subscribe(function () { return _this._zone.run(function () {
                    _this.componentBlur();
                }); });
        });
    };
    DropDownListComponent.prototype.unsubscribeEvents = function () {
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_13__["isDocumentAvailable"])()) {
            return;
        }
        this.navigationSubscription.unsubscribe();
        this.openSubscription.unsubscribe();
        this.closeSubscription.unsubscribe();
        this.enterSubscription.unsubscribe();
        this.componentBlurredSubscription.unsubscribe();
        this.filterBlurredSubscription.unsubscribe();
        if (this.selectionSubscription) {
            this.selectionSubscription.unsubscribe();
        }
    };
    DropDownListComponent.prototype.itemFromEvent = function (event) {
        var index = event.indices[0];
        var dataItem = Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(this.data[index]) ? this.data[index] : this.defaultItem;
        var value = this.prop(this.valueField, this.valuePrimitive)(dataItem);
        return {
            dataItem: dataItem,
            index: index,
            value: value
        };
    };
    DropDownListComponent.prototype.handleEnter = function () {
        if (this.popupOpen) {
            this.selectionService.change(this.selectionService.focused);
            this.wrapper.nativeElement.focus();
        }
    };
    DropDownListComponent.prototype.verifySettings = function (newValue) {
        var valueOrText = !Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(this.valueField) !== !Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(this.textField);
        if (!Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["isDevMode"])()) {
            return;
        }
        if (this.defaultItem && this.valueField && typeof this.defaultItem !== "object") {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["DropDownListMessages"].defaultItem);
        }
        if (this.valuePrimitive === true && Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(newValue) && typeof newValue === "object") {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["DropDownListMessages"].primitive);
        }
        if (this.valuePrimitive === false && Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(newValue) && typeof newValue !== "object") {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["DropDownListMessages"].object);
        }
        if (valueOrText) {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_26__["DropDownListMessages"].textAndValue);
        }
    };
    DropDownListComponent.prototype.componentBlur = function () {
        this.isFocused = false;
        this.selectionService.change(this.selectionService.selected[0]);
        this.onBlur.emit();
        this.onTouchedCallback();
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.onMouseDown = function (event) {
        var tagName = event.target.tagName.toLowerCase();
        if (tagName !== "input") {
            event.preventDefault();
        }
    };
    DropDownListComponent.prototype.onKeyPress = function (event) {
        if (event.which === 0 || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_12__["Keys"].enter) {
            return;
        }
        var character = String.fromCharCode(event.charCode || event.keyCode);
        if (this.ignoreCase) {
            character = character.toLowerCase();
        }
        if (character === " ") {
            event.preventDefault();
        }
        this.word += character;
        this.last = character;
        this.search();
    };
    DropDownListComponent.prototype.search = function () {
        var _this = this;
        clearTimeout(this.typingTimeout);
        if (!this.filterable) {
            this.typingTimeout = setTimeout(function () { _this.word = ""; }, this.delay);
            this.selectNext();
        }
    };
    DropDownListComponent.prototype.selectNext = function () {
        var data = this.data.map(function (item, index) {
            return { item: item, itemIndex: index };
        });
        var isInLoop = Object(_util__WEBPACK_IMPORTED_MODULE_13__["sameCharsOnly"])(this.word, this.last);
        var dataLength = data.length;
        var startIndex = isNaN(this.selectionService.selected[0]) ? 0 : this.selectionService.selected[0];
        var text, index, defaultItem;
        if (this.defaultItem) {
            defaultItem = { item: this.defaultItem, itemIndex: -1 };
            dataLength += 1;
            startIndex += 1;
        }
        startIndex += isInLoop ? 1 : 0;
        data = Object(_util__WEBPACK_IMPORTED_MODULE_13__["shuffleData"])(data, startIndex, defaultItem);
        index = 0;
        for (; index < dataLength; index++) {
            text = Object(_util__WEBPACK_IMPORTED_MODULE_13__["getter"])(data[index].item, this.textField);
            var loopMatch = Boolean(isInLoop && Object(_util__WEBPACK_IMPORTED_MODULE_13__["matchText"])(text, this.last, this.ignoreCase));
            var nextMatch = Boolean(Object(_util__WEBPACK_IMPORTED_MODULE_13__["matchText"])(text, this.word, this.ignoreCase));
            if (loopMatch || nextMatch) {
                index = data[index].itemIndex;
                break;
            }
        }
        if (index !== dataLength) {
            this.navigate(index);
        }
    };
    DropDownListComponent.prototype.emitChange = function (value) {
        this.onChangeCallback(value);
        this.valueChange.emit(value);
    };
    DropDownListComponent.prototype.navigate = function (index) {
        this.selectionService.select(index);
    };
    DropDownListComponent.prototype.prop = function (field, usePrimitive) {
        return function (dataItem) {
            if (Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(dataItem)) {
                if (usePrimitive) {
                    return field && dataItem.hasOwnProperty(field) ? dataItem[field] : dataItem;
                }
                else {
                    return dataItem[field];
                }
            }
            return null;
        };
    };
    DropDownListComponent.prototype.findDataItem = function (_a) {
        var primitive = _a.primitive, valueField = _a.valueField, data = _a.data, value = _a.value;
        var result = {
            dataItem: null,
            index: -1
        };
        var prop = this.prop(valueField, primitive);
        var mappedData = data.map(prop);
        var index = mappedData.indexOf(prop(value));
        result.dataItem = data[index];
        result.index = index;
        return result;
    };
    DropDownListComponent.prototype.setState = function () {
        var value = this.value;
        var valueField = this.valueField;
        var textField = this.textField;
        var primitive = this.valuePrimitive;
        var data = this.data;
        if (this.defaultItem) {
            var defaultValue = this.prop(valueField, primitive)(this.defaultItem);
            var currentValue = this.prop(valueField, primitive)(value);
            if (!Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(value) || (currentValue === defaultValue)) {
                this.updateState({ dataItem: this.defaultItem, confirm: true });
                this.resetSelection(-1);
                return;
            }
        }
        var resolved = this.findDataItem({ primitive: primitive, valueField: valueField, data: data, value: value });
        // the data and value are of same shape
        // e.g value: 'foo', data: ['foo', 'bar']
        // or value: { value: 1, text: 'foo' }, data: [{ value: 1, text: 'foo' }]
        var ofSameType = !(primitive && textField);
        if (resolved.dataItem) {
            this.updateState({ dataItem: resolved.dataItem, confirm: true });
            this.resetSelection(resolved.index);
        }
        else if (Object(_util__WEBPACK_IMPORTED_MODULE_13__["isPresent"])(value) && ofSameType) {
            this.updateState({ dataItem: value });
            this.resetSelection();
        }
        else if (this._previousDataItem) {
            this.updateState({ dataItem: this._previousDataItem });
            this.resetSelection();
        }
        else {
            this.clearState();
            this.resetSelection();
        }
    };
    /**
     * @hidden
     */
    DropDownListComponent.prototype.handleFilter = function (event) {
        this.filterChange.emit(event.target.value);
    };
    DropDownListComponent.prototype.nextTick = function (f) {
        var _this = this;
        this._zone.runOutsideAngular(function () {
            // Use `setTimeout` instead of a resolved promise
            // because the latter does not wait long enough.
            setTimeout(function () { return _this._zone.run(f); });
        });
    };
    DropDownListComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    exportAs: 'kendoDropDownList',
                    providers: [
                        DROPDOWNLIST_VALUE_ACCESSOR,
                        _selection_service__WEBPACK_IMPORTED_MODULE_14__["SelectionService"],
                        _navigation_service__WEBPACK_IMPORTED_MODULE_15__["NavigationService"],
                        _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["LocalizationService"],
                        {
                            provide: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["L10N_PREFIX"],
                            useValue: 'kendo.dropdownlist'
                        }
                    ],
                    selector: 'kendo-dropdownlist',
                    template: "\n        <span #wrapper unselectable=\"on\"\n          role=\"listbox\"\n          [id]=\"focusableId\"\n          [ngClass]=\"{\n            'k-dropdown-wrap': true,\n            'k-state-default': !this.disabled,\n            'k-state-disabled': this.disabled,\n            'k-state-focused': this.isFocused\n          }\"\n          [attr.dir]=\"direction\"\n          [attr.readonly]=\"readonly\"\n          [attr.tabindex]=\"widgetTabIndex\"\n          [attr.aria-disabled]=\"disabled\"\n          [attr.aria-readonly]=\"readonly\"\n          [attr.aria-haspopup]=\"ariaHasPopup\"\n          [attr.aria-expanded]=\"ariaExpanded\"\n          [attr.aria-owns]=\"ariaOwns\"\n          [attr.aria-activedescendant]=\"ariaActivedescendant\"\n          (focus)=\"focusComponent()\"\n          (blur)=\"blurComponent()\"\n          (keydown)=\"keydown($event)\"\n          (keypress)=\"keypress($event)\"\n          (click)=\"click()\"\n        >\n            <span [ngClass]=\"{ 'k-input': true }\" unselectable=\"on\">\n               <ng-template *ngIf=\"valueTemplate\"\n                   [templateContext]=\"{\n                       templateRef: valueTemplate.templateRef,\n                       $implicit: dataItem\n                   }\">\n               </ng-template>\n               <ng-template [ngIf]=\"!valueTemplate\">{{ getText() }}</ng-template>\n           </span>\n           <span [ngClass]=\"{ 'k-select': true}\" unselectable=\"on\">\n               <span [ngClass]=\"buttonClasses\"></span>\n           </span>\n           <ng-template #popupTemplate>\n               <!--filterable-->\n\n               <ng-template [ngIf]=\"filterable\">\n                   <span [ngClass]=\"{ 'k-list-filter': true }\" (click)=\"$event.stopImmediatePropagation()\">\n                       <input #filterInput\n                           [dir]=\"direction\"\n                           [(ngModel)]=\"filterText\"\n                           class=\"k-textbox\"\n                           (keydown)=\"keydown($event)\"\n                           (input)=\"handleFilter($event)\"\n                           (focus)=\"onFilterFocus()\"\n                           (blur)=\"blurFilterInput()\" />\n                       <span [ngClass]=\"{ 'k-icon': true, 'k-i-search': true }\" unselectable=\"on\"></span>\n                   </span>\n               </ng-template>\n               <!--default item-->\n               <ng-template [ngIf]=\"defaultItem && !itemTemplate\">\n                   <div [ngClass]=\"setDefaultItemClasses()\" kendoDropDownsSelectable [index]=\"-1\">\n                       {{ getDefaultItemText() }}\n                   </div>\n               </ng-template>\n               <ng-template [ngIf]=\"defaultItem && itemTemplate\">\n                   <div [ngClass]=\"setDefaultItemClasses()\" kendoDropDownsSelectable [index]=\"-1\">\n                       <ng-template\n                           [templateContext]=\"{\n                               templateRef: itemTemplate.templateRef,\n                               $implicit: defaultItem\n                           }\">\n                       </ng-template>\n                   </div>\n               </ng-template>\n               <!--header template-->\n               <ng-template *ngIf=\"headerTemplate\"\n                   [templateContext]=\"{\n                       templateRef: headerTemplate.templateRef\n                   }\">\n               </ng-template>\n               <!--list-->\n               <kendo-list\n                   [id]=\"listBoxId\"\n                   [optionPrefix]=\"optionPrefix\"\n                   [data]=\"data\"\n                   [textField]=\"textField\"\n                   [valueField]=\"valueField\"\n                   [template]=\"itemTemplate\"\n                   [height]=\"listHeight\"\n                   [show]=\"popupOpen\"\n                   >\n               </kendo-list>\n               <!--no-data template-->\n               <div class=\"k-nodata\" *ngIf=\"data.length === 0\">\n                   <ng-template [ngIf]=\"noDataTemplate\"\n                       [templateContext]=\"{\n                           templateRef: noDataTemplate ? noDataTemplate.templateRef : undefined\n                       }\">\n                   </ng-template>\n                   <ng-template [ngIf]=\"!noDataTemplate\">\n                       <div>NO DATA FOUND.</div>\n                   </ng-template>\n               </div>\n               <!--footer template-->\n               <ng-template *ngIf=\"footerTemplate\"\n                   [templateContext]=\"{\n                       templateRef: footerTemplate.templateRef\n                   }\">\n               </ng-template>\n            </ng-template>\n        </span>\n        <ng-template [ngIf]=\"popupOpen\">\n            <kendo-resize-sensor (resize)=\"onResize()\"></kendo-resize-sensor>\n        </ng-template>\n        <ng-container #container></ng-container>\n  "
                },] },
    ];
    /** @nocollapse */
    DropDownListComponent.ctorParameters = function () { return [
        { type: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_23__["LocalizationService"], },
        { type: _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__["PopupService"], },
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_14__["SelectionService"], },
        { type: _navigation_service__WEBPACK_IMPORTED_MODULE_15__["NavigationService"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_touch_enabled__WEBPACK_IMPORTED_MODULE_25__["TOUCH_ENABLED"],] },] },
    ]; };
    DropDownListComponent.propDecorators = {
        'focusableId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'iconClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'loading': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'textField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'popupSettings': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'listHeight': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'defaultItem': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'readonly': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'filterable': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'ignoreCase': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'delay': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valuePrimitive': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabindex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabIndex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ["tabIndex",] },],
        'valueChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'filterChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'selectionChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'open': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'close': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['focus',] },],
        'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"], args: ['blur',] },],
        'itemTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_item_template_directive__WEBPACK_IMPORTED_MODULE_16__["ItemTemplateDirective"],] },],
        'valueTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_value_template_directive__WEBPACK_IMPORTED_MODULE_17__["ValueTemplateDirective"],] },],
        'headerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_header_template_directive__WEBPACK_IMPORTED_MODULE_18__["HeaderTemplateDirective"],] },],
        'footerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_19__["FooterTemplateDirective"],] },],
        'noDataTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_20__["NoDataTemplateDirective"],] },],
        'container': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['container', { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] },] },],
        'filterInput': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['filterInput',] },],
        'popupTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['popupTemplate',] },],
        'wrapper': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['wrapper',] },],
        'widgetClasses': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-widget',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-dropdown',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-header',] },],
        'dir': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.dir',] },],
    };
    return DropDownListComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.module.js":
/*!***************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.module.js ***!
  \***************************************************************************************/
/*! exports provided: DropDownListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownListModule", function() { return DropDownListModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dropdownlist_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dropdownlist.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.component.js");
/* harmony import */ var _templates_value_template_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./templates/value-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/value-template.directive.js");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js");
/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");





var DROPDOWNLIST_DIRECTIVES = [
    _dropdownlist_component__WEBPACK_IMPORTED_MODULE_1__["DropDownListComponent"],
    _templates_value_template_directive__WEBPACK_IMPORTED_MODULE_2__["ValueTemplateDirective"]
];
/**
 * @hidden
 *
 * The exported package module.
 *
 * The package exports:
 * - `DropDownListComponent`&mdash;The DropDownList component class.
 * - `ItemTemplateDirective`&mdash;The item template directive.
 * - `ValueTemplateDirective`&mdash;The value template directive.
 * - `HeaderTemplateDirective`&mdash;The header template directive.
 * - `FooterTemplateDirective`&mdash;The footer template directive.
 */
var DropDownListModule = /** @class */ (function () {
    function DropDownListModule() {
    }
    DropDownListModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [DROPDOWNLIST_DIRECTIVES],
                    exports: [DROPDOWNLIST_DIRECTIVES, _shared_directives_module__WEBPACK_IMPORTED_MODULE_4__["SharedDirectivesModule"]],
                    imports: [_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]]
                },] },
    ];
    /** @nocollapse */
    DropDownListModule.ctorParameters = function () { return []; };
    return DropDownListModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdowns.module.js":
/*!************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdowns.module.js ***!
  \************************************************************************************/
/*! exports provided: DropDownsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownsModule", function() { return DropDownsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _autocomplete_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autocomplete.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.module.js");
/* harmony import */ var _combobox_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./combobox.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.module.js");
/* harmony import */ var _dropdownlist_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dropdownlist.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.module.js");
/* harmony import */ var _multiselect_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./multiselect.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.module.js");





/**
 * Represents the [NgModule](https://angular.io/docs/ts/latest/guide/ngmodule.html)
 * definition for the DropDowns components.
 *
 * @example
 *
 * ```ts-no-run
 * // Import the DropDowns module
 * import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
 *
 * // The browser platform with a compiler
 * import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
 *
 * import { NgModule } from '@angular/core';
 *
 * // Import the app component
 * import { AppComponent } from './app.component';
 *
 * // Define the app module
 * _@NgModule({
 *     declarations: [AppComponent], // declare app component
 *     imports:      [BrowserModule, DropDownsModule], // import DropDowns module
 *     bootstrap:    [AppComponent]
 * })
 * export class AppModule {}
 *
 * // Compile and launch the module
 * platformBrowserDynamic().bootstrapModule(AppModule);
 *
 * ```
 */
var DropDownsModule = /** @class */ (function () {
    function DropDownsModule() {
    }
    DropDownsModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    exports: [_autocomplete_module__WEBPACK_IMPORTED_MODULE_1__["AutoCompleteModule"], _combobox_module__WEBPACK_IMPORTED_MODULE_2__["ComboBoxModule"], _dropdownlist_module__WEBPACK_IMPORTED_MODULE_3__["DropDownListModule"], _multiselect_module__WEBPACK_IMPORTED_MODULE_4__["MultiSelectModule"]]
                },] },
    ];
    /** @nocollapse */
    DropDownsModule.ctorParameters = function () { return []; };
    return DropDownsModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/error-messages.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/error-messages.js ***!
  \**********************************************************************************/
/*! exports provided: MultiselectMessages, ComboBoxMessages, DropDownListMessages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MultiselectMessages", function() { return MultiselectMessages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComboBoxMessages", function() { return ComboBoxMessages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownListMessages", function() { return DropDownListMessages; });
/**
 * @hidden
 */
/* tslint:disable:max-line-length */
/* tslint:disable:variable-name */
var MultiselectMessages = {
    'array': 'Expected values of array type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/multiselect/#value-selection',
    'object': 'Expected values of Object type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/multiselect/#value-selection',
    'primitive': 'Expected values of primitive type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/multiselect/#value-selection'
};
/**
 * @hidden
 */
/* tslint:disable:max-line-length */
/* tslint:disable:variable-name */
var ComboBoxMessages = {
    'object': 'Expected value of type Object. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/combobox/#toc-value-selection',
    'primitive': 'Expected value of primitive type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/combobox/#toc-value-selection',
    'textAndValue': 'Expected textField and valueField options to be set. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/combobox/#toc-bind-to-arrays-of-complex-data'
};
/**
 * @hidden
 */
/* tslint:disable:max-line-length */
/* tslint:disable:variable-name */
var DropDownListMessages = {
    'defaultItem': 'defaultItem and data items must be of same type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/api/DropDownListComponent/#toc-defaultitem',
    'object': 'Expected value of type Object. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/dropdownlist/#toc-value-selection',
    'primitive': 'Expected value of primitive type. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/dropdownlist/#toc-value-selection',
    'textAndValue': 'Expected textField and valueField options to be set. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/dropdownlist/#toc-bind-to-arrays-of-complex-data'
};


/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js ***!
  \*************************************************************************/
/*! exports provided: ɵa, ɵb, ɵc, ɵk, ɵe, ɵg, ɵd, ɵj, ɵf, ɵi, ɵh, AutoCompleteComponent, ComboBoxComponent, DropDownListComponent, MultiSelectComponent, TagListComponent, ItemTemplateDirective, CustomItemTemplateDirective, HeaderTemplateDirective, FooterTemplateDirective, ValueTemplateDirective, TemplateContextDirective, SelectableDirective, SummaryTagDirective, DropDownsModule, MultiSelectModule, SharedModule, AutoCompleteModule, ComboBoxModule, DropDownListModule, SharedDirectivesModule, ListComponent, PopupComponent, ResizeSensorComponent, PreventableEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/main.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["AutoCompleteComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ComboBoxComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ComboBoxComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownListComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["DropDownListComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MultiSelectComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["MultiSelectComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TagListComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["TagListComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemTemplateDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CustomItemTemplateDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["CustomItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderTemplateDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["HeaderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FooterTemplateDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["FooterTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ValueTemplateDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ValueTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TemplateContextDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["TemplateContextDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SelectableDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["SelectableDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SummaryTagDirective", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["SummaryTagDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownsModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["DropDownsModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MultiSelectModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["MultiSelectModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["SharedModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["AutoCompleteModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ComboBoxModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ComboBoxModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownListModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["DropDownListModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["SharedDirectivesModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ListComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PopupComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["PopupComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ResizeSensorComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PreventableEvent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["PreventableEvent"]; });

/* harmony import */ var _autocomplete_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autocomplete.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return _autocomplete_component__WEBPACK_IMPORTED_MODULE_1__["AUTOCOMPLETE_VALUE_ACCESSOR"]; });

/* harmony import */ var _combobox_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./combobox.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return _combobox_component__WEBPACK_IMPORTED_MODULE_2__["COMBOBOX_VALUE_ACCESSOR"]; });

/* harmony import */ var _dropdownlist_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dropdownlist.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return _dropdownlist_component__WEBPACK_IMPORTED_MODULE_3__["DROPDOWNLIST_VALUE_ACCESSOR"]; });

/* harmony import */ var _list_item_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list-item.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list-item.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵk", function() { return _list_item_directive__WEBPACK_IMPORTED_MODULE_4__["ListItemDirective"]; });

/* harmony import */ var _navigation_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navigation.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵe", function() { return _navigation_service__WEBPACK_IMPORTED_MODULE_5__["NavigationService"]; });

/* harmony import */ var _searchbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./searchbar.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵg", function() { return _searchbar_component__WEBPACK_IMPORTED_MODULE_6__["SearchBarComponent"]; });

/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵd", function() { return _selection_service__WEBPACK_IMPORTED_MODULE_7__["SelectionService"]; });

/* harmony import */ var _templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./templates/group-tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/group-tag-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵj", function() { return _templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_8__["GroupTagTemplateDirective"]; });

/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵf", function() { return _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_9__["NoDataTemplateDirective"]; });

/* harmony import */ var _templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./templates/tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/tag-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵi", function() { return _templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_10__["TagTemplateDirective"]; });

/* harmony import */ var _touch_enabled__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./touch-enabled */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/touch-enabled.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ɵh", function() { return _touch_enabled__WEBPACK_IMPORTED_MODULE_11__["TOUCH_ENABLED"]; });

/**
 * Generated bundle index. Do not edit.
 */














/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list-item.directive.js":
/*!***************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/list-item.directive.js ***!
  \***************************************************************************************/
/*! exports provided: ListItemDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListItemDirective", function() { return ListItemDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * @hidden
 */
var ListItemDirective = /** @class */ (function () {
    function ListItemDirective(element) {
        this.element = element;
    }
    ListItemDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: 'li' // tslint:disable-line
                },] },
    ];
    /** @nocollapse */
    ListItemDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
    ]; };
    return ListItemDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list.component.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/list.component.js ***!
  \**********************************************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _list_item_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./list-item.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list-item.directive.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony import */ var rxjs_operators_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators/map */ "./node_modules/rxjs-compat/_esm5/operators/map.js");
/* harmony import */ var rxjs_operators_merge__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators/merge */ "./node_modules/rxjs-compat/_esm5/operators/merge.js");






/**
 * @hidden
 */
var ListComponent = /** @class */ (function () {
    function ListComponent(selectionService) {
        this.data = [];
        this.selected = [];
        this.focused = -1;
        this.multipleSelection = false;
        this.onClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectionService = selectionService;
        this.scrollSubscription = this.selectionService
            .onSelect.pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_4__["map"])(function (args) { return args.indices[0]; }), Object(rxjs_operators_merge__WEBPACK_IMPORTED_MODULE_5__["merge"])(this.selectionService.onFocus))
            .subscribe(this.scrollToItem.bind(this));
    }
    ListComponent.prototype.ngAfterViewInit = function () {
        if (this.show === true) {
            this.scrollToItem(this.selectionService.focused);
        }
    };
    ListComponent.prototype.ngOnDestroy = function () {
        this.scrollSubscription.unsubscribe();
    };
    ListComponent.prototype.setContainerClasses = function () {
        return {
            'k-list-scroller': true
        };
    };
    ListComponent.prototype.getHeight = function () {
        return this.height + "px";
    };
    ListComponent.prototype.getText = function (dataItem) {
        return Object(_util__WEBPACK_IMPORTED_MODULE_2__["getter"])(dataItem, this.textField);
    };
    ListComponent.prototype.getValue = function (dataItem) {
        return Object(_util__WEBPACK_IMPORTED_MODULE_2__["getter"])(dataItem, this.valueField);
    };
    ListComponent.prototype.scrollToItem = function (index) {
        var items = this.items.toArray();
        if (Object(_util__WEBPACK_IMPORTED_MODULE_2__["isPresent"])(items[index]) && index !== -1) {
            this.scroll(items[index].element);
        }
    };
    ListComponent.prototype.scroll = function (item) {
        if (!item) {
            return;
        }
        var nativeElement = item.nativeElement;
        var content = this.content.nativeElement, itemOffsetTop = nativeElement.offsetTop, itemOffsetHeight = nativeElement.offsetHeight, contentScrollTop = content.scrollTop, contentOffsetHeight = content.clientHeight, bottomDistance = itemOffsetTop + itemOffsetHeight;
        if (contentScrollTop > itemOffsetTop) {
            contentScrollTop = itemOffsetTop;
        }
        else if (bottomDistance > (contentScrollTop + contentOffsetHeight)) {
            contentScrollTop = (bottomDistance - contentOffsetHeight);
        }
        content.scrollTop = contentScrollTop;
    };
    ListComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'kendo-list',
                    template: "\n    <div #content [ngClass]=\"setContainerClasses()\" [style.maxHeight]=\"getHeight()\" unselectable=\"on\">\n    <ul role=\"listbox\" [attr.id]=\"id\" [attr.aria-hidden]=\"!show\" [ngClass]=\"{ 'k-list': true, 'k-reset': true }\">\n        <li role=\"option\"\n            *ngFor=\"let dataItem of data; let index = index\"\n            kendoDropDownsSelectable\n            [index]=\"index\"\n            [multipleSelection]=\"multipleSelection\"\n            [attr.id]=\"optionPrefix + '-' + getValue(dataItem)\"\n            [attr.tabIndex]=\"-1\"\n            [ngClass]=\"{'k-item': true}\">\n            <ng-template *ngIf=\"template\"\n                [templateContext]=\"{\n                    templateRef: template.templateRef,\n                    $implicit: dataItem\n                }\">\n            </ng-template>\n            <ng-template [ngIf]=\"!template\">{{ getText(dataItem) }}</ng-template>\n        </li>\n    </ul>\n    </div>\n  "
                },] },
    ];
    /** @nocollapse */
    ListComponent.ctorParameters = function () { return [
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_3__["SelectionService"], },
    ]; };
    ListComponent.propDecorators = {
        'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'selected': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'focused': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'textField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'height': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'template': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'show': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'id': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'optionPrefix': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'multipleSelection': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'items': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"], args: [_list_item_directive__WEBPACK_IMPORTED_MODULE_1__["ListItemDirective"],] },],
        'content': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['content',] },],
    };
    return ListComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/main.js":
/*!************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/main.js ***!
  \************************************************************************/
/*! exports provided: AutoCompleteComponent, ComboBoxComponent, DropDownListComponent, MultiSelectComponent, TagListComponent, ItemTemplateDirective, CustomItemTemplateDirective, HeaderTemplateDirective, FooterTemplateDirective, ValueTemplateDirective, TemplateContextDirective, SelectableDirective, SummaryTagDirective, DropDownsModule, MultiSelectModule, SharedModule, AutoCompleteModule, ComboBoxModule, DropDownListModule, SharedDirectivesModule, ListComponent, PopupComponent, ResizeSensorComponent, PreventableEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _autocomplete_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./autocomplete.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteComponent", function() { return _autocomplete_component__WEBPACK_IMPORTED_MODULE_0__["AutoCompleteComponent"]; });

/* harmony import */ var _combobox_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./combobox.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ComboBoxComponent", function() { return _combobox_component__WEBPACK_IMPORTED_MODULE_1__["ComboBoxComponent"]; });

/* harmony import */ var _dropdownlist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dropdownlist.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownListComponent", function() { return _dropdownlist_component__WEBPACK_IMPORTED_MODULE_2__["DropDownListComponent"]; });

/* harmony import */ var _multiselect_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./multiselect.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MultiSelectComponent", function() { return _multiselect_component__WEBPACK_IMPORTED_MODULE_3__["MultiSelectComponent"]; });

/* harmony import */ var _taglist_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./taglist.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/taglist.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TagListComponent", function() { return _taglist_component__WEBPACK_IMPORTED_MODULE_4__["TagListComponent"]; });

/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemTemplateDirective", function() { return _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_5__["ItemTemplateDirective"]; });

/* harmony import */ var _templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./templates/custom-item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/custom-item-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CustomItemTemplateDirective", function() { return _templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_6__["CustomItemTemplateDirective"]; });

/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderTemplateDirective", function() { return _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_7__["HeaderTemplateDirective"]; });

/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FooterTemplateDirective", function() { return _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_8__["FooterTemplateDirective"]; });

/* harmony import */ var _templates_value_template_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./templates/value-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/value-template.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ValueTemplateDirective", function() { return _templates_value_template_directive__WEBPACK_IMPORTED_MODULE_9__["ValueTemplateDirective"]; });

/* harmony import */ var _templates_template_context_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./templates/template-context.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/template-context.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TemplateContextDirective", function() { return _templates_template_context_directive__WEBPACK_IMPORTED_MODULE_10__["TemplateContextDirective"]; });

/* harmony import */ var _selectable_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./selectable.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selectable.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SelectableDirective", function() { return _selectable_directive__WEBPACK_IMPORTED_MODULE_11__["SelectableDirective"]; });

/* harmony import */ var _summary_tag_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./summary-tag.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/summary-tag.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SummaryTagDirective", function() { return _summary_tag_directive__WEBPACK_IMPORTED_MODULE_12__["SummaryTagDirective"]; });

/* harmony import */ var _dropdowns_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./dropdowns.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdowns.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownsModule", function() { return _dropdowns_module__WEBPACK_IMPORTED_MODULE_13__["DropDownsModule"]; });

/* harmony import */ var _multiselect_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./multiselect.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MultiSelectModule", function() { return _multiselect_module__WEBPACK_IMPORTED_MODULE_14__["MultiSelectModule"]; });

/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./shared.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return _shared_module__WEBPACK_IMPORTED_MODULE_15__["SharedModule"]; });

/* harmony import */ var _autocomplete_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./autocomplete.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/autocomplete.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteModule", function() { return _autocomplete_module__WEBPACK_IMPORTED_MODULE_16__["AutoCompleteModule"]; });

/* harmony import */ var _combobox_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./combobox.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/combobox.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ComboBoxModule", function() { return _combobox_module__WEBPACK_IMPORTED_MODULE_17__["ComboBoxModule"]; });

/* harmony import */ var _dropdownlist_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./dropdownlist.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/dropdownlist.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropDownListModule", function() { return _dropdownlist_module__WEBPACK_IMPORTED_MODULE_18__["DropDownListModule"]; });

/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function() { return _shared_directives_module__WEBPACK_IMPORTED_MODULE_19__["SharedDirectivesModule"]; });

/* harmony import */ var _list_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./list.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return _list_component__WEBPACK_IMPORTED_MODULE_20__["ListComponent"]; });

/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PopupComponent", function() { return _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_21__["PopupComponent"]; });

/* harmony import */ var _progress_kendo_angular_resize_sensor__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @progress/kendo-angular-resize-sensor */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorComponent", function() { return _progress_kendo_angular_resize_sensor__WEBPACK_IMPORTED_MODULE_22__["ResizeSensorComponent"]; });

/* harmony import */ var _common_preventable_event__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./common/preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PreventableEvent", function() { return _common_preventable_event__WEBPACK_IMPORTED_MODULE_23__["PreventableEvent"]; });



























/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.component.js":
/*!*****************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.component.js ***!
  \*****************************************************************************************/
/*! exports provided: MultiSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MultiSelectComponent", function() { return MultiSelectComponent; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _searchbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./searchbar.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subscription__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Subscription */ "./node_modules/rxjs-compat/_esm5/Subscription.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators/catchError */ "./node_modules/rxjs-compat/_esm5/operators/catchError.js");
/* harmony import */ var rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators/filter */ "./node_modules/rxjs-compat/_esm5/operators/filter.js");
/* harmony import */ var rxjs_operators_map__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators/map */ "./node_modules/rxjs-compat/_esm5/operators/map.js");
/* harmony import */ var rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators/tap */ "./node_modules/rxjs-compat/_esm5/operators/tap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");
/* harmony import */ var _navigation_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./navigation.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js");
/* harmony import */ var _navigation_action__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./navigation-action */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony import */ var _templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./templates/custom-item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/custom-item-template.directive.js");
/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony import */ var _templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./templates/tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/tag-template.directive.js");
/* harmony import */ var _templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./templates/group-tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/group-tag-template.directive.js");
/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");
/* harmony import */ var _error_messages__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./error-messages */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/error-messages.js");
/* harmony import */ var _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./common/preventable-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/preventable-event.js");
/* harmony import */ var _common_remove_tag_event__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./common/remove-tag-event */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/remove-tag-event.js");
/* harmony import */ var _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @progress/kendo-angular-l10n */ "./node_modules/@progress/kendo-angular-l10n/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* tslint:disable:max-line-length */


























var MULTISELECT_VALUE_ACCESSOR = {
    multi: true,
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NG_VALUE_ACCESSOR"],
    // tslint:disable-next-line:no-use-before-declare
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["forwardRef"])(function () { return MultiSelectComponent; })
};
/**
 * Represents the Kendo UI MultiSelect component for Angular.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-multiselect [data]="listItems">
 *  </kendo-multiselect>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 */
var MultiSelectComponent = /** @class */ (function () {
    function MultiSelectComponent(localization, popupService, selectionService, navigationService, cdr, differs, renderer, hostElement, _zone) {
        var _this = this;
        this.localization = localization;
        this.popupService = popupService;
        this.selectionService = selectionService;
        this.navigationService = navigationService;
        this.cdr = cdr;
        this.differs = differs;
        this.renderer = renderer;
        this.hostElement = hostElement;
        this._zone = _zone;
        this.activeId = Object(_util__WEBPACK_IMPORTED_MODULE_0__["guid"])();
        this.listBoxId = Object(_util__WEBPACK_IMPORTED_MODULE_0__["guid"])();
        this.focusedTagIndex = undefined;
        /**
         * @hidden
         */
        this.focusableId = "k-" + Object(_util__WEBPACK_IMPORTED_MODULE_0__["guid"])();
        /**
         * Controls whether to close the suggestion list of the MultiSelect after the selection of an item.
         * @default true
         */
        this.autoClose = true;
        /**
         * Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) of the component.
         */
        this.tabindex = 0;
        /**
         * Sets the disabled state of the component.
         */
        this.disabled = false;
        /**
         * Sets the readonly state of the component.
         */
        this.readonly = false;
        /**
         * Enables the [filtering]({% slug filtering_multiselect %}) functionality of the MultiSelect.
         */
        this.filterable = false;
        /**
         * Sets the height of the suggestions list. By default, `listHeight` is 200px.
         *
         * > The `listHeight` property affects only the list of suggestions and not the whole popup container.
         * > To set the height of the popup container, use `popupSettings.height`.
         */
        this.listHeight = 200;
        /**
         * If set to `true`, renders a button on hovering over the component.
         * Clicking this button resets the value of the component to an empty array and triggers the `change` event.
         */
        this.clearButton = true;
        /**
         * A user-defined callback function which receives an array of selected data items and maps them to an array of tags.
         *
         * @param { Any[] } dataItems - The selected data items from the list.
         * @returns { Any[] } - The tags that will be rendered by the component.
         */
        this.tagMapper = function (tags) { return tags || []; };
        /**
         * Specifies whether the MultiSelect allows user-defined values that are not present in the dataset.
         * The default value is `false`.
         *
         * For more information, refer to the article on [custom values]({% slug custom_values_multiselect %}).
         */
        this.allowCustom = false;
        /**
         * A user-defined callback function which returns normalized custom values.
         * Typically used when the data items are different from type `string`.
         *
         * @param { Any } value - The custom value that is defined by the user.
         * @returns { Any }
         *
         * @example
         * ```ts
         * import { map } from 'rxjs/operators/map';
         *
         * _@Component({
         * selector: 'my-app',
         * template: `
         *   <kendo-multiselect
         *       [allowCustom]="true"
         *       [data]="listItems"
         *       [textField]="'text'"
         *       [valueField]="'value'"
         *       [valueNormalizer]="valueNormalizer"
         *       (valueChange)="onValueChange($event)"
         *   >
         *   </kendo-multiselect>
         * `
         * })
         *
         * class AppComponent {
         *   public listItems: Array<{ text: string, value: number }> = [
         *       { text: "Small", value: 1 },
         *       { text: "Medium", value: 2 },
         *       { text: "Large", value: 3 }
         *   ];
         *
         *   public onValueChange(value) {
         *       console.log("valueChange : ", value);
         *   }
         *
         *   public valueNormalizer = (text$: Observable<string>) => text$.pipe(map((text: string) => {
         *      return {
         *         ProductID: Math.floor(Math.random() * (1000 - 100) + 1000), //generate unique valueField
         *         ProductName: text };
         *   }));
         *
         * }
         * ```
         */
        this.valueNormalizer = function (text) { return text.pipe(Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_7__["map"])(function (userInput) {
            var comparer = function (item) { return userInput.toLowerCase() === item.toLowerCase(); };
            var matchingValue = _this.value.find(comparer);
            if (matchingValue) {
                return matchingValue;
            }
            var matchingItem = _this.data.find(comparer);
            return matchingItem ? matchingItem : userInput;
        })); };
        /**
         * Fires each time the user types in the input field.
         * You can filter the source based on the passed filtration value.
         */
        this.filterChange = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        /**
         * Fires each time the value is changed&mdash;
         * when the component is blurred or the value is cleared through the **Clear** button.
         * For more details, refer to the example on [events]({% slug overview_multiselect %}#toc-events).
         *
         * When the value of the component is programmatically changed through its API or form binding
         * to `ngModel` or `formControl`, the `valueChange` event is not triggered because it
         * might cause a mix-up with the built-in `valueChange` mechanisms of the `ngModel` or `formControl` bindings.
         */
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        /**
         * Fires each time the popup is about to open.
         * This event is preventable. If you cancel it, the popup will remain closed.
         */
        this.open = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        /**
         * Fires each time the popup is about to close.
         * This event is preventable. If you cancel it, the popup will remain open.
         */
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        /**
         * Fires each time the user focuses the MultiSelect.
         */
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"](); //tslint:disable-line:no-output-rename
        /**
         * Fires each time the MultiSelect gets blurred.
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"](); //tslint:disable-line:no-output-rename
        /**
         * Fires each time a tag is about to be removed.
         * This event is preventable. If you cancel it, the tag will not be removed.
         */
        this.removeTag = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.onChangeCallback = function (_) { };
        this.onTouchedCallback = function (_) { };
        this._data = [];
        this._placeholder = '';
        this._open = false;
        this._value = [];
        this.selectedDataItems = [];
        this._popupSettings = { animate: true };
        this.observableSubscriptions = new rxjs_Subscription__WEBPACK_IMPORTED_MODULE_3__["Subscription"]();
        this.changeSubscription = new rxjs_Subscription__WEBPACK_IMPORTED_MODULE_3__["Subscription"]();
        this.isFocused = false;
        this.wrapperBlurred = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.customValueSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.hostElement = hostElement.nativeElement;
        this.popupMouseDownHandler = this.onMouseDown.bind(this);
        this.direction = this.localization.rtl ? 'rtl' : 'ltr';
        this.subscribeEvents();
    }
    Object.defineProperty(MultiSelectComponent.prototype, "popupOpen", {
        get: function () {
            return this._open;
        },
        set: function (open) {
            if (this.disabled || this.readonly || this.popupOpen === open) {
                return;
            }
            var eventArgs = new _common_preventable_event__WEBPACK_IMPORTED_MODULE_22__["PreventableEvent"]();
            if (open) {
                this.open.emit(eventArgs);
            }
            else {
                this.close.emit(eventArgs);
            }
            if (eventArgs.isDefaultPrevented()) {
                return;
            }
            this._toggle(open);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.focusComponent = function () {
        if (!this.isFocused) {
            this.isFocused = true;
            this.onFocus.emit();
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.blurComponent = function () {
        if (this.isFocused) {
            this.closePopup();
            this.isFocused = false;
            if (!this.allowCustom) {
                this.text = "";
            }
            this.onBlur.emit();
            this.onTouchedCallback();
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.wrapperMousedown = function (event) {
        if (this.isFocused && event.target === this.searchbar.input.nativeElement) {
            return;
        }
        this.searchbar.focus();
        this.popupOpen = !this.popupOpen;
        event.preventDefault();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.onResize = function () {
        if (this._open) {
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
        }
    };
    Object.defineProperty(MultiSelectComponent.prototype, "appendTo", {
        get: function () {
            var appendTo = this.popupSettings.appendTo;
            if (!appendTo || appendTo === 'root') {
                return undefined;
            }
            return appendTo === 'component' ? this.container : appendTo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        /**
         * Sets the data of the MultiSelect.
         *
         * > The data has to be provided in an array-like list of items.
         */
        set: function (data) {
            this._data = data || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        /**
         * Sets the value of the MultiSelect. It can be either of the primitive (string, numbers) or of the complex (objects) type.
         * To define the type, use the `valuePrimitive` option.
         *
         * > All selected values which are not present in the source are ignored.
         */
        set: function (values) {
            this._value = values ? values : [];
            if (!this.differ && this.value) {
                this.differ = this.differs.find(this.value).create();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "tabIndex", {
        get: function () {
            return this.tabindex;
        },
        /**
         * @hidden
         */
        set: function (tabIndex) {
            this.tabindex = tabIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "placeholder", {
        get: function () {
            return this.selectedDataItems.length ? '' : this._placeholder;
        },
        /**
         * The hint which is displayed when the component is empty.
         * When the values are selected, it disappears.
         */
        set: function (text) {
            this._placeholder = text || '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "popupSettings", {
        get: function () {
            return this._popupSettings;
        },
        /**
         * Configures the popup of the MultiSelect.
         *
         * The available options are:
         * - `animate: Boolean`&mdash;Controls the popup animation. By default, the open and close animations are enabled.
         * - `width: Number`&mdash;Sets the width of the popup container. By default, the width of the host element is used.
         * - `height: Number`&mdash;Sets the height of the popup container.
         * - `popupClass: String`&mdash;Specifies a list of CSS classes that are used to style the popup.
         */
        set: function (settings) {
            this._popupSettings = Object.assign({ animate: true }, settings);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "widgetClasses", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "dir", {
        get: function () {
            return this.direction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "focusedClass", {
        get: function () {
            return this.isFocused;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "disabledClass", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "listContainerClasses", {
        get: function () {
            var containerClasses = ['k-list-container', 'k-reset'];
            if (this.popupSettings.popupClass) {
                containerClasses.push(this.popupSettings.popupClass);
            }
            return containerClasses;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "width", {
        get: function () {
            var wrapperOffsetWidth = 0;
            if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isDocumentAvailable"])()) {
                wrapperOffsetWidth = this.wrapper.nativeElement.offsetWidth;
            }
            var width = this.popupSettings.width || wrapperOffsetWidth;
            var minWidth = isNaN(wrapperOffsetWidth) ? wrapperOffsetWidth : wrapperOffsetWidth + "px";
            var maxWidth = isNaN(width) ? width : width + "px";
            return { min: minWidth, max: maxWidth };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "height", {
        get: function () {
            var popupHeight = this.popupSettings.height;
            return Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(popupHeight) ? popupHeight + "px" : 'auto';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.popupOpened = function () {
        this.popupWidth = this.width.max;
        this.popupMinWidth = this.width.min;
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.onMouseDown = function (event) {
        var tagName = event.target.tagName.toLowerCase();
        if (tagName !== "input") {
            event.preventDefault();
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.verifySettings = function () {
        var valueOrText = !Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(this.valueField) !== !Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(this.textField);
        if (!Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["isDevMode"])() || this.value.length === 0) {
            return;
        }
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_0__["isArray"])(this.value)) {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_21__["MultiselectMessages"].array);
        }
        if (this.valuePrimitive === true && Object(_util__WEBPACK_IMPORTED_MODULE_0__["isObjectArray"])(this.value)) {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_21__["MultiselectMessages"].primitive);
        }
        if (this.valuePrimitive === false && !Object(_util__WEBPACK_IMPORTED_MODULE_0__["isObjectArray"])(this.value)) {
            throw new Error(_error_messages__WEBPACK_IMPORTED_MODULE_21__["MultiselectMessages"].object);
        }
        if (valueOrText) {
            throw new Error("Expected textField and valueField options to be set. See http://www.telerik.com/kendo-angular-ui/components/dropdowns/multiselect/#toc-bind-to-arrays-of-complex-data");
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.change = function (event) {
        var _this = this;
        var isCustomItem = (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(event.added) || Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(event.removed)) && (event.added === -1 || event.removed === -1);
        if (isCustomItem) {
            this.addCustomValue(this.text);
            return; //change is emited asynchronosly
        }
        //existing items
        if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(event.added)) {
            var dataItem = this.data[event.added];
            var newItem = (this.valuePrimitive && Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(dataItem[this.valueField])) ? dataItem[this.valueField] :
                dataItem;
            this.value = this.value.concat([newItem]);
            this.cdr.markForCheck();
        }
        if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(event.removed)) {
            var dataItem_1 = this.data[event.removed];
            if (this.valuePrimitive) {
                var index = this.value.indexOf(Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(dataItem_1[this.valueField]) ? dataItem_1[this.valueField] : dataItem_1);
                this.value.splice(index, 1);
            }
            else {
                this.value = this.value.filter(function (item) { return item[_this.valueField] !== dataItem_1[_this.valueField]; });
            }
            this.cdr.markForCheck();
        }
        this.emitValueChange();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.setState = function (value) {
        var objectArray = Object(_util__WEBPACK_IMPORTED_MODULE_0__["isObjectArray"])(value);
        var selection = Object(_util__WEBPACK_IMPORTED_MODULE_0__["selectedIndices"])(this.value, this.data, this.valueField);
        this.selectionService.resetSelection(selection);
        if (this.popupOpen && this.selectionService.focused === undefined && this.data.length) {
            this.selectionService.focused = 0;
        }
        if (this.selectedDataItems.length <= 0) {
            if (this.valuePrimitive && !this.valueField) {
                this.selectedDataItems = value.slice();
            }
            if (objectArray || this.valuePrimitive && this.valueField) {
                this.selectedDataItems = Object(_util__WEBPACK_IMPORTED_MODULE_0__["resolveAllValues"])(value, this.data, this.valueField);
            }
        }
        this.tags = this.tagMapper(this.selectedDataItems.slice(0));
        this.cdr.markForCheck();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.handleBlur = function () {
        this.wrapperBlurred.emit();
        // this.closePopup();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.handleFilter = function (text) {
        if (this.filterable) {
            this.filterChange.emit(text);
        }
        else {
            this.searchTextAndFocus(text);
        }
        this.text = text;
        this.searchbar.setInputSize();
        if (text && !this.popupOpen) {
            this.openPopup();
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.clearFilter = function () {
        if (this.filterable && this.text) {
            this.filterChange.emit("");
        }
        this.text = "";
        this.searchbar.setInputSize();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.handleNavigate = function (event) {
        var navigateInput = this.text && event.keyCode !== _common_keys__WEBPACK_IMPORTED_MODULE_13__["Keys"].down && event.keyCode !== _common_keys__WEBPACK_IMPORTED_MODULE_13__["Keys"].up;
        var selectValue = this.text && event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_13__["Keys"].enter || event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_13__["Keys"].esc;
        var deleteTag = !this.text && event.keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_13__["Keys"].backspace && this.tags.length > 0;
        if (deleteTag) {
            this.handleBackspace();
            return;
        }
        if (this.disabled || navigateInput && !selectValue) {
            return;
        }
        var eventData = event;
        var focused = isNaN(this.selectionService.focused) ? -1 : this.selectionService.focused;
        var action = this.navigationService.process({
            current: focused,
            max: this.data.length - 1,
            min: this.allowCustom && this.text ? -1 : 0,
            open: this.popupOpen,
            originalEvent: eventData
        });
        if (action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Undefined &&
            ((action === _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Enter && this.popupOpen) || action !== _navigation_action__WEBPACK_IMPORTED_MODULE_12__["NavigationAction"].Enter)) {
            event.preventDefault();
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.handleRemoveTag = function (tagData) {
        var eventArgs = new _common_remove_tag_event__WEBPACK_IMPORTED_MODULE_23__["RemoveTagEvent"](tagData);
        if (this.disabled || this.readonly) {
            return;
        }
        this.focus();
        this.removeTag.emit(eventArgs);
        if (eventArgs.isDefaultPrevented()) {
            return;
        }
        if (tagData instanceof Array) {
            this.removeGroupTag(tagData);
        }
        else {
            this.removeSingleTag(tagData);
        }
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.clearAll = function (event) {
        event.stopImmediatePropagation();
        if (this.filterable && this.text) {
            this.text = "";
            this.filterChange.emit("");
        }
        this.value = [];
        this.selectedDataItems = [];
        this.cdr.markForCheck();
        this.emitValueChange();
        this.setState([]);
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.addCustomValue = function (text) {
        this.customValueSubject.next(text);
    };
    MultiSelectComponent.prototype.ngAfterContentChecked = function () {
        this.verifySettings();
    };
    MultiSelectComponent.prototype.ngDoCheck = function () {
        if (this.differ) {
            var valueChanges = this.differ.diff(this.value);
            if (valueChanges) {
                this.selectedDataItems = this.getSelectedDataItems(valueChanges);
                this.tags = this.tagMapper(this.selectedDataItems.slice(0));
            }
        }
    };
    MultiSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderer.removeAttribute(this.hostElement, "tabindex");
        this.createCustomValueStream();
        this.localizationChangeSubscription = this.localization
            .changes.subscribe(function (_a) {
            var rtl = _a.rtl;
            return _this.direction = rtl ? 'rtl' : 'ltr';
        });
    };
    MultiSelectComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (this.valuePrimitive === undefined) {
            this.valuePrimitive = !this.valueField;
        }
        if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isChanged"])("valueNormalizer", changes)) {
            this.createCustomValueStream();
        }
        if (changes.value && this.selectedDataItems.length > 0) {
            //persisted selected data items when list is filtered
            this.selectedDataItems = this.selectedDataItems.concat(this.data).filter(function (curr) {
                return changes.value.currentValue.find(function (item) { return item[_this.valueField] === curr; });
            }).filter(function (dataItem) { return !!dataItem; }); //filter undefined values
        }
        var STATE_PROPS = /(data|textField|valueField|valuePrimitive)/g;
        if (STATE_PROPS.test(Object.keys(changes).join())) {
            this.setState(this.value);
        }
    };
    MultiSelectComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.searchbar.setInputSize();
        this.observableSubscriptions.add(this.searchbar.onFocus.subscribe(function (event) {
            if (!_this.isFocused) {
                _this.isFocused = true;
                _this.onFocus.emit(event);
            }
        }));
        this.observableSubscriptions.add(this.searchbar.onBlur.subscribe(function (event) {
            if (_this.isFocused) {
                _this.isFocused = false;
                _this.onBlur.emit(event);
                _this.onTouchedCallback();
            }
        }));
    };
    MultiSelectComponent.prototype.ngOnDestroy = function () {
        this._toggle(false);
        this.unsubscribeEvents();
        if (this.localizationChangeSubscription) {
            this.localizationChangeSubscription.unsubscribe();
        }
    };
    /**
     * Focuses the MultiSelect.
     */
    MultiSelectComponent.prototype.focus = function () {
        if (!this.disabled) {
            this.searchbar.focus();
        }
    };
    /**
     * Blurs the MultiSelect.
     */
    MultiSelectComponent.prototype.blur = function () {
        if (!this.disabled) {
            this.searchbar.blur();
        }
    };
    /**
     * Toggles the visibility of the popup.
     * If you use the `toggle` method to open or close the popup, the respective `open` and `close` events will not be fired.
     *
     * @param open - The state of the popup.
     */
    MultiSelectComponent.prototype.toggle = function (open) {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this._toggle((open === undefined) ? !_this._open : open);
            _this.cdr.markForCheck();
        });
    };
    Object.defineProperty(MultiSelectComponent.prototype, "isOpen", {
        /**
         * Returns the current open state of the popup.
         */
        get: function () {
            return this.popupOpen;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Resets the value of the MultiSelect.
     * If you use the `reset` method to clear the value of the component,
     * the model will not update automatically and the `selectionChange` and `valueChange` events will not be fired.
     */
    MultiSelectComponent.prototype.reset = function () {
        this.text = "";
        this.value = [];
        this.selectedDataItems = [];
        this.setState([]);
        this.cdr.markForCheck();
    };
    // NG MODEL BINDINGS
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.writeValue = function (value) {
        this.value = value || [];
        this.selectedDataItems = [];
        this.setState(this.value);
        this.verifySettings();
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.setDisabledState = function (isDisabled) {
        this.disabled = isDisabled;
    };
    /**
     * @hidden
     */
    MultiSelectComponent.prototype.onTagMapperChange = function () {
        this.tags = this.tagMapper(this.selectedDataItems.slice(0));
        this.cdr.markForCheck();
    };
    MultiSelectComponent.prototype.subscribeEvents = function () {
        var _this = this;
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_0__["isDocumentAvailable"])()) {
            return;
        }
        this.observableSubscriptions = this.changeSubscription = this.selectionService.onChange.subscribe(this.handleItemChange.bind(this));
        var isOpen = function () { return _this.popupOpen; };
        var isClosed = function () { return !_this.popupOpen; };
        var isTagFocused = function () { return !_this.popupOpen && _this.focusedTagIndex !== undefined; };
        [
            this.navigationService.esc.subscribe(this.closePopup.bind(this)),
            this.navigationService.enter.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(isOpen)).subscribe(this.handleEnter.bind(this)),
            this.navigationService.open.subscribe(this.openPopup.bind(this)),
            this.navigationService.close.subscribe(this.handleClose.bind(this)),
            this.navigationService.up.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(isOpen)).subscribe(function (event) { return _this.handleUp(event.index); }),
            this.navigationService.home.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(function () { return isClosed; })).subscribe(this.handleHome.bind(this)),
            this.navigationService.end.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(function () { return isClosed; })).subscribe(this.handleEnd.bind(this)),
            this.navigationService.backspace.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(isTagFocused)).subscribe(this.handleBackspace.bind(this)),
            this.navigationService.delete.pipe(Object(rxjs_operators_filter__WEBPACK_IMPORTED_MODULE_6__["filter"])(isTagFocused)).subscribe(this.handleDelete.bind(this)),
            this.navigationService.left.subscribe(this.handleLeftKey.bind(this)),
            this.navigationService.right.subscribe(this.handleRightKey.bind(this)),
            this.navigationService.down.subscribe(function (event) { return _this.handleDownKey(event.index); })
        ].forEach(function (s) { return _this.observableSubscriptions.add(s); });
    };
    MultiSelectComponent.prototype.unsubscribeEvents = function () {
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_0__["isDocumentAvailable"])()) {
            return;
        }
        this.observableSubscriptions.unsubscribe();
        if (this.customValueSubscription) {
            this.customValueSubscription.unsubscribe();
        }
    };
    MultiSelectComponent.prototype.removeGroupTag = function (dataItems) {
        var _this = this;
        var indices = dataItems.map(function (dataItem) { return Object(_util__WEBPACK_IMPORTED_MODULE_0__["selectedIndices"])([dataItem], _this.data, _this.valueField)[0]; });
        var valuesToRemove = [];
        indices.forEach(function (currentValue, index) {
            if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isNumber"])(currentValue)) {
                _this.selectionService.unselect(currentValue);
            }
            else {
                var filter_1 = function (item) { return Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.valueField) !== Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(dataItems[index], _this.valueField); };
                valuesToRemove.push(_this.value.find(filter_1));
            }
        });
        if (valuesToRemove.length) {
            var newValue_1 = this.value.slice();
            var newSelectedDataItems_1 = this.selectedDataItems.slice();
            valuesToRemove.forEach(function (value) {
                var filter = function (item) { return Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.valueField) !== value; };
                newValue_1 = newValue_1.filter(filter);
                newSelectedDataItems_1 = newSelectedDataItems_1.filter(filter);
            });
            this.value = newValue_1;
            this.selectedDataItems = newSelectedDataItems_1;
        }
        this.tags = this.tagMapper(this.selectedDataItems.slice(0));
        this.cdr.markForCheck();
        this.emitValueChange();
    };
    MultiSelectComponent.prototype.removeSingleTag = function (dataItem) {
        var _this = this;
        var index = Object(_util__WEBPACK_IMPORTED_MODULE_0__["selectedIndices"])([dataItem], this.data, this.valueField)[0];
        if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isNumber"])(index)) {
            this.selectionService.unselect(index);
            this.popupOpen = false;
        }
        else {
            var filter_2 = function (item) { return Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.valueField) !== Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(dataItem, _this.valueField); };
            this.value = this.value.filter(filter_2);
            this.selectedDataItems = this.selectedDataItems.filter(filter_2);
            this.tags = this.tagMapper(this.selectedDataItems.slice(0));
            this.cdr.markForCheck();
            this.emitValueChange();
        }
    };
    MultiSelectComponent.prototype.createCustomValueStream = function () {
        var _this = this;
        if (this.customValueSubscription) {
            this.customValueSubscription.unsubscribe();
        }
        this.customValueSubscription = this.customValueSubject.pipe(Object(rxjs_operators_tap__WEBPACK_IMPORTED_MODULE_8__["tap"])(function () {
            _this.loading = true;
            _this.disabled = true;
        }), this.valueNormalizer, Object(rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function () {
            _this.loading = false;
            _this.disabled = false;
            _this.text = "";
            _this.filterChange.emit("");
            if (_this.autoClose) {
                _this.popupOpen = false;
                _this.nextTick(function () {
                    _this.searchbar.focus();
                });
            }
            return _this.customValueSubject;
        }))
            .subscribe(function (normalizedValue) {
            _this.loading = false;
            _this.disabled = false;
            _this.text = "";
            _this.filterChange.emit("");
            if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(normalizedValue)) {
                var newValue_2 = _this.valuePrimitive ? Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(normalizedValue, _this.valueField) : normalizedValue;
                if (_this.value.indexOf(newValue_2) === -1) {
                    _this.value = _this.value.concat([newValue_2]);
                    _this.selectedDataItems = _this.selectedDataItems.concat([normalizedValue]);
                }
                else {
                    _this.value.splice(_this.value.indexOf(newValue_2), 1);
                    _this.selectedDataItems = _this.selectedDataItems.filter(function (item) { return Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.valueField) !== newValue_2; });
                }
                _this.tags = _this.tagMapper(_this.selectedDataItems.slice(0));
                _this.emitValueChange();
            }
            if (_this.autoClose) {
                _this.popupOpen = false;
                _this.nextTick(function () {
                    _this.searchbar.focus();
                });
            }
        });
    };
    MultiSelectComponent.prototype.handleItemChange = function (event) {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(event.added) && event.added !== -1) {
            this.clearFilter();
        }
        this.change(event);
        if (this.autoClose) {
            this.popupOpen = false;
        }
    };
    MultiSelectComponent.prototype.handleEnter = function (event) {
        var focusedIndex = this.selectionService.focused;
        if (this.popupOpen) {
            event.originalEvent.preventDefault();
        }
        if (focusedIndex === -1 && !this.allowCustom) {
            return;
        }
        if (focusedIndex === -1 && this.text) {
            this.addCustomValue(this.text);
            return; //clear filter & close popup at customValueSubscription due to race conditions
        }
        if (this.selectionService.isSelected(focusedIndex)) {
            this.selectionService.unselect(focusedIndex);
        }
        else {
            this.selectionService.add(focusedIndex);
        }
        this.clearFilter();
        if (this.autoClose) {
            this.popupOpen = false;
        }
    };
    MultiSelectComponent.prototype.handleClose = function () {
        this.closePopup();
        this.searchbar.focus();
    };
    MultiSelectComponent.prototype.handleEnd = function () {
        this.focusedTagIndex = this.tags.length - 1;
    };
    MultiSelectComponent.prototype.handleHome = function () {
        this.focusedTagIndex = 0;
    };
    MultiSelectComponent.prototype.handleUp = function (index) {
        this.selectionService.focused = index;
    };
    MultiSelectComponent.prototype.handleBackspace = function () {
        if (this.focusedTagIndex !== undefined) {
            this.handleDelete();
            return;
        }
        this.handleRemoveTag(this.tags[this.tags.length - 1]);
        this.searchbar.focus();
    };
    MultiSelectComponent.prototype.handleDelete = function () {
        this.handleRemoveTag(this.tags[this.focusedTagIndex]);
        if (this.focusedTagIndex === this.tags.length) {
            this.focusedTagIndex = undefined;
        }
    };
    MultiSelectComponent.prototype.handleLeftKey = function () {
        if (this.direction === 'rtl' && this.focusedTagIndex === 0) {
            this.focusedTagIndex = undefined;
            return;
        }
        if (this.direction === 'rtl' && this.focusedTagIndex === undefined) {
            return;
        }
        if (this.focusedTagIndex === undefined || this.focusedTagIndex < 0) {
            this.focusedTagIndex = this.tags.length - 1;
        }
        else if (this.focusedTagIndex !== 0) {
            this.focusedTagIndex--;
        }
    };
    MultiSelectComponent.prototype.handleDownKey = function (index) {
        if (this.popupOpen) {
            this.selectionService.focused = index || 0;
        }
        else {
            this.openPopup();
        }
    };
    MultiSelectComponent.prototype.handleRightKey = function () {
        var last = this.tags.length - 1;
        if (this.direction === 'rtl' && this.focusedTagIndex === undefined) {
            this.focusedTagIndex = 0;
            return;
        }
        if (this.direction === 'rtl' && this.focusedTagIndex === last) {
            return;
        }
        if (this.focusedTagIndex === last) {
            this.focusedTagIndex = undefined;
        }
        else if (this.focusedTagIndex < last) {
            this.focusedTagIndex++;
        }
    };
    MultiSelectComponent.prototype.findIndex = function (text) {
        var _this = this;
        return this.data.findIndex(function (item) {
            var itemText = Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.textField);
            itemText = itemText === undefined ? "" : itemText.toString().toLowerCase();
            return itemText.startsWith(text.toLowerCase());
        });
    };
    MultiSelectComponent.prototype.searchTextAndFocus = function (text) {
        var index = this.allowCustom && text ? -1 : this.findIndex(text);
        this.selectionService.focused = index;
    };
    MultiSelectComponent.prototype.closePopup = function () {
        this.popupOpen = false;
        this.focusedTagIndex = undefined;
    };
    MultiSelectComponent.prototype.openPopup = function () {
        this.popupOpen = true;
        this.focusedTagIndex = undefined;
    };
    MultiSelectComponent.prototype.emitValueChange = function () {
        this.onChangeCallback(this.value);
        this.valueChange.emit(this.value);
    };
    MultiSelectComponent.prototype._toggle = function (open) {
        var _this = this;
        this._open = open;
        if (this.popupRef) {
            this.popupRef.popupElement
                .removeEventListener('mousedown', this.popupMouseDownHandler);
            this.popupRef.close();
            this.popupRef = null;
        }
        if (this._open) {
            this.popupRef = this.popupService.open({
                anchor: this.wrapper,
                animate: this.popupSettings.animate,
                appendTo: this.appendTo,
                content: this.popupTemplate,
                popupClass: this.listContainerClasses,
                positionMode: 'absolute'
            });
            var popupWrapper = this.popupRef.popupElement;
            var _a = this.width, min = _a.min, max = _a.max;
            popupWrapper.addEventListener('mousedown', this.popupMouseDownHandler);
            popupWrapper.style.minWidth = min;
            popupWrapper.style.width = max;
            popupWrapper.style.height = this.height;
            popupWrapper.setAttribute("dir", this.direction);
            this.popupRef.popupOpen.subscribe(this.popupOpened.bind(this));
            this.popupRef.popupAnchorViewportLeave.subscribe(function () { return _this.popupOpen = false; });
        }
    };
    MultiSelectComponent.prototype.getSelectedDataItems = function (valueChanges) {
        var _this = this;
        if (!this.data.length && this.valuePrimitive && this.valueField) {
            return [];
        }
        var selectedDataItems = [];
        valueChanges.forEachItem(function (value) {
            var currentValue = value.currentValue;
            var isBoundToComplexData = Object(_util__WEBPACK_IMPORTED_MODULE_0__["isObjectArray"])(_this.data);
            var index = _this.data.findIndex(function (item) {
                var value = _this.valuePrimitive ? currentValue : Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(currentValue, _this.valueField);
                return Object(_util__WEBPACK_IMPORTED_MODULE_0__["getter"])(item, _this.valueField) === value;
            });
            if (index !== -1) {
                _this.selectionService.select(index);
                selectedDataItems.push(_this.data[index]);
            }
            else if (Object(_util__WEBPACK_IMPORTED_MODULE_0__["isPresent"])(value) && !(isBoundToComplexData && _this.valuePrimitive)) {
                selectedDataItems.push(currentValue);
            }
        });
        return selectedDataItems;
    };
    MultiSelectComponent.prototype.nextTick = function (f) {
        var _this = this;
        this._zone.runOutsideAngular(function () {
            // Use `setTimeout` instead of a resolved promise
            // because the latter does not wait long enough.
            setTimeout(function () { return _this._zone.run(f); });
        });
    };
    MultiSelectComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"], args: [{
                    exportAs: 'kendoMultiSelect',
                    providers: [
                        MULTISELECT_VALUE_ACCESSOR,
                        _selection_service__WEBPACK_IMPORTED_MODULE_10__["SelectionService"],
                        _navigation_service__WEBPACK_IMPORTED_MODULE_11__["NavigationService"],
                        _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_24__["LocalizationService"],
                        {
                            provide: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_24__["L10N_PREFIX"],
                            useValue: 'kendo.multiselect'
                        }
                    ],
                    selector: 'kendo-multiselect',
                    template: "\n        <div class=\"k-multiselect-wrap k-floatwrap\"\n            #wrapper\n            (mousedown)=\"wrapperMousedown($event)\"\n        >\n            <kendo-taglist\n                [tags]=\"tags\"\n                [textField]=\"textField\"\n                [focused]=\"focusedTagIndex\"\n                [disabled]=\"disabled\"\n                [template]=\"tagTemplate\"\n                [groupTemplate]=\"groupTagTemplate\"\n                [activeId]=\"activeId\"\n                (removeTag)=\"handleRemoveTag($event)\"\n            >\n            </kendo-taglist>\n            <kendo-searchbar\n                #searchbar\n                [id]=\"focusableId\"\n                [role]=\"'listbox'\"\n                [listId]=\"listBoxId\"\n                [activeDescendant]=\"activeId\"\n                [userInput]=\"text\"\n                [disabled]=\"disabled\"\n                [readonly]=\"readonly\"\n                [tabIndex]=\"tabIndex\"\n                [popupOpen]=\"popupOpen\"\n                [placeholder]=\"placeholder\"\n                (onNavigate)=\"handleNavigate($event)\"\n                (valueChange)=\"handleFilter($event)\"\n                (onBlur)=\"blurComponent()\"\n                (onFocus)=\"focusComponent()\"\n            >\n            </kendo-searchbar>\n            <span *ngIf=\"!loading && !readonly && clearButton && (tags?.length || text?.length)\" class=\"k-icon k-clear-value k-i-close\" title=\"clear\" role=\"button\" tabindex=\"-1\" (mousedown)=\"clearAll($event)\"></span>\n            <span *ngIf=\"loading\" class=\"k-icon k-i-loading\"></span>\n        </div>\n        <ng-template #popupTemplate>\n            <!--header template-->\n            <ng-template *ngIf=\"headerTemplate\"\n                [templateContext]=\"{\n                    templateRef: headerTemplate.templateRef\n                }\">\n            </ng-template>\n            <!--custom item template-->\n            <div class=\"k-list\" *ngIf=\"allowCustom && text\">\n                <div class=\"k-item k-custom-item\" kendoDropDownsSelectable [multipleSelection]=\"true\" [index]=\"-1\">\n                    <ng-template *ngIf=\"customItemTemplate;else default_custom_item_template\"\n                        [templateContext]=\"{\n                            templateRef: customItemTemplate.templateRef,\n                            $implicit: text\n                        }\">\n                    </ng-template>\n                    <ng-template #default_custom_item_template>{{ text }}</ng-template>\n                    <span class=\"k-icon k-i-plus\" style=\"float: right\"></span>\n                </div>\n            </div>\n            <!--list-->\n            <kendo-list\n                [id]=\"listBoxId\"\n                [data]=\"data\"\n                [textField]=\"textField\"\n                [valueField]=\"valueField\"\n                [height]=\"listHeight\"\n                [template]=\"template\"\n                [show]=\"popupOpen\"\n                [multipleSelection]=\"true\"\n                >\n            </kendo-list>\n            <!--no data template-->\n            <div class=\"k-nodata\" *ngIf=\"data.length === 0\">\n                <ng-template [ngIf]=\"noDataTemplate\"\n                    [templateContext]=\"{\n                        templateRef: noDataTemplate ? noDataTemplate.templateRef : undefined\n                    }\">\n                </ng-template>\n                <ng-template [ngIf]=\"!noDataTemplate\">\n                    <div>NO DATA FOUND.</div>\n                </ng-template>\n            </div>\n            <!--footer template-->\n            <ng-template *ngIf=\"footerTemplate\"\n                [templateContext]=\"{\n                    templateRef: footerTemplate.templateRef\n                }\">\n            </ng-template>\n        </ng-template>\n        <ng-template [ngIf]=\"popupOpen\">\n            <kendo-resize-sensor (resize)=\"onResize()\"></kendo-resize-sensor>\n        </ng-template>\n        <ng-container #container></ng-container>\n  "
                },] },
    ];
    /** @nocollapse */
    MultiSelectComponent.ctorParameters = function () { return [
        { type: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_24__["LocalizationService"], },
        { type: _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_25__["PopupService"], },
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_10__["SelectionService"], },
        { type: _navigation_service__WEBPACK_IMPORTED_MODULE_11__["NavigationService"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["KeyValueDiffers"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"], },
    ]; };
    MultiSelectComponent.propDecorators = {
        'focusableId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'autoClose': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'loading': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'valueField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'textField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'tabindex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'tabIndex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"], args: ["tabIndex",] },],
        'placeholder': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'readonly': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'filterable': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'popupSettings': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'listHeight': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'valuePrimitive': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'clearButton': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'tagMapper': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'allowCustom': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'valueNormalizer': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] },],
        'filterChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] },],
        'valueChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] },],
        'open': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] },],
        'close': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] },],
        'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"], args: ['focus',] },],
        'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"], args: ['blur',] },],
        'removeTag': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] },],
        'container': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['container', { read: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewContainerRef"] },] },],
        'searchbar': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: [_searchbar_component__WEBPACK_IMPORTED_MODULE_1__["SearchBarComponent"],] },],
        'popupTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['popupTemplate',] },],
        'wrapper': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['wrapper',] },],
        'template': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_item_template_directive__WEBPACK_IMPORTED_MODULE_14__["ItemTemplateDirective"],] },],
        'customItemTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_15__["CustomItemTemplateDirective"],] },],
        'headerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_header_template_directive__WEBPACK_IMPORTED_MODULE_16__["HeaderTemplateDirective"],] },],
        'footerTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_17__["FooterTemplateDirective"],] },],
        'tagTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_18__["TagTemplateDirective"],] },],
        'groupTagTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_19__["GroupTagTemplateDirective"],] },],
        'noDataTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChild"], args: [_templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_20__["NoDataTemplateDirective"],] },],
        'widgetClasses': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['class.k-widget',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['class.k-multiselect',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['class.k-header',] },],
        'dir': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['attr.dir',] },],
        'focusedClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['class.k-state-focused',] },],
        'disabledClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"], args: ['class.k-state-disabled',] },],
    };
    return MultiSelectComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.module.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.module.js ***!
  \**************************************************************************************/
/*! exports provided: MultiSelectModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MultiSelectModule", function() { return MultiSelectModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _multiselect_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./multiselect.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.component.js");
/* harmony import */ var _taglist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./taglist.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/taglist.component.js");
/* harmony import */ var _templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./templates/tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/tag-template.directive.js");
/* harmony import */ var _templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./templates/custom-item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/custom-item-template.directive.js");
/* harmony import */ var _templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./templates/group-tag-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/group-tag-template.directive.js");
/* harmony import */ var _summary_tag_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./summary-tag.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/summary-tag.directive.js");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js");
/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");









var MULTISELECT_DIRECTIVES = [
    _multiselect_component__WEBPACK_IMPORTED_MODULE_1__["MultiSelectComponent"],
    _taglist_component__WEBPACK_IMPORTED_MODULE_2__["TagListComponent"],
    _templates_tag_template_directive__WEBPACK_IMPORTED_MODULE_3__["TagTemplateDirective"],
    _templates_group_tag_template_directive__WEBPACK_IMPORTED_MODULE_5__["GroupTagTemplateDirective"],
    _summary_tag_directive__WEBPACK_IMPORTED_MODULE_6__["SummaryTagDirective"],
    _templates_custom_item_template_directive__WEBPACK_IMPORTED_MODULE_4__["CustomItemTemplateDirective"]
];
/**
 * @hidden
 *
 * The exported package module.
 *
 * The package exports:
 * - `MultiSelectComponent`&mdash;The MultiSelect component class.
 * - `SummaryTagDirective`&mdash;The MultiSelect summary tag directive.
 * - `ItemTemplateDirective`&mdash;The item template directive.
 * - `CustomItemTemplateDirective`&mdash;The custom item template directive.
 * - `TagTemplateDirective`&mdash;The tag template directive.
 * - `SummaryTagTemplateDirective`&mdash;The summary tag template directive.
 * - `HeaderTemplateDirective`&mdash;The header template directive.
 * - `FooterTemplateDirective`&mdash;The footer template directive.
 * - `NoDataTemplateDirective`&mdash;The no-data template directive.
 */
var MultiSelectModule = /** @class */ (function () {
    function MultiSelectModule() {
    }
    MultiSelectModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [MULTISELECT_DIRECTIVES],
                    exports: [MULTISELECT_DIRECTIVES, _shared_directives_module__WEBPACK_IMPORTED_MODULE_8__["SharedDirectivesModule"]],
                    imports: [_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]]
                },] },
    ];
    /** @nocollapse */
    MultiSelectModule.ctorParameters = function () { return []; };
    return MultiSelectModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js ***!
  \*************************************************************************************/
/*! exports provided: NavigationAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationAction", function() { return NavigationAction; });
/**
 * @hidden
 */
var NavigationAction;
(function (NavigationAction) {
    NavigationAction[NavigationAction["Undefined"] = 0] = "Undefined";
    NavigationAction[NavigationAction["Open"] = 1] = "Open";
    NavigationAction[NavigationAction["Close"] = 2] = "Close";
    NavigationAction[NavigationAction["Enter"] = 3] = "Enter";
    NavigationAction[NavigationAction["Tab"] = 4] = "Tab";
    NavigationAction[NavigationAction["Esc"] = 5] = "Esc";
    NavigationAction[NavigationAction["Delete"] = 6] = "Delete";
    NavigationAction[NavigationAction["Backspace"] = 7] = "Backspace";
    NavigationAction[NavigationAction["Home"] = 8] = "Home";
    NavigationAction[NavigationAction["End"] = 9] = "End";
    NavigationAction[NavigationAction["Up"] = 10] = "Up";
    NavigationAction[NavigationAction["Down"] = 11] = "Down";
    NavigationAction[NavigationAction["Left"] = 12] = "Left";
    NavigationAction[NavigationAction["Right"] = 13] = "Right";
})(NavigationAction || (NavigationAction = {}));


/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation.service.js ***!
  \**************************************************************************************/
/*! exports provided: NavigationEvent, NavigationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationEvent", function() { return NavigationEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationService", function() { return NavigationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _navigation_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navigation-action */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/navigation-action.js");




var MIN_INDEX = 0;
/**
 * @hidden
 */
var NavigationEvent = /** @class */ (function () {
    /**
     * The index of the item to which the user navigated.
     */
    function NavigationEvent(index, originalEvent) {
        this.index = index;
        this.originalEvent = originalEvent;
    }
    return NavigationEvent;
}());

/**
 * @hidden
 */
var NavigationService = /** @class */ (function () {
    function NavigationService() {
        this.open = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.enter = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.tab = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.esc = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.up = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.right = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.down = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.left = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.delete = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.backspace = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.home = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.end = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    NavigationService.prototype.process = function (args) {
        var keyCode = args.originalEvent.keyCode;
        var altKey = args.originalEvent.altKey;
        var index;
        var action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Undefined;
        if (altKey && keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].down) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Open;
        }
        else if (altKey && keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].up) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Close;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].enter) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Enter;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].esc) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Esc;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].tab) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Tab;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].up) {
            index = this.next({ current: args.current, start: args.max, end: args.min, step: -1 });
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Up;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].left) {
            index = this.next({ current: args.current, start: args.max, end: args.min, step: -1 });
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Left;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].down) {
            index = this.next({ current: args.current, start: args.min, end: args.max, step: 1 });
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Down;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].right) {
            index = this.next({ current: args.current, start: args.min, end: args.max, step: 1 });
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Right;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].home) {
            index = MIN_INDEX;
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Home;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].end) {
            index = args.max;
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].End;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].delete) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Delete;
        }
        else if (keyCode === _common_keys__WEBPACK_IMPORTED_MODULE_2__["Keys"].backspace) {
            action = _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Backspace;
        }
        var eventData = new NavigationEvent(index, args.originalEvent);
        if (action !== _navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"].Undefined) {
            this[_navigation_action__WEBPACK_IMPORTED_MODULE_3__["NavigationAction"][action].toLowerCase()].emit(eventData);
        }
        return action;
    };
    NavigationService.prototype.next = function (args) {
        if (!Object(_util__WEBPACK_IMPORTED_MODULE_1__["isPresent"])(args.current)) {
            return args.start;
        }
        else {
            return args.current !== args.end ? args.current + args.step : args.end;
        }
    };
    NavigationService.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
    ];
    /** @nocollapse */
    NavigationService.ctorParameters = function () { return []; };
    return NavigationService;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js":
/*!***************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js ***!
  \***************************************************************************************/
/*! exports provided: SearchBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchBarComponent", function() { return SearchBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_keys__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./common/keys */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/common/keys.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @progress/kendo-angular-l10n */ "./node_modules/@progress/kendo-angular-l10n/dist/es/index.js");
/* tslint:disable:member-ordering */




/**
 * @hidden
 */
var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent(localization, renderer) {
        this.localization = localization;
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onNavigate = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this._userInput = "";
        this._previousValue = "";
        this._placeholder = "";
        this._composing = false;
        this.direction = localization.rtl ? 'rtl' : 'ltr';
        this.renderer = renderer;
    }
    Object.defineProperty(SearchBarComponent.prototype, "userInput", {
        get: function () {
            return this._userInput;
        },
        set: function (userInput) {
            this._userInput = userInput || "";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SearchBarComponent.prototype, "searchBarClass", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SearchBarComponent.prototype, "value", {
        get: function () {
            return this.input.nativeElement.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SearchBarComponent.prototype, "placeholder", {
        get: function () {
            return this._placeholder;
        },
        set: function (text) {
            this._placeholder = text || '';
            this.setInputSize();
        },
        enumerable: true,
        configurable: true
    });
    SearchBarComponent.prototype.compositionStart = function () {
        this._composing = true;
    };
    SearchBarComponent.prototype.compositionUpdate = function () {
        this._composing = true;
    };
    SearchBarComponent.prototype.compositionEnd = function () {
        this._composing = false;
        this._previousValue = this.value;
        this.valueChange.emit(this.value);
    };
    SearchBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.localizationChangeSubscription = this.localization
            .changes.subscribe(function (_a) {
            var rtl = _a.rtl;
            return _this.direction = rtl ? 'rtl' : 'ltr';
        });
    };
    SearchBarComponent.prototype.ngOnChanges = function (changes) {
        var previousUserInput;
        if (this.input && (changes.userInput || changes.suggestedText)) {
            if (changes.userInput && changes.userInput.previousValue) {
                if (this._previousValue === changes.userInput.previousValue) {
                    previousUserInput = this._previousValue;
                }
                else {
                    previousUserInput = changes.userInput.currentValue || "";
                }
            }
            else {
                previousUserInput = this._previousValue;
            }
            var caretIndex = this.input.nativeElement.selectionStart;
            var caretAtEnd = previousUserInput.length === caretIndex;
            this.writeInputValue(this.suggestedText ? Object(_util__WEBPACK_IMPORTED_MODULE_2__["combineStr"])(this.userInput, this.suggestedText) : this.userInput);
            if (this.suggestedText) {
                this.setInputSelection(this.userInput.length, this.suggestedText.length);
            }
            else if (caretAtEnd) {
                this.setInputSelection(this.userInput.length, this.userInput.length);
            }
            else {
                this.setInputSelection(caretIndex, caretIndex);
            }
            this._previousValue = this.userInput;
        }
    };
    SearchBarComponent.prototype.ngOnDestroy = function () {
        if (this.localizationChangeSubscription) {
            this.localizationChangeSubscription.unsubscribe();
        }
    };
    SearchBarComponent.prototype.writeInputValue = function (text) {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_2__["isDocumentAvailable"])()) {
            this.renderer.setProperty(this.input.nativeElement, 'value', text);
        }
    };
    SearchBarComponent.prototype.setInputSelection = function (start, end) {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_2__["isDocumentAvailable"])() && this.input.nativeElement === document.activeElement) {
            try {
                this.input.nativeElement.setSelectionRange(start, end);
            }
            catch (e) {
                //Make sure element is in the DOM before invoking its methods
            }
        }
    };
    SearchBarComponent.prototype.handleInput = function (event) {
        var value = event.target.value;
        if (value !== this.userInput && !this._composing) {
            this._previousValue = value;
            this.valueChange.emit(value);
        }
    };
    SearchBarComponent.prototype.handleFocus = function (event) {
        this.onFocus.emit(event);
    };
    SearchBarComponent.prototype.handleBlur = function (event) {
        this.onBlur.emit(event);
    };
    SearchBarComponent.prototype.handleKeydown = function (event) {
        var keyCode = event.keyCode;
        var keys = [_common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].up, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].down, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].left, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].right, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].enter,
            _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].esc, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].delete, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].backspace, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].home, _common_keys__WEBPACK_IMPORTED_MODULE_1__["Keys"].end];
        if (keys.indexOf(keyCode) > -1) {
            this.onNavigate.emit(event);
        }
    };
    SearchBarComponent.prototype.focus = function () {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_2__["isDocumentAvailable"])()) {
            this.input.nativeElement.focus();
        }
    };
    SearchBarComponent.prototype.blur = function () {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_2__["isDocumentAvailable"])()) {
            this.input.nativeElement.blur();
        }
    };
    SearchBarComponent.prototype.setInputSize = function () {
        var lengthOf = function (x) { return x ? x.length : 0; };
        var input = this.input.nativeElement;
        var placeholderLength = lengthOf(this.placeholder);
        var textLength = lengthOf(this.value);
        var size = Math.max(placeholderLength, textLength, 1);
        this.renderer.setAttribute(input, 'size', size.toString());
    };
    SearchBarComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'kendo-searchbar',
                    template: "\n        <input #input\n            autocomplete=\"off\"\n            [id]=\"id\"\n            [disabled]=\"disabled\"\n            [readonly]=\"readonly\"\n            [placeholder]=\"placeholder\"\n            [class]=\"'k-input'\"\n            (input)=\"handleInput($event)\"\n            (focus)=\"handleFocus($event)\"\n            (blur)=\"handleBlur($event)\"\n            (keydown)=\"handleKeydown($event)\"\n            [attr.tabIndex]=\"tabIndex\"\n            [attr.dir]=\"direction\"\n            [attr.role]=\"role\"\n            [attr.aria-disabled]=\"disabled\"\n            [attr.aria-readonly]=\"readonly\"\n            [attr.aria-haspopup]=\"true\"\n            [attr.aria-expanded]=\"popupOpen\"\n            [attr.aria-owns]=\"listId\"\n            [attr.aria-activedescendant]=\"activeDescendant\"\n        />\n   "
                },] },
    ];
    /** @nocollapse */
    SearchBarComponent.ctorParameters = function () { return [
        { type: _progress_kendo_angular_l10n__WEBPACK_IMPORTED_MODULE_3__["LocalizationService"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
    ]; };
    SearchBarComponent.propDecorators = {
        'id': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'listId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'activeDescendant': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'readonly': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'tabIndex': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'popupOpen': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'role': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'userInput': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'suggestedText': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'valueChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'onNavigate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'input': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ["input",] },],
        'searchBarClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-searchbar',] },],
        'placeholder': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'compositionStart': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ["compositionstart",] },],
        'compositionUpdate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ["compositionupdate",] },],
        'compositionEnd': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ["compositionend",] },],
    };
    return SearchBarComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selectable.directive.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/selectable.directive.js ***!
  \****************************************************************************************/
/*! exports provided: SelectableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectableDirective", function() { return SelectableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _selection_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selection.service */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js");


/**
 * @hidden
 */
var SelectableDirective = /** @class */ (function () {
    function SelectableDirective(selectionService) {
        this.multipleSelection = false;
        this.selectionService = selectionService;
    }
    Object.defineProperty(SelectableDirective.prototype, "focusedClassName", {
        get: function () {
            return this.selectionService.isFocused(this.index);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectableDirective.prototype, "selectedClassName", {
        get: function () {
            return this.selectionService.isSelected(this.index);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectableDirective.prototype, "ariaSelected", {
        get: function () {
            return this.selectionService.isSelected(this.index) ? true : undefined;
        },
        enumerable: true,
        configurable: true
    });
    SelectableDirective.prototype.onClick = function (event) {
        event.stopPropagation();
        if (this.multipleSelection) {
            if (this.selectionService.isSelected(this.index)) {
                this.selectionService.unselect(this.index);
            }
            else {
                this.selectionService.add(this.index);
            }
        }
        else {
            this.selectionService.change(this.index);
        }
    };
    SelectableDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownsSelectable]'
                },] },
    ];
    /** @nocollapse */
    SelectableDirective.ctorParameters = function () { return [
        { type: _selection_service__WEBPACK_IMPORTED_MODULE_1__["SelectionService"], },
    ]; };
    SelectableDirective.propDecorators = {
        'index': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'multipleSelection': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'focusedClassName': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-state-focused',] },],
        'selectedClassName': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.k-state-selected',] },],
        'ariaSelected': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.aria-selected',] },],
        'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click', ['$event'],] },],
    };
    return SelectableDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/selection.service.js ***!
  \*************************************************************************************/
/*! exports provided: SelectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectionService", function() { return SelectionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");


/**
 * @hidden
 */
var SelectionService = /** @class */ (function () {
    function SelectionService() {
        this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectedIndices = [];
    }
    SelectionService.prototype.isSelected = function (index) {
        return Object(_util__WEBPACK_IMPORTED_MODULE_1__["isPresent"])(this.selectedIndices.find(function (current) { return current === index; }));
    };
    SelectionService.prototype.isFocused = function (index) {
        return index === this.focused;
    };
    SelectionService.prototype.focus = function (index) {
        if (this.isFocused(index)) {
            return;
        }
        this.focused = index;
        this.onFocus.emit(index);
    };
    SelectionService.prototype.select = function (index) {
        if (this.isSelected(index)) {
            return;
        }
        this.onSelect.emit({
            indices: [index]
        });
        this.selectedIndices = [index];
        this.focused = index;
    };
    SelectionService.prototype.add = function (index) {
        if (this.isSelected(index)) {
            return;
        }
        this.selectedIndices.push(index);
        this.focused = index;
        this.onChange.emit({
            added: index,
            indices: this.selectedIndices.slice()
        });
    };
    SelectionService.prototype.unselect = function (index) {
        if (!this.isSelected(index)) {
            return;
        }
        var position = this.selectedIndices.indexOf(index);
        this.selectedIndices.splice(position, 1);
        this.focused = index;
        this.onChange.emit({
            indices: this.selectedIndices.slice(),
            removed: index
        });
    };
    SelectionService.prototype.change = function (index) {
        this.onChange.emit({
            indices: [index]
        });
        this.selectedIndices = [index];
        this.focused = index;
    };
    SelectionService.prototype.resetSelection = function (index) {
        this.selectedIndices = index instanceof Array ? index : [index];
        this.focused = this.selectedIndices[this.selectedIndices.length - 1];
    };
    Object.defineProperty(SelectionService.prototype, "selected", {
        get: function () {
            return this.selectedIndices.slice();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectionService.prototype, "focused", {
        get: function () {
            return this.focusedIndex;
        },
        set: function (index) {
            if (this.focusedIndex !== index) {
                this.focusedIndex = index;
                this.onFocus.emit(index);
            }
        },
        enumerable: true,
        configurable: true
    });
    SelectionService.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
    ];
    /** @nocollapse */
    SelectionService.ctorParameters = function () { return []; };
    return SelectionService;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js ***!
  \********************************************************************************************/
/*! exports provided: SharedDirectivesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function() { return SharedDirectivesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./templates/item-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js");
/* harmony import */ var _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./templates/header-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js");
/* harmony import */ var _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./templates/footer-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js");
/* harmony import */ var _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./templates/no-data-template.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js");





var SHARED_DIRECTIVES = [
    _templates_header_template_directive__WEBPACK_IMPORTED_MODULE_2__["HeaderTemplateDirective"],
    _templates_footer_template_directive__WEBPACK_IMPORTED_MODULE_3__["FooterTemplateDirective"],
    _templates_item_template_directive__WEBPACK_IMPORTED_MODULE_1__["ItemTemplateDirective"],
    _templates_no_data_template_directive__WEBPACK_IMPORTED_MODULE_4__["NoDataTemplateDirective"]
];
/**
 * @hidden
 *
 * The exported package module.
 *
 * The package exports:
 * - `ItemTemplateDirective`&mdash;The item template directive.
 * - `HeaderTemplateDirective`&mdash;The header template directive.
 * - `FooterTemplateDirective`&mdash;The footer template directive.
 * - `NoDataTemplateDirective`&mdash;The noData template directive.
 */
var SharedDirectivesModule = /** @class */ (function () {
    function SharedDirectivesModule() {
    }
    SharedDirectivesModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [SHARED_DIRECTIVES],
                    exports: [SHARED_DIRECTIVES]
                },] },
    ];
    /** @nocollapse */
    SharedDirectivesModule.ctorParameters = function () { return []; };
    return SharedDirectivesModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared.module.js ***!
  \*********************************************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list.component.js");
/* harmony import */ var _searchbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./searchbar.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/searchbar.component.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_resize_sensor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @progress/kendo-angular-resize-sensor */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/index.js");
/* harmony import */ var _shared_directives_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared-directives.module */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/shared-directives.module.js");
/* harmony import */ var _list_item_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./list-item.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/list-item.directive.js");
/* harmony import */ var _selectable_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./selectable.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/selectable.directive.js");
/* harmony import */ var _templates_template_context_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./templates/template-context.directive */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/template-context.directive.js");











var INTERNAL_DIRECTIVES = [
    _list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"],
    _list_item_directive__WEBPACK_IMPORTED_MODULE_8__["ListItemDirective"],
    _selectable_directive__WEBPACK_IMPORTED_MODULE_9__["SelectableDirective"],
    _searchbar_component__WEBPACK_IMPORTED_MODULE_4__["SearchBarComponent"],
    _templates_template_context_directive__WEBPACK_IMPORTED_MODULE_10__["TemplateContextDirective"]
];
/**
 * @hidden
 */
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [INTERNAL_DIRECTIVES],
                    exports: [INTERNAL_DIRECTIVES, _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_5__["PopupModule"], _progress_kendo_angular_resize_sensor__WEBPACK_IMPORTED_MODULE_6__["ResizeSensorModule"], _shared_directives_module__WEBPACK_IMPORTED_MODULE_7__["SharedDirectivesModule"]],
                    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_5__["PopupModule"], _progress_kendo_angular_resize_sensor__WEBPACK_IMPORTED_MODULE_6__["ResizeSensorModule"], _shared_directives_module__WEBPACK_IMPORTED_MODULE_7__["SharedDirectivesModule"]]
                },] },
    ];
    /** @nocollapse */
    SharedModule.ctorParameters = function () { return []; };
    return SharedModule;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/summary-tag.directive.js":
/*!*****************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/summary-tag.directive.js ***!
  \*****************************************************************************************/
/*! exports provided: SummaryTagDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SummaryTagDirective", function() { return SummaryTagDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js");
/* harmony import */ var _multiselect_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./multiselect.component */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/multiselect.component.js");



/**
 * A Directive which configures the MultiSelect to show one single summary tag for all selected data items.
 * When a number is provided, the summary tag is displayed after the given amount of data items are selected.
 * @example
 * ```ts-no-run
 * <kendo-multiselect kendoMultiSelectSummaryTag [data]="data"></kendo-multiselect>
 * ```
 *
 * @example
 * ```ts-no-run
 * <kendo-multiselect [kendoMultiSelectSummaryTag]="2" [data]="data"></kendo-multiselect>
 * ```
 */
var SummaryTagDirective = /** @class */ (function () {
    function SummaryTagDirective(multiSelectComponent) {
        this.multiSelectComponent = multiSelectComponent;
        /**
         * A numeric value that indicates the number of selected data items after which the summary tag will appear.
         */
        this.showAfter = 0; // tslint:disable-line:no-input-rename
        this.createTagMapper();
    }
    SummaryTagDirective.prototype.ngOnChanges = function (changes) {
        if (Object(_util__WEBPACK_IMPORTED_MODULE_1__["isChanged"])("showAfter", changes)) {
            this.createTagMapper();
            this.multiSelectComponent.onTagMapperChange();
        }
    };
    SummaryTagDirective.prototype.createTagMapper = function () {
        var _this = this;
        this.multiSelectComponent.tagMapper = function (tags) {
            if (tags.length > _this.showAfter) {
                var result = void 0;
                result = tags.slice(0, _this.showAfter);
                result.push(tags.slice(_this.showAfter, tags.length));
                return result;
            }
            else {
                return tags;
            }
        };
    };
    SummaryTagDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoMultiSelectSummaryTag]'
                },] },
    ];
    /** @nocollapse */
    SummaryTagDirective.ctorParameters = function () { return [
        { type: _multiselect_component__WEBPACK_IMPORTED_MODULE_2__["MultiSelectComponent"], },
    ]; };
    SummaryTagDirective.propDecorators = {
        'showAfter': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['kendoMultiSelectSummaryTag',] },],
    };
    return SummaryTagDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/taglist.component.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/taglist.component.js ***!
  \*************************************************************************************/
/*! exports provided: TagListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagListComponent", function() { return TagListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * @hidden
 */
var TagListComponent = /** @class */ (function () {
    function TagListComponent() {
        this.removeTag = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    TagListComponent.prototype.tagText = function (tag) {
        return tag[this.textField] ? tag[this.textField] : tag;
    };
    TagListComponent.prototype.deleteTag = function (event, tag) {
        event.preventDefault();
        event.stopImmediatePropagation();
        if (!this.disabled && event.which === 1) {
            this.removeTag.emit(tag);
        }
    };
    TagListComponent.prototype.itemId = function (focused, index) {
        return focused === index ? this.activeId : undefined;
    };
    TagListComponent.prototype.isGroupTag = function (tag) {
        return tag instanceof Array;
    };
    TagListComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'kendo-taglist',
                    template: "\n        <ul role=\"listbox\" class=\"k-reset\">\n            <li *ngFor=\"let tag of tags; let i = index;\"\n                [ngClass]=\"{'k-button': true, 'k-state-focused': i === focused }\"\n                [attr.id]=\"itemId(focused, i)\"\n            >\n                <ng-template *ngIf=\"isGroupTag(tag); then groupTag else singleTag\"></ng-template>\n                    <ng-template #groupTag>\n                        <span>\n                            <ng-template *ngIf=\"groupTemplate\"\n                                [templateContext]=\"{\n                                templateRef: groupTemplate.templateRef,\n                                $implicit: tag\n                            }\">\n                            </ng-template>\n                            <ng-template [ngIf]=\"!groupTemplate\">{{ tag.length }} {{ tag.length === 1 ? 'item' : 'items' }} selected</ng-template>\n                        </span>\n                    </ng-template>\n                    <ng-template #singleTag>\n                        <span>\n                            <ng-template *ngIf=\"template\"\n                                [templateContext]=\"{\n                                templateRef: template.templateRef,\n                                $implicit: tag\n                            }\">\n                            </ng-template>\n                            <ng-template [ngIf]=\"!template\">{{ tagText(tag) }}</ng-template>\n                        </span>\n                    </ng-template>\n\n                <span aria-label=\"delete\" class=\"k-select\">\n                    <span class=\"k-icon k-i-close\" (mousedown)=\"deleteTag($event, tag)\">\n                    </span>\n                </span>\n            </li>\n        </ul>\n  "
                },] },
    ];
    /** @nocollapse */
    TagListComponent.ctorParameters = function () { return []; };
    TagListComponent.propDecorators = {
        'tags': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'textField': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'focused': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'template': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'groupTemplate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'activeId': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'removeTag': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    };
    return TagListComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/custom-item-template.directive.js":
/*!************************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/custom-item-template.directive.js ***!
  \************************************************************************************************************/
/*! exports provided: CustomItemTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomItemTemplateDirective", function() { return CustomItemTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the custom list item content.
 * The template context is set to the current component.
 * To get a reference to the current text that is typed by the user,
 * use the `let-customItem` directive.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-multiselect [data]="listItems" [allowCustom]="true">
 *    <ng-template kendoMultiSelectCustomItemTemplate let-customItem>
 *      <span>New Item: {{customItem}}</span>
 *    </ng-template>
 *  </kendo-multiselect>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_multiselect %}).
 */
var CustomItemTemplateDirective = /** @class */ (function () {
    function CustomItemTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    CustomItemTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoMultiSelectCustomItemTemplate]'
                },] },
    ];
    /** @nocollapse */
    CustomItemTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return CustomItemTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/footer-template.directive.js ***!
  \*******************************************************************************************************/
/*! exports provided: FooterTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterTemplateDirective", function() { return FooterTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the footer content of the list.
 *
 * To define the footer template, nest a `<ng-template>` tag with the `kendo<ComponentName>FooterTemplate` directive inside the component tag.
 *
 * You can use:
 * - The `kendoAutoCompleteFooterTemplate` directive for the AutoComplete.
 * - The `kendoComboBoxFooterTemplate` directive for the ComboBox.
 * - The `kendoDropDownListFooterTemplate` directive for the DropDownList.
 * - The `kendoMultiSelectFooterTemplate` directive for the MultiSelect.
 *
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-combobox [data]="listItems">
 *    <ng-template kendoComboBoxFooterTemplate>
 *      <h4>Footer template</h4>
 *    </ng-template>
 *  </kendo-combobox>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_ddl %}).
 */
var FooterTemplateDirective = /** @class */ (function () {
    function FooterTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    FooterTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownListFooterTemplate],[kendoComboBoxFooterTemplate],[kendoAutoCompleteFooterTemplate],[kendoMultiSelectFooterTemplate]'
                },] },
    ];
    /** @nocollapse */
    FooterTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return FooterTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/group-tag-template.directive.js":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/group-tag-template.directive.js ***!
  \**********************************************************************************************************/
/*! exports provided: GroupTagTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupTagTemplateDirective", function() { return GroupTagTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the grouped tag values.
 * It can only be used with the MultiSelect component.
 *
 * The template context is set to the current MultiSelect.
 * To get a reference to the current grouped data items collection, use the `let-dataItems` directive.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-multiselect kendoMultiSelectSummaryTag [data]="items">
 *    <ng-template kendoMultiSelectGroupTagTemplate let-dataItems>
 *      <span>{{dataItems.length}} item(s) selected</span>
 *    </ng-template>
 *  </kendo-multiselect>
 * `
 * })
 * class AppComponent {
 *   public items: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_multiselect %}).
 */
var GroupTagTemplateDirective = /** @class */ (function () {
    function GroupTagTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    GroupTagTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoMultiSelectGroupTagTemplate]'
                },] },
    ];
    /** @nocollapse */
    GroupTagTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return GroupTagTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/header-template.directive.js ***!
  \*******************************************************************************************************/
/*! exports provided: HeaderTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderTemplateDirective", function() { return HeaderTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the header content of the list.
 *
 * To define the header template, nest a `<ng-template>` tag with the `kendo<ComponentName>HeaderTemplate` directive inside the component tag.
 *
 * You can use:
 * - The `kendoAutoCompleteHeaderTemplate` directive for the AutoComplete.
 * - The `kendoComboBoxHeaderTemplate` directive for the ComboBox.
 * - The `kendoDropDownListHeaderTemplate` directive for the DropDownList.
 * - The `kendoMultiSelectHeaderTemplate` directive for the MultiSelect.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-combobox [data]="listItems">
 *    <ng-template kendoComboBoxHeaderTemplate>
 *      <h4>Header template</h4>
 *    </ng-template>
 *  </kendo-combobox>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 * For more examples, refer to the article on [templates]({% slug templates_ddl %}).
 */
var HeaderTemplateDirective = /** @class */ (function () {
    function HeaderTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    HeaderTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownListHeaderTemplate],[kendoComboBoxHeaderTemplate],[kendoAutoCompleteHeaderTemplate],[kendoMultiSelectHeaderTemplate]'
                },] },
    ];
    /** @nocollapse */
    HeaderTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return HeaderTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/item-template.directive.js ***!
  \*****************************************************************************************************/
/*! exports provided: ItemTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemTemplateDirective", function() { return ItemTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the list item content.
 *
 * To define the item template, nest a `<ng-template>` tag with the `kendo<ComponentName>ItemTemplate` directive inside the component tag.
 *
 * You can use:
 * - The `kendoAutoCompleteItemTemplate` directive for the AutoComplete.
 * - The `kendoComboBoxItemTemplate` directive for the ComboBox.
 * - The `kendoDropDownListItemTemplate` directive for the DropDownList.
 * - The `kendoMultiSelectItemTemplate` directive for the MultiSelect.
 *
 * The template context is set to the current component. To get a reference to the current data item, use the `let-dataItem` directive.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-combobox [data]="listItems">
 *    <ng-template kendoComboBoxItemTemplate let-dataItem>
 *      <span>{{dataItem}} option</span>
 *    </ng-template>
 *  </kendo-combobox>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_ddl %}).
 */
var ItemTemplateDirective = /** @class */ (function () {
    function ItemTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    ItemTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownListItemTemplate],[kendoComboBoxItemTemplate],[kendoAutoCompleteItemTemplate],[kendoMultiSelectItemTemplate]'
                },] },
    ];
    /** @nocollapse */
    ItemTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return ItemTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js":
/*!********************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/no-data-template.directive.js ***!
  \********************************************************************************************************/
/*! exports provided: NoDataTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoDataTemplateDirective", function() { return NoDataTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering content when there is no data present.
 *
 * To define the no-data template, nest a `<ng-template>` tag with the `kendo<ComponentName>NoDataTemplate` directive inside the component tag.
 *
 * You can use:
 * - The `kendoAutoCompleteNoDataTemplate` directive for the AutoComplete.
 * - The `kendoComboBoxNoDataTemplate` directive for the ComboBox.
 * - The `kendoDropDownListNoDataTemplate` directive for the DropDownList.
 * - The `kendoMultiSelectNoDataTemplate` directive for the MultiSelect.
 *
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-combobox [data]="listItems">
 *    <ng-template kendoComboBoxNoDataTemplate>
 *      <h4>No data!</h4>
 *    </ng-template>
 *  </kendo-combobox>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = [];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_ddl %}).
 */
var NoDataTemplateDirective = /** @class */ (function () {
    function NoDataTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    NoDataTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownListNoDataTemplate],[kendoComboBoxNoDataTemplate],[kendoAutoCompleteNoDataTemplate],[kendoMultiSelectNoDataTemplate]'
                },] },
    ];
    /** @nocollapse */
    NoDataTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return NoDataTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/tag-template.directive.js":
/*!****************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/tag-template.directive.js ***!
  \****************************************************************************************************/
/*! exports provided: TagTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagTemplateDirective", function() { return TagTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the selected tag value. It can only be used with the MultiSelect component.
 *
 * The template context is set to the current MultiSelect. To get a reference to the current data item, use the `let-dataItem` directive.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-multiselect [data]="items">
 *    <ng-template kendoMultiSelectTagTemplate let-dataItem>
 *      <span>{{dataItem}} option</span>
 *    </ng-template>
 *  </kendo-multiselect>
 * `
 * })
 * class AppComponent {
 *   public items: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_multiselect %}).
 */
var TagTemplateDirective = /** @class */ (function () {
    function TagTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    TagTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoMultiSelectTagTemplate]'
                },] },
    ];
    /** @nocollapse */
    TagTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return TagTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/template-context.directive.js":
/*!********************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/template-context.directive.js ***!
  \********************************************************************************************************/
/*! exports provided: TemplateContextDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateContextDirective", function() { return TemplateContextDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * @hidden
 */
var TemplateContextDirective = /** @class */ (function () {
    function TemplateContextDirective(viewContainerRef) {
        this.viewContainerRef = viewContainerRef;
    }
    Object.defineProperty(TemplateContextDirective.prototype, "templateContext", {
        set: function (context) {
            if (this.insertedViewRef) {
                this.viewContainerRef.remove(this.viewContainerRef.indexOf(this.insertedViewRef));
                this.insertedViewRef = undefined;
            }
            if (context.templateRef) {
                this.insertedViewRef = this.viewContainerRef.createEmbeddedView(context.templateRef, context);
            }
        },
        enumerable: true,
        configurable: true
    });
    TemplateContextDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[templateContext]' // tslint:disable-line
                },] },
    ];
    /** @nocollapse */
    TemplateContextDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], },
    ]; };
    TemplateContextDirective.propDecorators = {
        'templateContext': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    };
    return TemplateContextDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/value-template.directive.js":
/*!******************************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/templates/value-template.directive.js ***!
  \******************************************************************************************************/
/*! exports provided: ValueTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValueTemplateDirective", function() { return ValueTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* tslint:disable:max-line-length */

/**
 * Used for rendering the selected value of the component. It can only be used with the DropDownList.
 *
 * The template context is set to the current DropDownList. To get a reference to the current data item, use the `let-dataItem` directive.
 *
 * @example
 * ```ts
 * _@Component({
 * selector: 'my-app',
 * template: `
 *  <kendo-dropdownlist [data]="listItems">
 *    <ng-template kendoDropDownListValueTemplate let-dataItem>
 *      <span>{{dataItem}} option</span>
 *    </ng-template>
 *  </kendo-dropdownlist>
 * `
 * })
 * class AppComponent {
 *   public listItems: Array<string> = ["Item 1", "Item 2", "Item 3", "Item 4"];
 * }
 * ```
 *
 * For more examples, refer to the article on [templates]({% slug templates_ddl %}).
 */
var ValueTemplateDirective = /** @class */ (function () {
    function ValueTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    ValueTemplateDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[kendoDropDownListValueTemplate]'
                },] },
    ];
    /** @nocollapse */
    ValueTemplateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
    ]; };
    return ValueTemplateDirective;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/touch-enabled.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/touch-enabled.js ***!
  \*********************************************************************************/
/*! exports provided: TOUCH_ENABLED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOUCH_ENABLED", function() { return TOUCH_ENABLED; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * @hidden
 */
var TOUCH_ENABLED = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('dropdowns-touch-enabled');


/***/ }),

/***/ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js":
/*!************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-dropdowns/dist/es/util.js ***!
  \************************************************************************/
/*! exports provided: isPresent, isNumber, isChanged, guid, combineStr, isDocumentAvailable, isWindowAvailable, isArray, isObject, resolveValuesInArray, validateComplexValues, resolveAllValues, isObjectArray, selectedIndices, getter, resolveValue, sameCharsOnly, shuffleData, matchText */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPresent", function() { return isPresent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return isNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isChanged", function() { return isChanged; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "guid", function() { return guid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "combineStr", function() { return combineStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDocumentAvailable", function() { return isDocumentAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isWindowAvailable", function() { return isWindowAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isArray", function() { return isArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return isObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolveValuesInArray", function() { return resolveValuesInArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateComplexValues", function() { return validateComplexValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolveAllValues", function() { return resolveAllValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObjectArray", function() { return isObjectArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectedIndices", function() { return selectedIndices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getter", function() { return getter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolveValue", function() { return resolveValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sameCharsOnly", function() { return sameCharsOnly; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shuffleData", function() { return shuffleData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matchText", function() { return matchText; });
/* tslint:disable:no-null-keyword */
/* tslint:disable:no-bitwise */
/* tslint:disable:align */
/**
 * @hidden
 */
var isPresent = function (value) { return value !== null && value !== undefined; };
/**
 * @hidden
 */
var isNumber = function (value) { return !isNaN(value); };
/**
 * @hidden
 */
var isChanged = function (propertyName, changes) { return (changes[propertyName] && !changes[propertyName].isFirstChange() &&
    changes[propertyName].previousValue !== changes[propertyName].currentValue); };
/**
 * @hidden
 */
var guid = function () {
    var id = "";
    var i;
    var random;
    for (i = 0; i < 32; i++) {
        random = Math.random() * 16 | 0;
        if (i === 8 || i === 12 || i === 16 || i === 20) {
            id += "-";
        }
        id += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
    }
    return id;
};
/**
 * @hidden
 */
var combineStr = function (begin, end) {
    return begin.concat(end.substr(end.toLowerCase().indexOf(begin.toLowerCase()) + begin.length));
};
/**
 * @hidden
 */
var isDocumentAvailable = function () { return typeof document !== 'undefined'; };
/**
 * @hidden
 */
var isWindowAvailable = function () { return typeof window !== 'undefined'; };
/**
 * @hidden
 */
var isArray = function (value) { return Array.isArray(value); };
/**
 * @hidden
 */
var isObject = function (value) { return typeof value === 'object'; };
/**
 * @hidden
 */
var resolveValuesInArray = function (values, data, valueField) {
    if (data === void 0) { data = []; }
    return data.filter(function (curr) {
        return values.some(function (item) { return item === curr[valueField]; });
    });
};
/**
 * @hidden
 */
var validateComplexValues = function (values, valueField) {
    return isArray(values) && values.filter(function (item) {
        return isObject(item) && item[valueField];
    });
};
/**
 * @hidden
 */
var resolveAllValues = function (value, data, valueField) {
    var customValues = validateComplexValues(value, valueField) || [];
    var resolvedValues = resolveValuesInArray(value, data, valueField) || [];
    return resolvedValues.concat(customValues);
};
/**
 * @hidden
 */
var isObjectArray = function (values) {
    return isArray(values) && values.some(function (item) { return isObject(item); });
};
/**
 * @hidden
 */
var selectedIndices = function (values, data, valueField) {
    var extractedValues = data.map(function (item) {
        return isPresent(item[valueField]) ? item[valueField] : item;
    });
    return values.reduce(function (arr, item) {
        var value = isPresent(item[valueField]) ? item[valueField] : item;
        var index = extractedValues.indexOf(value);
        if (index !== -1) {
            arr.push(index);
        }
        return arr;
    }, []);
};
/**
 * @hidden
 */
var getter = function (dataItem, field, usePrimitive) {
    if (usePrimitive === void 0) { usePrimitive = false; }
    if (isPresent(dataItem)) {
        if (usePrimitive) {
            return field && isPresent(dataItem[field]) ? dataItem[field] : dataItem;
        }
        else {
            return field ? dataItem[field] : dataItem;
        }
    }
};
/**
 * @hidden
 */
var resolveValue = function (args) {
    var dataItem;
    if (isPresent(args.value)) {
        var data = [args.defaultItem].concat(args.data);
        dataItem = data.find(function (element) { return getter(element, args.valueField) === args.value; });
        return {
            dataItem: dataItem,
            focused: args.data.indexOf(dataItem),
            selected: args.data.indexOf(dataItem)
        };
    }
    else if (args.index) {
        dataItem = args.data[args.index];
        return {
            dataItem: args.data[args.index],
            focused: args.index,
            selected: args.index
        };
    }
    return {
        dataItem: args.defaultItem,
        focused: -1,
        selected: -1
    };
};
/**
 * @hidden
 */
var sameCharsOnly = function (word, character) {
    for (var idx = 0; idx < word.length; idx++) {
        if (word.charAt(idx) !== character) {
            return false;
        }
    }
    return true;
};
/**
 * @hidden
 */
var shuffleData = function (data, splitIndex, defaultItem) {
    var result = data;
    if (defaultItem) {
        result = [defaultItem].concat(result);
    }
    return result.slice(splitIndex).concat(result.slice(0, splitIndex));
};
/**
 * @hidden
 */
var matchText = function (text, word, ignoreCase) {
    if (!isPresent(text)) {
        return false;
    }
    var temp = String(text);
    if (ignoreCase) {
        temp = temp.toLowerCase();
    }
    return temp.indexOf(word) === 0;
};


/***/ }),

/***/ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/index.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-resize-sensor/dist/es/index.js ***!
  \*****************************************************************************/
/*! exports provided: ResizeSensorComponent, ResizeSensorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/main.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorComponent", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ResizeSensorComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorModule", function() { return _main__WEBPACK_IMPORTED_MODULE_0__["ResizeSensorModule"]; });

/**
 * Generated bundle index. Do not edit.
 */



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/main.js":
/*!****************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-resize-sensor/dist/es/main.js ***!
  \****************************************************************************/
/*! exports provided: ResizeSensorComponent, ResizeSensorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _resize_sensor_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./resize-sensor.component */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorComponent", function() { return _resize_sensor_component__WEBPACK_IMPORTED_MODULE_0__["ResizeSensorComponent"]; });

/* harmony import */ var _resize_sensor_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./resize-sensor.module */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorModule", function() { return _resize_sensor_module__WEBPACK_IMPORTED_MODULE_1__["ResizeSensorModule"]; });





/***/ }),

/***/ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.component.js":
/*!***********************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.component.js ***!
  \***********************************************************************************************/
/*! exports provided: ResizeSensorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorComponent", function() { return ResizeSensorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_operators_auditTime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/auditTime */ "./node_modules/rxjs-compat/_esm5/operators/auditTime.js");



/**
 * Emit up to 10 resize events per second by default.
 * Chosen as a compromise between responsiveness and performance.
 */
var DEFAULT_RATE_LIMIT = 10;
var computedProp = function (elem, prop) {
    return getComputedStyle(elem, null).getPropertyValue(prop);
};
var WRAP_STYLE = 'position: absolute; display: block; left: 0; top: 0; right: 0; bottom: 0; z-index: -1;' +
    'overflow: hidden; visibility: hidden;';
var EXPAND_CHILD_STYLE = 'position: absolute; left: 0; top: 0; transition: 0s;';
var SHRINK_CHILD_STYLE = EXPAND_CHILD_STYLE + 'width: 200%; height: 200%;';
/**
 * Resize Sensor Component
 *
 * Triggers a "resize" event whenever the parent DOM element size changes.
 */
var ResizeSensorComponent = /** @class */ (function () {
    function ResizeSensorComponent(element, zone, renderer) {
        this.element = element;
        this.zone = zone;
        this.renderer = renderer;
        /**
         * The maximum number of resize events to emit per second.
         *
         * Defaults to 10.
         */
        this.rateLimit = DEFAULT_RATE_LIMIT;
        /**
         * Fires when the parent DOM element has been resized.
         */
        this.resize = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.source = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.initialized = false;
        this.acceptedSize = false;
    }
    ResizeSensorComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.zone.runOutsideAngular(function () {
            var scrollHandler = _this.scroll.bind(_this);
            var detachExpand = _this.renderer.listen(_this.expand.nativeElement, 'scroll', scrollHandler);
            var detachShrink = _this.renderer.listen(_this.shrink.nativeElement, 'scroll', scrollHandler);
            _this.detachScrollHandlers = function () {
                detachExpand();
                detachShrink();
            };
        });
    };
    ResizeSensorComponent.prototype.ngAfterViewChecked = function () {
        var _this = this;
        if (typeof document === 'undefined') {
            return;
        }
        if (this.initialized) {
            this.scroll();
            return;
        }
        var throttleTime = 1000 / (this.rateLimit || DEFAULT_RATE_LIMIT);
        this.subscription = this.source.asObservable()
            .pipe(Object(rxjs_operators_auditTime__WEBPACK_IMPORTED_MODULE_2__["auditTime"])(throttleTime))
            .subscribe(function () {
            if (!_this.acceptedSize) {
                _this.resize.emit();
            }
        });
        this.parentElement = this.element.nativeElement.parentElement;
        if (computedProp(this.parentElement, 'position') === 'static') {
            this.parentElement.style.position = 'relative';
        }
        this.reset();
        this.lastWidth = this.parentElement.offsetWidth;
        this.lastHeight = this.parentElement.offsetHeight;
        this.initialized = true;
    };
    ResizeSensorComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.detachScrollHandlers) {
            this.detachScrollHandlers();
        }
    };
    /**
     * Sets the passed size or the element size as current.
     */
    ResizeSensorComponent.prototype.acceptSize = function (size) {
        if (size === void 0) { size = this.measure(); }
        this.lastWidth = size.width;
        this.lastHeight = size.height;
        this.acceptedSize = true;
    };
    /**
     * @hidden
     */
    ResizeSensorComponent.prototype.scroll = function (_event) {
        var _this = this;
        if (!this.parentElement) {
            return;
        }
        var _a = this.measure(), width = _a.width, height = _a.height;
        var sameSize = width === this.lastWidth && height === this.lastHeight;
        if (sameSize) {
            return;
        }
        this.lastWidth = width;
        this.lastHeight = height;
        this.acceptedSize = false;
        this.zone.runOutsideAngular(function () {
            _this.source.next();
        });
        this.reset();
    };
    ResizeSensorComponent.prototype.reset = function () {
        var expandChild = this.expandChild.nativeElement;
        expandChild.style.width = 100000 + 'px';
        expandChild.style.height = 100000 + 'px';
        var expand = this.expand.nativeElement;
        expand.scrollLeft = 100000;
        expand.scrollTop = 100000;
        var shrink = this.shrink.nativeElement;
        shrink.scrollLeft = 100000;
        shrink.scrollTop = 100000;
    };
    ResizeSensorComponent.prototype.measure = function () {
        return {
            height: this.parentElement.offsetHeight,
            width: this.parentElement.offsetWidth
        };
    };
    ResizeSensorComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'kendo-resize-sensor',
                    styles: [':host { ' + WRAP_STYLE + ' }'],
                    template: '<div #expand style="' + WRAP_STYLE + '">' +
                        '  <div #expandChild style="' + EXPAND_CHILD_STYLE + '"></div>' +
                        '</div>' +
                        '<div #shrink style="' + WRAP_STYLE + '">' +
                        '  <div style="' + SHRINK_CHILD_STYLE + '"></div>' +
                        '</div>'
                },] },
    ];
    /** @nocollapse */
    ResizeSensorComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
    ]; };
    ResizeSensorComponent.propDecorators = {
        'rateLimit': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        'resize': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
        'expand': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['expand',] },],
        'expandChild': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['expandChild',] },],
        'shrink': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['shrink',] },],
    };
    return ResizeSensorComponent;
}());



/***/ }),

/***/ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.module.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.module.js ***!
  \********************************************************************************************/
/*! exports provided: ResizeSensorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResizeSensorModule", function() { return ResizeSensorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _resize_sensor_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./resize-sensor.component */ "./node_modules/@progress/kendo-angular-resize-sensor/dist/es/resize-sensor.component.js");


var COMPONENT_DIRECTIVES = [_resize_sensor_component__WEBPACK_IMPORTED_MODULE_1__["ResizeSensorComponent"]];
/**
 * Resize Sensor module
 */
var ResizeSensorModule = /** @class */ (function () {
    function ResizeSensorModule() {
    }
    ResizeSensorModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [COMPONENT_DIRECTIVES],
                    exports: [COMPONENT_DIRECTIVES]
                },] },
    ];
    /** @nocollapse */
    ResizeSensorModule.ctorParameters = function () { return []; };
    return ResizeSensorModule;
}());



/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/Subscription.js":
/*!********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/Subscription.js ***!
  \********************************************************/
/*! exports provided: Subscription */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subscription", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["Subscription"]; });


//# sourceMappingURL=Subscription.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/observable/interval.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/observable/interval.js ***!
  \***************************************************************/
/*! exports provided: interval */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "interval", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["interval"]; });


//# sourceMappingURL=interval.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/catchError.js":
/*!****************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/catchError.js ***!
  \****************************************************************/
/*! exports provided: catchError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "catchError", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"]; });


//# sourceMappingURL=catchError.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/concatMap.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/concatMap.js ***!
  \***************************************************************/
/*! exports provided: concatMap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "concatMap", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["concatMap"]; });


//# sourceMappingURL=concatMap.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/distinctUntilChanged.js":
/*!**************************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/distinctUntilChanged.js ***!
  \**************************************************************************/
/*! exports provided: distinctUntilChanged */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "distinctUntilChanged", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["distinctUntilChanged"]; });


//# sourceMappingURL=distinctUntilChanged.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/merge.js":
/*!***********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/merge.js ***!
  \***********************************************************/
/*! exports provided: merge */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "merge", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["merge"]; });


//# sourceMappingURL=merge.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/partition.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/partition.js ***!
  \***************************************************************/
/*! exports provided: partition */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "partition", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["partition"]; });


//# sourceMappingURL=partition.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/skipWhile.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/skipWhile.js ***!
  \***************************************************************/
/*! exports provided: skipWhile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "skipWhile", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["skipWhile"]; });


//# sourceMappingURL=skipWhile.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/take.js":
/*!**********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/take.js ***!
  \**********************************************************/
/*! exports provided: take */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "take", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["take"]; });


//# sourceMappingURL=take.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/takeUntil.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/takeUntil.js ***!
  \***************************************************************/
/*! exports provided: takeUntil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeUntil", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["takeUntil"]; });


//# sourceMappingURL=takeUntil.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/throttleTime.js":
/*!******************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/throttleTime.js ***!
  \******************************************************************/
/*! exports provided: throttleTime */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "throttleTime", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["throttleTime"]; });


//# sourceMappingURL=throttleTime.js.map

/***/ })

}]);
//# sourceMappingURL=src-app-get-all-games-get-all-games-module~src-app-start-game-start-game-module.js.map