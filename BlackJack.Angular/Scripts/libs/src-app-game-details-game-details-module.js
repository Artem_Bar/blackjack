(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-game-details-game-details-module"],{

/***/ "./src/app/game-details/game-details-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/game-details/game-details-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: GameDetailsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameDetailsRoutingModule", function() { return GameDetailsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_game_details_game_details_game_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/game-details/game-details/game-details.component */ "./src/app/game-details/game-details/game-details.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: src_app_game_details_game_details_game_details_component__WEBPACK_IMPORTED_MODULE_2__["GameDetailsComponent"]
    }
];
var GameDetailsRoutingModule = /** @class */ (function () {
    function GameDetailsRoutingModule() {
    }
    GameDetailsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], GameDetailsRoutingModule);
    return GameDetailsRoutingModule;
}());



/***/ }),

/***/ "./src/app/game-details/game-details.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/game-details/game-details.module.ts ***!
  \*****************************************************/
/*! exports provided: GameDetailsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameDetailsModule", function() { return GameDetailsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_game_details_game_details_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/game-details/game-details-routing.module */ "./src/app/game-details/game-details-routing.module.ts");
/* harmony import */ var src_app_game_details_game_details_game_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/game-details/game-details/game-details.component */ "./src/app/game-details/game-details/game-details.component.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GameDetailsModule = /** @class */ (function () {
    function GameDetailsModule() {
    }
    GameDetailsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                src_app_game_details_game_details_routing_module__WEBPACK_IMPORTED_MODULE_1__["GameDetailsRoutingModule"],
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [src_app_game_details_game_details_game_details_component__WEBPACK_IMPORTED_MODULE_2__["GameDetailsComponent"]]
        })
    ], GameDetailsModule);
    return GameDetailsModule;
}());



/***/ }),

/***/ "./src/app/game-details/game-details/game-details.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/game-details/game-details/game-details.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent\">\r\n    <table id=\"main\"  class=\"table table-bordered table-hover\" border=\"1\">\r\n        <caption>Details Game #{{details.gameId}}</caption>\r\n        <tr>\r\n            <th>Name</th>\r\n            <th>Role</th>\r\n            <th>State</th>\r\n            <th>Cards</th>\r\n            <th>Points</th>\r\n            <th>Total points</th>\r\n        </tr>\r\n        <tr *ngFor=\"let player of details.players\">\r\n            <td>{{player.name}}</td>\r\n            <td *ngIf=\"player.role!=2\"> {{conversion.getPlayerRole(player.role)}}</td>\r\n            <td *ngIf=\"player.role!=2\"> {{conversion.getPlayerState(player.state)}}</td>\r\n            <td *ngIf=\"player.role==2\" colspan=\"2\">{{conversion.getPlayerRole(player.role)}}</td>\r\n\r\n            <td>\r\n                <table align=\"center\">\r\n                    <tr *ngFor=\"let card of player.cards\">\r\n                        <td>{{conversion.getCardValue(card.value)}}  {{conversion.getCardSuit(card.suit)}}</td>\r\n                    </tr>\r\n                </table>\r\n            </td>\r\n            <td>\r\n                <table align=\"center\">\r\n                    <tr *ngFor=\"let card of player.cards\">\r\n                        <td>{{card.points}}</td>\r\n                    </tr>\r\n                </table>\r\n            </td>\r\n            <td>{{player.totalPoints}}</td>\r\n        </tr>\r\n    </table>\r\n    <div class=\"row\">\r\n        <br />\r\n        <br />\r\n        <div class=\"col-md-4 col-md-push-5\">\r\n            <button id=\"kendoButtonBack\" kendoButton (click)=\"onClickToBack()\">Back</button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/game-details/game-details/game-details.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/game-details/game-details/game-details.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".parent {\n  margin-top: 100px; }\n\n#main {\n  border: 1px;\n  border-width: 1px;\n  width: 1050px;\n  height: 300px;\n  margin: auto;\n  text-align: center;\n  align-content: center; }\n\ntr {\n  font-size: medium; }\n\ncaption {\n  text-align: center;\n  font-size: xx-large; }\n\nth {\n  text-align: center;\n  font: large; }\n\nbutton {\n  font-size: x-large;\n  width: 200px;\n  height: 40px; }\n"

/***/ }),

/***/ "./src/app/game-details/game-details/game-details.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/game-details/game-details/game-details.component.ts ***!
  \*********************************************************************/
/*! exports provided: GameDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameDetailsComponent", function() { return GameDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_shared_services_history_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/history.service */ "./src/app/shared/services/history.service.ts");
/* harmony import */ var src_app_shared_models_history_game_details_history_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/models/history/game-details-history.view */ "./src/app/shared/models/history/game-details-history.view.ts");
/* harmony import */ var src_app_shared_helpers_conversions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/helpers/conversions */ "./src/app/shared/helpers/conversions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GameDetailsComponent = /** @class */ (function () {
    function GameDetailsComponent(historyService, activateRoute, router) {
        this.historyService = historyService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.conversion = new src_app_shared_helpers_conversions__WEBPACK_IMPORTED_MODULE_4__["ConversionHelper"]();
        this.details = new src_app_shared_models_history_game_details_history_view__WEBPACK_IMPORTED_MODULE_3__["GameDetailsHistoryView"]();
    }
    GameDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.historyService.GameDetails(this.gameId).subscribe(function (data) {
            _this.details = data;
        });
    };
    GameDetailsComponent.prototype.onClickToBack = function () {
        this.router.navigate(["/getAllGames"]);
    };
    GameDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-game-details',
            template: __webpack_require__(/*! ./game-details.component.html */ "./src/app/game-details/game-details/game-details.component.html"),
            styles: [__webpack_require__(/*! ./game-details.component.scss */ "./src/app/game-details/game-details/game-details.component.scss")],
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [src_app_shared_services_history_service__WEBPACK_IMPORTED_MODULE_2__["HistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GameDetailsComponent);
    return GameDetailsComponent;
}());



/***/ }),

/***/ "./src/app/shared/models/history/game-details-history.view.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/models/history/game-details-history.view.ts ***!
  \********************************************************************/
/*! exports provided: GameDetailsHistoryView, PlayerGameDetailsHistoryViewItem, CardGameDetailsHistoryViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameDetailsHistoryView", function() { return GameDetailsHistoryView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerGameDetailsHistoryViewItem", function() { return PlayerGameDetailsHistoryViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardGameDetailsHistoryViewItem", function() { return CardGameDetailsHistoryViewItem; });
var GameDetailsHistoryView = /** @class */ (function () {
    function GameDetailsHistoryView() {
    }
    return GameDetailsHistoryView;
}());

var PlayerGameDetailsHistoryViewItem = /** @class */ (function () {
    function PlayerGameDetailsHistoryViewItem() {
    }
    return PlayerGameDetailsHistoryViewItem;
}());

var CardGameDetailsHistoryViewItem = /** @class */ (function () {
    function CardGameDetailsHistoryViewItem() {
    }
    return CardGameDetailsHistoryViewItem;
}());



/***/ })

}]);
//# sourceMappingURL=src-app-game-details-game-details-module.js.map