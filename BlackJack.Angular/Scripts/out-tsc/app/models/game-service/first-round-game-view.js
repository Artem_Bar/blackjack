"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FirstRoundGameView = /** @class */ (function () {
    function FirstRoundGameView() {
    }
    return FirstRoundGameView;
}());
exports.FirstRoundGameView = FirstRoundGameView;
var PlayerFirstRoundGameViewItem = /** @class */ (function () {
    function PlayerFirstRoundGameViewItem() {
    }
    return PlayerFirstRoundGameViewItem;
}());
exports.PlayerFirstRoundGameViewItem = PlayerFirstRoundGameViewItem;
var CardFirstRoundGameViewItem = /** @class */ (function () {
    function CardFirstRoundGameViewItem() {
    }
    return CardFirstRoundGameViewItem;
}());
exports.CardFirstRoundGameViewItem = CardFirstRoundGameViewItem;
//# sourceMappingURL=first-round-game-view.js.map