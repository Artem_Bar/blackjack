"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NextRoundGameView = /** @class */ (function () {
    function NextRoundGameView() {
    }
    return NextRoundGameView;
}());
exports.NextRoundGameView = NextRoundGameView;
var PlayerNextRoundGameViewItem = /** @class */ (function () {
    function PlayerNextRoundGameViewItem() {
    }
    return PlayerNextRoundGameViewItem;
}());
exports.PlayerNextRoundGameViewItem = PlayerNextRoundGameViewItem;
var CardNextRoundGameViewItem = /** @class */ (function () {
    function CardNextRoundGameViewItem() {
    }
    return CardNextRoundGameViewItem;
}());
exports.CardNextRoundGameViewItem = CardNextRoundGameViewItem;
//# sourceMappingURL=next-round-game-view.js.map