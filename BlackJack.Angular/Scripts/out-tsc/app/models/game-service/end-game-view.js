"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EndGameView = /** @class */ (function () {
    function EndGameView() {
    }
    return EndGameView;
}());
exports.EndGameView = EndGameView;
var PlayerEndGameViewItem = /** @class */ (function () {
    function PlayerEndGameViewItem() {
    }
    return PlayerEndGameViewItem;
}());
exports.PlayerEndGameViewItem = PlayerEndGameViewItem;
var CardEndGameViewItem = /** @class */ (function () {
    function CardEndGameViewItem() {
    }
    return CardEndGameViewItem;
}());
exports.CardEndGameViewItem = CardEndGameViewItem;
//# sourceMappingURL=end-game-view.js.map