"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardSuitView;
(function (CardSuitView) {
    CardSuitView[CardSuitView["diamonds"] = 0] = "diamonds";
    CardSuitView[CardSuitView["hearts"] = 1] = "hearts";
    CardSuitView[CardSuitView["spades"] = 2] = "spades";
    CardSuitView[CardSuitView["clubs"] = 3] = "clubs";
})(CardSuitView = exports.CardSuitView || (exports.CardSuitView = {}));
//# sourceMappingURL=card-suit.view.js.map