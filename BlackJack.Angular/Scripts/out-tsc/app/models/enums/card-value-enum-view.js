"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardValueEnumView;
(function (CardValueEnumView) {
    CardValueEnumView[CardValueEnumView["two"] = 0] = "two";
    CardValueEnumView[CardValueEnumView["three"] = 1] = "three";
    CardValueEnumView[CardValueEnumView["four"] = 2] = "four";
    CardValueEnumView[CardValueEnumView["five"] = 3] = "five";
    CardValueEnumView[CardValueEnumView["six"] = 4] = "six";
    CardValueEnumView[CardValueEnumView["seven"] = 5] = "seven";
    CardValueEnumView[CardValueEnumView["eight"] = 6] = "eight";
    CardValueEnumView[CardValueEnumView["nine"] = 7] = "nine";
    CardValueEnumView[CardValueEnumView["ten"] = 8] = "ten";
    CardValueEnumView[CardValueEnumView["jack"] = 9] = "jack";
    CardValueEnumView[CardValueEnumView["queen"] = 10] = "queen";
    CardValueEnumView[CardValueEnumView["king"] = 11] = "king";
    CardValueEnumView[CardValueEnumView["ace"] = 12] = "ace";
})(CardValueEnumView = exports.CardValueEnumView || (exports.CardValueEnumView = {}));
//# sourceMappingURL=card-value-enum-view.js.map