"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerRoleView;
(function (PlayerRoleView) {
    PlayerRoleView[PlayerRoleView["bot"] = 0] = "bot";
    PlayerRoleView[PlayerRoleView["player"] = 1] = "player";
    PlayerRoleView[PlayerRoleView["dealer"] = 2] = "dealer";
})(PlayerRoleView = exports.PlayerRoleView || (exports.PlayerRoleView = {}));
//# sourceMappingURL=player-role.view.js.map