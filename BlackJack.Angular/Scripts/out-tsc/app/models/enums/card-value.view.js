"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardValueView;
(function (CardValueView) {
    CardValueView[CardValueView["two"] = 0] = "two";
    CardValueView[CardValueView["three"] = 1] = "three";
    CardValueView[CardValueView["four"] = 2] = "four";
    CardValueView[CardValueView["five"] = 3] = "five";
    CardValueView[CardValueView["six"] = 4] = "six";
    CardValueView[CardValueView["seven"] = 5] = "seven";
    CardValueView[CardValueView["eight"] = 6] = "eight";
    CardValueView[CardValueView["nine"] = 7] = "nine";
    CardValueView[CardValueView["ten"] = 8] = "ten";
    CardValueView[CardValueView["jack"] = 9] = "jack";
    CardValueView[CardValueView["queen"] = 10] = "queen";
    CardValueView[CardValueView["king"] = 11] = "king";
    CardValueView[CardValueView["ace"] = 12] = "ace";
})(CardValueView = exports.CardValueView || (exports.CardValueView = {}));
//# sourceMappingURL=card-value.view.js.map