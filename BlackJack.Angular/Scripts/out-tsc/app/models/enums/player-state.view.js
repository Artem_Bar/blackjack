"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerStateView;
(function (PlayerStateView) {
    PlayerStateView[PlayerStateView["inGame"] = 0] = "inGame";
    PlayerStateView[PlayerStateView["win"] = 1] = "win";
    PlayerStateView[PlayerStateView["loss"] = 2] = "loss";
    PlayerStateView[PlayerStateView["draw"] = 3] = "draw";
    PlayerStateView[PlayerStateView["blackJack"] = 4] = "blackJack";
})(PlayerStateView = exports.PlayerStateView || (exports.PlayerStateView = {}));
//# sourceMappingURL=player-state.view.js.map