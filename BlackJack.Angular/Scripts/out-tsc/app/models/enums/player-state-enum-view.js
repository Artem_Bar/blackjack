"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerStateEnumView;
(function (PlayerStateEnumView) {
    PlayerStateEnumView[PlayerStateEnumView["inGame"] = 0] = "inGame";
    PlayerStateEnumView[PlayerStateEnumView["win"] = 1] = "win";
    PlayerStateEnumView[PlayerStateEnumView["loss"] = 2] = "loss";
    PlayerStateEnumView[PlayerStateEnumView["draw"] = 3] = "draw";
    PlayerStateEnumView[PlayerStateEnumView["blackJack"] = 4] = "blackJack";
})(PlayerStateEnumView = exports.PlayerStateEnumView || (exports.PlayerStateEnumView = {}));
//# sourceMappingURL=player-state-enum-view.js.map