"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameStateEnumView;
(function (GameStateEnumView) {
    GameStateEnumView[GameStateEnumView["continue"] = 0] = "continue";
    GameStateEnumView[GameStateEnumView["end"] = 1] = "end";
})(GameStateEnumView = exports.GameStateEnumView || (exports.GameStateEnumView = {}));
//# sourceMappingURL=game-state-enum-view.js.map