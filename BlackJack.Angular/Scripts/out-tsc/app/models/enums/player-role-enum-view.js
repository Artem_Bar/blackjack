"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerRoleEnumView;
(function (PlayerRoleEnumView) {
    PlayerRoleEnumView[PlayerRoleEnumView["bot"] = 0] = "bot";
    PlayerRoleEnumView[PlayerRoleEnumView["player"] = 1] = "player";
    PlayerRoleEnumView[PlayerRoleEnumView["dealer"] = 2] = "dealer";
})(PlayerRoleEnumView = exports.PlayerRoleEnumView || (exports.PlayerRoleEnumView = {}));
//# sourceMappingURL=player-role-enum-view.js.map