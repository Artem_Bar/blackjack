"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameStateView;
(function (GameStateView) {
    GameStateView[GameStateView["continue"] = 0] = "continue";
    GameStateView[GameStateView["end"] = 1] = "end";
})(GameStateView = exports.GameStateView || (exports.GameStateView = {}));
//# sourceMappingURL=game-state.view.js.map