"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var history_service_1 = require("src/app/shared/services/history.service");
var get_all_games_history_view_1 = require("src/app/shared/models/history/get-all-games-history.view");
var GetAllGamesComponent = /** @class */ (function () {
    function GetAllGamesComponent(historyService, router) {
        this.historyService = historyService;
        this.router = router;
        this.data = new get_all_games_history_view_1.GetAllGamesHistoryView();
    }
    GetAllGamesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.historyService.GetAllGames().subscribe(function (games) {
            _this.data = games;
        });
    };
    GetAllGamesComponent.prototype.onClick = function (id) {
        this.router.navigate(['/gameDetails', id]);
    };
    GetAllGamesComponent = __decorate([
        core_1.Component({
            selector: 'app-all-games',
            templateUrl: './get-all-games.component.html',
            styleUrls: ['./get-all-games.component.scss'],
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [history_service_1.HistoryService, router_1.Router])
    ], GetAllGamesComponent);
    return GetAllGamesComponent;
}());
exports.GetAllGamesComponent = GetAllGamesComponent;
//# sourceMappingURL=get-all-games.component.js.map