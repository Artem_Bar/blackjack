"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var get_all_games_routing_module_1 = require("./get-all-games-routing.module");
var get_all_games_component_1 = require("src/app/components/history/get-all-games/get-all-games.component");
var shared_module_1 = require("src/app/shared/modules/shared.module");
var GetAllGamesModule = /** @class */ (function () {
    function GetAllGamesModule() {
    }
    GetAllGamesModule = __decorate([
        core_1.NgModule({
            imports: [
                get_all_games_routing_module_1.GetAllGamesRoutingModule,
                kendo_angular_grid_1.GridModule,
                shared_module_1.SharedModule
            ],
            declarations: [get_all_games_component_1.GetAllGamesComponent]
        })
    ], GetAllGamesModule);
    return GetAllGamesModule;
}());
exports.GetAllGamesModule = GetAllGamesModule;
//# sourceMappingURL=get-all-games.module.js.map