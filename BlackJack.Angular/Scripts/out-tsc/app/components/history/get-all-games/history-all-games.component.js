"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var history_service_1 = require("src/app/services/history.service");
var kendo_angular_intl_1 = require("@progress/kendo-angular-intl");
var HistoryAllGamesComponent = /** @class */ (function () {
    function HistoryAllGamesComponent(historyService, router, route, intl) {
        this.historyService = historyService;
        this.router = router;
        this.route = route;
        this.intl = intl;
    }
    HistoryAllGamesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.historyService.GetAllGames().subscribe(function (games) {
            _this.data = games;
        });
    };
    HistoryAllGamesComponent.prototype.onClick = function (id) {
        this.router.navigate(['/gameDetails', id]);
    };
    HistoryAllGamesComponent = __decorate([
        core_1.Component({
            selector: 'app-all-games',
            templateUrl: './history-all-games.component.html',
            styleUrls: ['./history-all-games.component.scss'],
            providers: [history_service_1.HistoryService]
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [history_service_1.HistoryService, router_1.Router, router_1.ActivatedRoute, kendo_angular_intl_1.IntlService])
    ], HistoryAllGamesComponent);
    return HistoryAllGamesComponent;
}());
exports.HistoryAllGamesComponent = HistoryAllGamesComponent;
//# sourceMappingURL=history-all-games.component.js.map