"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_details_routing_module_1 = require("./game-details-routing.module");
var game_details_component_1 = require("../game-details.component");
var shared_module_1 = require("src/app/shared/modules/shared.module");
var GameDetailsModule = /** @class */ (function () {
    function GameDetailsModule() {
    }
    GameDetailsModule = __decorate([
        core_1.NgModule({
            imports: [
                game_details_routing_module_1.GameDetailsRoutingModule,
                shared_module_1.SharedModule
            ],
            declarations: [game_details_component_1.GameDetailsComponent]
        })
    ], GameDetailsModule);
    return GameDetailsModule;
}());
exports.GameDetailsModule = GameDetailsModule;
//# sourceMappingURL=game-details.module.js.map