"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var history_service_1 = require("src/app/services/history.service");
var card_suit_enum_view_1 = require("src/app/models/enums/card-suit-enum.view");
var card_value_enum_view_1 = require("src/app/models/enums/card-value-enum.view");
var player_role_enum_view_1 = require("src/app/models/enums/player-role-enum.view");
var player_state_enum_view_1 = require("src/app/models/enums/player-state-enum.view");
var HistoryDetailsComponent = /** @class */ (function () {
    function HistoryDetailsComponent(historyService, activateRoute, router) {
        this.historyService = historyService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.gameId = this.activateRoute.snapshot.params['id'];
    }
    HistoryDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.historyService.GameDetails(this.gameId).subscribe(function (data) {
            _this.details = data;
        });
    };
    HistoryDetailsComponent.prototype.getPlayerRole = function (item) {
        return player_role_enum_view_1.PlayerRoleEnumView[item];
    };
    HistoryDetailsComponent.prototype.getPlayerState = function (item) {
        return player_state_enum_view_1.PlayerStateEnumView[item];
    };
    HistoryDetailsComponent.prototype.getCardValue = function (item) {
        return card_value_enum_view_1.CardValueEnumView[item];
    };
    HistoryDetailsComponent.prototype.getCardSuit = function (item) {
        return card_suit_enum_view_1.CardSuitEnumView[item];
    };
    HistoryDetailsComponent.prototype.onClick = function () {
        this.router.navigate(['/getAllGames']);
    };
    HistoryDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-game-details',
            templateUrl: './history-details.component.html',
            styleUrls: ['./history-details.component.scss'],
            providers: [history_service_1.HistoryService]
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [history_service_1.HistoryService, router_1.ActivatedRoute, router_1.Router])
    ], HistoryDetailsComponent);
    return HistoryDetailsComponent;
}());
exports.HistoryDetailsComponent = HistoryDetailsComponent;
//# sourceMappingURL=history-details.component.js.map