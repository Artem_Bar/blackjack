"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var game_service_1 = require("src/app/services/game.service");
var FirstRoundComponent = /** @class */ (function () {
    function FirstRoundComponent(gameService, activateRoute, route) {
        this.gameService = gameService;
        this.activateRoute = activateRoute;
        this.route = route;
    }
    ;
    FirstRoundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.gameService.firstRound(this.gameId).subscribe(function (round) { return _this.firstround = round; });
        console.log(this.firstround.players.length);
    };
    FirstRoundComponent = __decorate([
        core_1.Component({
            selector: 'first-round-app',
            templateUrl: './first-round.component.html',
            styleUrls: ['./first-round.component.css'],
            providers: [game_service_1.GameService]
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [game_service_1.GameService, router_1.ActivatedRoute, router_1.Router])
    ], FirstRoundComponent);
    return FirstRoundComponent;
}());
exports.FirstRoundComponent = FirstRoundComponent;
//# sourceMappingURL=first-round.component.js.map