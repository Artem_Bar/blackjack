"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var game_service_1 = require("src/app/shared/services/game.service");
var start_game_game_view_1 = require("src/app/shared/models/game/start-game-game-view");
var request_create_game_game_view_1 = require("src/app/shared/models/game/request-create-game-game.view");
var StartGameComponent = /** @class */ (function () {
    function StartGameComponent(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.quantityBots = [1, 2, 3, 4, 5];
        this.players = new start_game_game_view_1.StartGameGameView();
        this.data = new request_create_game_game_view_1.RequestCreateGameGameView();
    }
    StartGameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.gameService.startGame().subscribe(function (p) {
            _this.players.names = p.names;
        });
    };
    StartGameComponent.prototype.onClick = function () {
        var _this = this;
        this.gameService.createGame(this.data).subscribe(function (result) {
            _this.router.navigate(["/currentGame", result.gameId]);
        });
    };
    StartGameComponent = __decorate([
        core_1.Component({
            selector: 'app-start',
            templateUrl: './start-game.component.html',
            styleUrls: ['./start-game.component.scss'],
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [game_service_1.GameService, router_1.Router])
    ], StartGameComponent);
    return StartGameComponent;
}());
exports.StartGameComponent = StartGameComponent;
//# sourceMappingURL=start-game.component.js.map