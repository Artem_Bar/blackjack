"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var game_service_1 = require("src/app/shared/services/game.service");
var current_game_game_view_1 = require("src/app/shared/models/game/current-game-game.view");
var get_action_in_game_1 = require("src/app/shared/models/enums/get-action-in-game");
var conversions_1 = require("src/app/shared/helpers/conversions");
var CurrentGameComponent = /** @class */ (function () {
    function CurrentGameComponent(gameService, activateRoute, router) {
        this.gameService = gameService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.conversion = new conversions_1.ConversionHelper();
        this.currentGame = new current_game_game_view_1.CurrentGameGameView();
    }
    ;
    CurrentGameComponent.prototype.ngOnInit = function () {
        this.getRound(get_action_in_game_1.GetActionInGame.firstRound);
    };
    CurrentGameComponent.prototype.getRound = function (action) {
        var _this = this;
        this.gameService.currentGame(this.gameId, action).subscribe(function (round) {
            _this.currentGame = round;
            if (_this.currentGame.isDealerTakes) {
                _this.isDealerTake = true;
                _this.getGameEnd();
            }
        });
    };
    CurrentGameComponent.prototype.getGameEnd = function () {
        var _this = this;
        this.isDealerTake = true;
        this.gameService.currentGame(this.gameId, get_action_in_game_1.GetActionInGame.dealerTakes).subscribe(function (round) { return _this.currentGame = round; });
    };
    CurrentGameComponent.prototype.onClickToNewGame = function () {
        this.router.navigate([""]);
    };
    CurrentGameComponent.prototype.onClickToHistory = function () {
        this.router.navigate(["/getAllGames"]);
    };
    CurrentGameComponent = __decorate([
        core_1.Component({
            selector: 'app-current-game',
            templateUrl: './current-game.component.html',
            styleUrls: ['./current-game.component.scss'],
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [game_service_1.GameService, router_1.ActivatedRoute, router_1.Router])
    ], CurrentGameComponent);
    return CurrentGameComponent;
}());
exports.CurrentGameComponent = CurrentGameComponent;
//# sourceMappingURL=current-game.component.js.map