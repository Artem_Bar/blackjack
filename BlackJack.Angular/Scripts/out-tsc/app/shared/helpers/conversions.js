"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var get_action_in_game_1 = require("src/app/shared/models/enums/get-action-in-game");
var card_suit_enum_view_1 = require("src/app/shared/models/enums/card-suit-enum.view");
var card_value_enum_view_1 = require("src/app/shared/models/enums/card-value-enum.view");
var player_role_enum_view_1 = require("src/app/shared/models/enums/player-role-enum.view");
var player_state_enum_view_1 = require("src/app/shared/models/enums/player-state-enum.view");
var ConversionHelper = /** @class */ (function () {
    function ConversionHelper() {
    }
    ConversionHelper.prototype.getPlayerRole = function (item) {
        return player_role_enum_view_1.PlayerRoleEnumView[item];
    };
    ConversionHelper.prototype.getPlayerState = function (item) {
        return player_state_enum_view_1.PlayerStateEnumView[item];
    };
    ConversionHelper.prototype.getCardValue = function (item) {
        return card_value_enum_view_1.CardValueEnumView[item];
    };
    ConversionHelper.prototype.getCardSuit = function (item) {
        return card_suit_enum_view_1.CardSuitEnumView[item];
    };
    ConversionHelper.prototype.getActionInGame = function (item) {
        return get_action_in_game_1.GetActionInGame[item];
    };
    return ConversionHelper;
}());
exports.ConversionHelper = ConversionHelper;
//# sourceMappingURL=conversions.js.map