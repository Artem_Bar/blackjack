"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetActionInGame;
(function (GetActionInGame) {
    GetActionInGame[GetActionInGame["firstRound"] = 0] = "firstRound";
    GetActionInGame[GetActionInGame["nextRound"] = 1] = "nextRound";
    GetActionInGame[GetActionInGame["dealerTakes"] = 2] = "dealerTakes";
})(GetActionInGame = exports.GetActionInGame || (exports.GetActionInGame = {}));
//# sourceMappingURL=get-action-in-game.js.map