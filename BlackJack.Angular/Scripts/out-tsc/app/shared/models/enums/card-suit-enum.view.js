"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardSuitEnumView;
(function (CardSuitEnumView) {
    CardSuitEnumView[CardSuitEnumView["diamonds"] = 0] = "diamonds";
    CardSuitEnumView[CardSuitEnumView["hearts"] = 1] = "hearts";
    CardSuitEnumView[CardSuitEnumView["spades"] = 2] = "spades";
    CardSuitEnumView[CardSuitEnumView["clubs"] = 3] = "clubs";
})(CardSuitEnumView = exports.CardSuitEnumView || (exports.CardSuitEnumView = {}));
//# sourceMappingURL=card-suit-enum.view.js.map