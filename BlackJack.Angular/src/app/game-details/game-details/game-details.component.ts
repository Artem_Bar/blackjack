﻿import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoryService } from 'src/app/shared/services/history.service';
import { GetAllGamesHistoryView } from 'src/app/shared/models/history/get-all-games-history.view';
import { GameDetailsHistoryView } from 'src/app/shared/models/history/game-details-history.view';
import { ConversionHelper } from 'src/app/shared/helpers/conversions';

@Component({
    selector: 'app-game-details',
    templateUrl: './game-details.component.html',
    styleUrls: ['./game-details.component.scss'],
})

@Injectable()
export class GameDetailsComponent implements OnInit {
    gameId: number;
    details: GameDetailsHistoryView;
    conversion: ConversionHelper;
    constructor(private historyService: HistoryService, private activateRoute: ActivatedRoute, private router: Router) {
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.conversion = new ConversionHelper();
        this.details = new GameDetailsHistoryView();
    }

    ngOnInit() {
        this.historyService.GameDetails(this.gameId).subscribe(data => {
            this.details = data;
        });
    }
    onClickToBack() {
        this.router.navigate(["/getAllGames"])
    }
}
