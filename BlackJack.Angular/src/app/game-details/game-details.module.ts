﻿import { NgModule } from '@angular/core';
import { GameDetailsRoutingModule } from 'src/app/game-details/game-details-routing.module';
import { GameDetailsComponent } from 'src/app/game-details/game-details/game-details.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        GameDetailsRoutingModule,
        SharedModule
    ],
    declarations: [GameDetailsComponent]
})
export class GameDetailsModule { }
