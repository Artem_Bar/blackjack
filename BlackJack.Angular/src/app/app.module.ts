import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { GameService } from 'src/app/shared/services/game.service';
import { HistoryService } from 'src/app/shared/services/history.service';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { AppComponent } from 'src/app/app.component';

@NgModule({
    declarations: [
        HeaderComponent,
        AppComponent
    ],

    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule
    ],

    providers: [
        GameService,
        HistoryService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
