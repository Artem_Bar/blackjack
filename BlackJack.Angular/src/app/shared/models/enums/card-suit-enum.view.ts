﻿export enum CardSuitEnumView {
    diamonds = 0,
    hearts = 1,
    spades = 2,
    clubs = 3
}
