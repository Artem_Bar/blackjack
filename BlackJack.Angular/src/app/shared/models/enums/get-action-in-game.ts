﻿export enum GetActionInGame {
    firstRound = 0,
    nextRound = 1,
    dealerTakes = 2
}
