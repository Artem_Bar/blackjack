﻿export enum PlayerStateEnumView {
    inGame = 0,
    win = 1,
    loss = 2,
    draw = 3,
    blackJack = 4
}
