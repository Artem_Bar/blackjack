﻿import { CardSuitEnumView } from 'src/app/shared/models/enums/card-suit-enum.view'
import { CardValueEnumView } from 'src/app/shared/models/enums/card-value-enum.view';
import { PlayerRoleEnumView } from 'src/app/shared/models/enums/player-role-enum.view';
import { PlayerStateEnumView } from 'src/app/shared/models/enums/player-state-enum.view';

export class CurrentGameGameView {
    public gameId: number;
    public isDealerTakes: boolean;
    public players: PlayerCurrentGameGameViewItem[];
}

export class PlayerCurrentGameGameViewItem {
    public id: number;
    public name: string;
    public role: PlayerRoleEnumView;
    public state: PlayerStateEnumView;
    public cards: CardCurrentGameGameViewItem[];
    public totalPoints: number;
}

export class CardCurrentGameGameViewItem {
    public value: CardValueEnumView;
    public suit: CardSuitEnumView;
    public points: number;
}
