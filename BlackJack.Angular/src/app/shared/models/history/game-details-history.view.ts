﻿import { CardSuitEnumView } from 'src/app/shared/models/enums/card-suit-enum.view'
import { CardValueEnumView } from 'src/app/shared/models/enums/card-value-enum.view';
import { PlayerRoleEnumView } from 'src/app/shared/models/enums/player-role-enum.view';
import { PlayerStateEnumView } from 'src/app/shared/models/enums/player-state-enum.view';

export class GameDetailsHistoryView {
    public gameId: number;
    public players: PlayerGameDetailsHistoryViewItem[];
}

export class PlayerGameDetailsHistoryViewItem {
    public name: string;
    public role: PlayerRoleEnumView;
    public state: PlayerStateEnumView;
    public cards: CardGameDetailsHistoryViewItem;
    public totalPoints: number;
}

export class CardGameDetailsHistoryViewItem {
    public value: CardValueEnumView;
    public suit: CardSuitEnumView;
    public points: number;
}
