﻿import { GameStateEnumView } from 'src/app/shared/models/enums/game-state-enum.view';

export class GetAllGamesHistoryView {
    public games: GameGetAllGamesHistoryViewItem[];
}

export class GameGetAllGamesHistoryViewItem {
    public gameId: number;
    public date: Date;
    public state: GameStateEnumView;
}
