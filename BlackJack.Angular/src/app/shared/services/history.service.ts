﻿import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetAllGamesHistoryView } from 'src/app/shared/models/history/get-all-games-history.view';
import { GameDetailsHistoryView } from 'src/app/shared/models/history/game-details-history.view';
import { environment } from 'src/environments/environment';

@Injectable()
export class HistoryService {

    constructor(private http: HttpClient) { }

    public GetAllGames(): Observable<GetAllGamesHistoryView> {
        return this.http.get<GetAllGamesHistoryView>(environment.historyUrl + "getAllGames");
    }

    public GameDetails(gameId: number): Observable<GameDetailsHistoryView> {
        return this.http.get<GameDetailsHistoryView>(environment.historyUrl + "gameDetails/" + gameId);
    }
}
