﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StartGameGameView } from 'src/app/shared/models/game/start-game-game-view';
import { RequestCreateGameGameView } from 'src/app/shared/models/game/request-create-game-game.view';
import { CurrentGameGameView } from 'src/app/shared/models/game/current-game-game.view';
import { ResponseCreateGameGameView } from 'src/app/shared/models/game/response-create-game-game.view';
import { GetActionInGame } from 'src/app/shared/models/enums/get-action-in-game';
import { environment } from 'src/environments/environment';

@Injectable()
export class GameService {

    constructor(private http: HttpClient) { }

    public startGame(): Observable<StartGameGameView> {
        return this.http.get<StartGameGameView>(environment.gameUrl + "startGame");
    }

    public createGame(data: RequestCreateGameGameView): Observable<ResponseCreateGameGameView> {
        const body = { amount: data.amount, name: data.name };
        return this.http.post<ResponseCreateGameGameView>(environment.gameUrl + "createGame", body);
    }

    public currentGame(gameId: number, param: GetActionInGame): Observable<CurrentGameGameView> {
        return this.http.get<CurrentGameGameView>(environment.gameUrl + "currentGame/" + gameId + "/" + param);
    }
}
