﻿import { GetActionInGame } from 'src/app/shared/models/enums/get-action-in-game';
import { CardSuitEnumView } from 'src/app/shared/models/enums/card-suit-enum.view'
import { CardValueEnumView } from 'src/app/shared/models/enums/card-value-enum.view';
import { PlayerRoleEnumView } from 'src/app/shared/models/enums/player-role-enum.view';
import { PlayerStateEnumView } from 'src/app/shared/models/enums/player-state-enum.view';

export class ConversionHelper {

    getPlayerRole(item: PlayerRoleEnumView): string {
        return PlayerRoleEnumView[item];
    }

    getPlayerState(item: PlayerStateEnumView): string {
        return PlayerStateEnumView[item];
    }

    getCardValue(item: CardValueEnumView): string {
        return CardValueEnumView[item];
    }

    getCardSuit(item: CardSuitEnumView): string {
        return CardSuitEnumView[item];
    }

    getActionInGame(item: GetActionInGame): string {
        return GetActionInGame[item];
    }
}