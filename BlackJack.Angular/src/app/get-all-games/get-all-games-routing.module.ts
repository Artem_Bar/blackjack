﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetAllGamesComponent } from 'src/app/get-all-games/get-all-games/get-all-games.component';

const routes: Routes = [
    {
        path: '',
        component: GetAllGamesComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GetAllGamesRoutingModule { }