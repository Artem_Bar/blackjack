﻿import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HistoryService } from 'src/app/shared/services/history.service';
import { GetAllGamesHistoryView } from 'src/app/shared/models/history/get-all-games-history.view';
import { GameDetailsHistoryView } from 'src/app/shared/models/history/game-details-history.view';

@Component({
    selector: 'app-all-games',
    templateUrl: './get-all-games.component.html',
    styleUrls: ['./get-all-games.component.scss'],
})

@Injectable()
export class GetAllGamesComponent implements OnInit {
    data: GetAllGamesHistoryView;

    constructor(private historyService: HistoryService, private router: Router) {
        this.data = new GetAllGamesHistoryView();
    }

    ngOnInit(): void {
        this.historyService.GetAllGames().subscribe(games => {
            this.data = games;
        })
    }

    onClick(id: number) {
        this.router.navigate(['/gameDetails', id]);
    }
}
