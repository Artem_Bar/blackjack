﻿import { NgModule } from '@angular/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { GetAllGamesRoutingModule } from 'src/app/get-all-games/get-all-games-routing.module';
import { GetAllGamesComponent } from 'src/app/get-all-games/get-all-games/get-all-games.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        GetAllGamesRoutingModule,
        GridModule,
        SharedModule
    ],
    declarations: [GetAllGamesComponent]
})
export class GetAllGamesModule { }
