﻿import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `<header-app></header-app>
              <router-outlet> </router-outlet>`
})

export class AppComponent { }