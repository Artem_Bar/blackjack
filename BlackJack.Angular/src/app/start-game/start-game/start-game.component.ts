﻿import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/shared/services/game.service';
import { StartGameGameView } from 'src/app/shared/models/game/start-game-game-view';
import { RequestCreateGameGameView} from 'src/app/shared/models/game/request-create-game-game.view';
import { GetActionInGame } from 'src/app/shared/models/enums/get-action-in-game';

@Component({
    selector: 'app-start',
    templateUrl: './start-game.component.html',
    styleUrls: ['./start-game.component.scss'],
})

@Injectable()
export class StartGameComponent implements OnInit {
    public data: RequestCreateGameGameView;
    public players: StartGameGameView;
    public quantityBots: number[] = [1, 2, 3, 4, 5];

    constructor(private gameService: GameService, private router: Router) {
        this.players = new StartGameGameView();
        this.data = new RequestCreateGameGameView();
    }

    ngOnInit() {
        this.gameService.startGame().subscribe(p => {
            this.players.names = p.names
        });
    }

    onClick() {
        this.gameService.createGame(this.data).subscribe(result => {
            this.router.navigate(["/currentGame", result.gameId]);
        });
    }
}
