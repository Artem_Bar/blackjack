﻿import { NgModule } from '@angular/core';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { StartGameRoutingModule } from 'src/app/start-game/start-game-routing.module';
import { StartGameComponent } from 'src/app/start-game/start-game/start-game.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        StartGameRoutingModule,
        DropDownsModule,
        SharedModule
    ],
    declarations: [StartGameComponent]
})
export class StartGameModule { }

