﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

    {
        path: '',
        redirectTo: '/startGame',
        pathMatch: 'full'
    },

    {
        path: 'startGame',
        loadChildren: 'src/app/start-game/start-game.module#StartGameModule'
    },

    {
        path: 'currentGame/:id',
        loadChildren: 'src/app/current-game/current-game.module#CurrentGameModule'
    },

    {
        path: 'getAllGames',
        loadChildren: 'src/app/get-all-games/get-all-games.module#GetAllGamesModule'
    },

    {
        path: 'gameDetails/:id',
        loadChildren: 'src/app/game-details/game-details.module#GameDetailsModule'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            { useHash: true })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }