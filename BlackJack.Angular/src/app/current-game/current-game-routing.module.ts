﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrentGameComponent } from 'src/app/current-game/current-game/current-game.component';

const routes: Routes = [
    {
        path: '',
        component: CurrentGameComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CurrentGameRoutingModule { }