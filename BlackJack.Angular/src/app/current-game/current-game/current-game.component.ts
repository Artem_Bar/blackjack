﻿import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from 'src/app/shared/services/game.service';
import { CurrentGameGameView } from 'src/app/shared/models/game/current-game-game.view';
import { GetActionInGame } from 'src/app/shared/models/enums/get-action-in-game';
import { ConversionHelper } from 'src/app/shared/helpers/conversions';

@Component({
    selector: 'app-current-game',
    templateUrl: './current-game.component.html',
    styleUrls: ['./current-game.component.scss'],
})

@Injectable()
export class CurrentGameComponent implements OnInit {

    currentGame: CurrentGameGameView;
    gameId: number;
    isDealerTake: boolean;
    conversion: ConversionHelper;

    constructor(private gameService: GameService, private activateRoute: ActivatedRoute, private router: Router) {
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.conversion = new ConversionHelper();
        this.currentGame = new CurrentGameGameView(); 
    };

    ngOnInit() {
        this.getRound(GetActionInGame.firstRound);
    }

    getRound(action: GetActionInGame) {
        this.gameService.currentGame(this.gameId, action).subscribe(round => {
            this.currentGame = round
            if (this.currentGame.isDealerTakes) {
                this.isDealerTake = true;
                this.getGameEnd();
            }
        });
    }

    getGameEnd() {
        this.isDealerTake = true;
        this.gameService.currentGame(this.gameId, GetActionInGame.dealerTakes).subscribe(round => this.currentGame = round);
    }

    onClickToNewGame() {
        this.router.navigate([""]);
    }

    onClickToHistory() {
        this.router.navigate(["/getAllGames"]);
    }
}
