﻿import { NgModule } from '@angular/core';
import { CurrentGameRoutingModule } from './current-game-routing.module';
import { CurrentGameComponent } from 'src/app/current-game/current-game/current-game.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        CurrentGameRoutingModule,
        SharedModule
    ],
    declarations: [CurrentGameComponent]
})
export class CurrentGameModule { }