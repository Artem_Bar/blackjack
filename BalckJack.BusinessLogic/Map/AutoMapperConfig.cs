﻿using AutoMapper;
using BlackJack.Entities.Entities;
using BlackJack.ViewModels.Views.GameViews;
using BlackJack.ViewModels.Views.HistoryViews;

namespace BlackJack.BusinessLogic.Map
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Game, GameGetAllGamesHistoryViewItem>()
                 .ForMember("GameId", opt => opt.MapFrom(g => g.Id))
                 .ForMember("Date", opt => opt.MapFrom(g => g.CreationDate));

                x.CreateMap<Player, PlayerGameDetailsHistoryViewItem>();
                x.CreateMap<Player, PlayerCurrentGameGameViewItem>();

                x.CreateMap<Card, CardGameDetailsHistoryViewItem>();
                x.CreateMap<Card, CardCurrentGameGameViewItem>();

            });
        }
    }
}
