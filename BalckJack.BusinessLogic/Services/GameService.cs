﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using System.Threading.Tasks;
using AutoMapper;
using BlackJack.ViewModels.Enums;
using BlackJack.ViewModels.Views.GameViews;
using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;

namespace BlackJack.BusinessLogic.Services
{
    public class GameService : IGameService
    {
        private const int MaxPoints = 21;
        private const int StopTakeDealer = 17;
        private const int MinPointsForAce = 1;
        private const int MaxPointsForAce = 11;
        private const int PointsCardsForPictures = 10;
        private const int MinPointsForCard = 2;

        private IGameRepository _gameRepository;
        private IRoundRepository _roundRepository;
        private IPlayerRepository _playerRepository;
        private ICardRepository _cardRepository;
        private IPlayerInGameRepository _playerInGameRepository;

        public GameService(IGameRepository gameRepository, IRoundRepository roundRepository, IPlayerRepository playerReposytory, ICardRepository cardsRepository, IPlayerInGameRepository playerInGameRepository)
        {
            _gameRepository = gameRepository;
            _roundRepository = roundRepository;
            _playerRepository = playerReposytory;
            _cardRepository = cardsRepository;
            _playerInGameRepository = playerInGameRepository;
        }

        public async Task<StartGameGameView> StartGame()
        {
            await CreateCards();
            await CreatePlayers();
            List<string> names = (await _playerRepository.GetPlayersByRole(PlayerRole.Player))
                  .Select(player => player.Name).ToList();

            var players = new StartGameGameView();
            players.Names = names;
            return players;
        }

        public async Task<ResponseCreateGameGameView> CreateGame(RequestCreateGameGameView data)
        {
            Player user = await _playerRepository.GetPlayerByName(data.Name);

            if (user == null)
            {
                user = await _playerRepository.Create(new Player { Name = data.Name, Role = PlayerRole.Player });
            }

            Player dealer = (await _playerRepository.GetPlayersByRole(PlayerRole.Dealer))
                 .Single();

            List<Player> bots = (await _playerRepository.GetPlayersByRole(PlayerRole.Bot))
                 .ToList().GetRange(0, data.Amount);

            var selectPlayers = new List<Player>();

            selectPlayers.Add(user);
            selectPlayers.AddRange(bots);
            selectPlayers.Add(dealer);

            Game newGame = await _gameRepository.Create(new Game { State = GameState.Continue });

            var playersInGame = new List<PlayerInGame>();
            foreach (var player in selectPlayers)
            {
                playersInGame.Add(new PlayerInGame { GameId = newGame.Id, PlayerId = player.Id, State = PlayerState.InGame });
            }
            await _playerInGameRepository.Create(playersInGame);
            return new ResponseCreateGameGameView { GameId = newGame.Id };
        }

        public async Task<CurrentGameGameView> CurrentGame(int gameId, GetActionInGame param)
        {
            if (param == GetActionInGame.FirstRound)
            {
                return await GetFirstRound(gameId);
            }
            if (param == GetActionInGame.NextRound)
            {
                return await GetNextRound(gameId);
            }
            return await GetEndGame(gameId);
        }


        private async Task CreateCards()
        {
            int countCards = await _cardRepository.Count();
            if (countCards != 0)
            {
                return;
            }
            var cards = new List<Card>();
            int points = 0;
            var suits = Enum.GetValues(typeof(CardSuit)) as CardSuit[];
            var values = Enum.GetValues(typeof(CardValue)) as CardValue[];
            foreach (var suit in suits)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    if (values[i] < CardValue.Ten)
                    {
                        points = MinPointsForCard + i;
                    }

                    if (values[i] >= CardValue.Ten && values[i] != CardValue.Ace)
                    {
                        points = PointsCardsForPictures;
                    }

                    if (values[i] == CardValue.Ace)
                    {
                        points = MaxPointsForAce;
                    }

                    Card card = new Card { Value = values[i], Suit = suit, Points = points };
                    cards.Add(card);
                }
            }
            await _cardRepository.Create(cards);
        }

        private async Task CreatePlayers()
        {
            int countPlayers = await _playerRepository.Count();
            if (countPlayers != 0)
            {
                return;
            }
            var players = new List<Player> {
                    new Player{Name="Bot_1",Role=PlayerRole.Bot},
                    new Player{Name="Bot_2",Role=PlayerRole.Bot},
                    new Player{Name="Bot_3",Role=PlayerRole.Bot},
                    new Player{Name="Bot_4",Role=PlayerRole.Bot},
                    new Player{Name="Bot_5",Role=PlayerRole.Bot},
                    new Player{Name="Dealer",Role=PlayerRole.Dealer}
                  };
            await _playerRepository.Create(players);
        }


        private async Task CreateOneRound(int gameId, int playerId)
        {
            Card card = await _cardRepository.GetAnyCard();

            PlayerInGame player = await _playerInGameRepository.GetPlayerById(gameId, playerId);

            player.TotalPoints += await CheckForAce(player, card);

            var round = new Round();
            round.GameId = gameId;
            round.PlayerId = playerId;
            round.CardId = card.Id;

            await _roundRepository.Create(round);
            await CheckingPoints(player);
        }

        private async Task CreateFullRound(int gameId, List<Player> players)
        {
            List<Round> rounds = new List<Round>();
            foreach (var player in players)
            {
                await CreateOneRound(gameId, player.Id);
            }
        }


        private async Task<bool> CheckingStateForNextRound(List<PlayerInGame> playersStates, GetActionInGame round)
        {
            PlayerState userState = playersStates
              .Where(p => p.Player.Role == PlayerRole.Player)
              .Select(p => p.State)
              .FirstOrDefault();

            PlayerState dealerState = playersStates
             .Where(p => p.Player.Role == PlayerRole.Dealer)
             .Select(p => p.State)
             .FirstOrDefault();

            if (round == GetActionInGame.FirstRound && userState != PlayerState.InGame || dealerState == PlayerState.BlackJack)
            {
                return true;
            }

            if (round == GetActionInGame.NextRound && userState != PlayerState.InGame)
            {
                return true;
            }

            return false;
        }

        private async Task<int> CheckForAce(PlayerInGame player, Card card)
        {
            int cardPoints = card.Points;

            if (card.Value == CardValue.Ace && player.TotalPoints + MaxPointsForAce > MaxPoints)
            {
                cardPoints = MinPointsForAce;
            }
            return cardPoints;
        }

        private async Task CheckingPoints(PlayerInGame player)
        {
            if (player.TotalPoints == MaxPoints)
            {
                player.State = PlayerState.BlackJack;
            }

            if (player.TotalPoints > MaxPoints)
            {
                player.State = PlayerState.Loss;
            }

            await _playerInGameRepository.Update(player);
        }

        private async Task ControlCheck(int gameId)
        {
            List<PlayerInGame> allPlayersOfTheGame = (await _playerInGameRepository.GetPlayersByGameId(gameId)).ToList();

            PlayerInGame dealer = allPlayersOfTheGame.Where(player => player.Player.Role == PlayerRole.Dealer).FirstOrDefault();

            List<PlayerInGame> playersInGame = allPlayersOfTheGame
             .Where(p => p.Player.Role != PlayerRole.Dealer && p.State == PlayerState.InGame)
             .ToList();

            if (dealer.TotalPoints > MaxPoints)
            {
                foreach (var playerInGame in playersInGame)
                {
                    playerInGame.State = PlayerState.Win;
                }
                await _playerInGameRepository.Update(playersInGame);
                return;
            }


            foreach (var playerInGame in playersInGame)
            {
                if (playerInGame.TotalPoints == dealer.TotalPoints)
                {
                    playerInGame.State = PlayerState.Draw;
                }

                if (playerInGame.TotalPoints > dealer.TotalPoints)
                {
                    playerInGame.State = PlayerState.Win;
                }

                if (playerInGame.TotalPoints < dealer.TotalPoints)
                {
                    playerInGame.State = PlayerState.Loss;
                }
            }
            await _playerInGameRepository.Update(playersInGame);
        }


        private async Task<CurrentGameGameView> GetFormedRound(int gameId, List<PlayerInGame> playersStates)
        {
            var newRound = new CurrentGameGameView();
            var players = new List<PlayerCurrentGameGameViewItem>();
            List<Player> playersInGame = (await _playerInGameRepository.GetPlayersByGameId(gameId)).Select(p => p.Player).ToList();
            List<Round> round = await _roundRepository.GetRoundsByGameId(gameId);
            foreach (var playerInGame in playersInGame)
            {
                List<Card> cards = round.Where(r => r.PlayerId == playerInGame.Id).Select(r => r.Card).ToList();
                PlayerCurrentGameGameViewItem player = Mapper.Map<Player, PlayerCurrentGameGameViewItem>(playerInGame);
                List<CardCurrentGameGameViewItem> cardsForPlayer = Mapper.Map<List<Card>, List<CardCurrentGameGameViewItem>>(cards);
                player.Cards = cardsForPlayer;
                player.State = (PlayerStateEnumView)playersStates.Where(p => p.Player.Id == playerInGame.Id).FirstOrDefault().State;
                player.TotalPoints = playersStates.Where(p => p.Player.Id == playerInGame.Id).First().TotalPoints;
                players.Add(player);
            }
            newRound.Players = players;
            newRound.GameId = gameId;
            return newRound;
        }

        private async Task<CurrentGameGameView> GetFirstRound(int gameId)
        {
            List<Player> players = (await _playerInGameRepository.GetPlayersByGameId(gameId)).Select(p => p.Player).ToList();

            await CreateFullRound(gameId, players);

            await CreateFullRound(gameId, players);

            List<PlayerInGame> playersStates = await _playerInGameRepository.GetPlayersByGameId(gameId);

            CurrentGameGameView newRound = await GetFormedRound(gameId, playersStates);

            newRound.IsDealerTakes = await CheckingStateForNextRound(playersStates, GetActionInGame.FirstRound);

            return newRound;
        }

        private async Task<CurrentGameGameView> GetNextRound(int gameId)
        {
            List<Player> playersInGame = await _playerInGameRepository.GetPlayerForNextRound(gameId, PlayerState.InGame);

            await CreateFullRound(gameId, playersInGame);

            List<PlayerInGame> playersStates = await _playerInGameRepository.GetPlayersByGameId(gameId);

            CurrentGameGameView newRound = await GetFormedRound(gameId, playersStates);

            newRound.IsDealerTakes = await CheckingStateForNextRound(playersStates, GetActionInGame.NextRound);

            return newRound;
        }

        private async Task<CurrentGameGameView> GetEndGame(int gameId)
        {
            List<PlayerInGame> allPlayers = (await _playerInGameRepository.GetPlayersByGameId(gameId)).ToList();

            List<PlayerInGame> players = allPlayers.Where(player => player.Player.Role != PlayerRole.Dealer).ToList();

            PlayerInGame dealer = allPlayers.Where(player => player.Player.Role == PlayerRole.Dealer).FirstOrDefault();

            if (players.Any(p => p.State == PlayerState.InGame))
            {
                int totalPoints = dealer.TotalPoints;
                while (totalPoints <= StopTakeDealer)
                {
                    await CreateOneRound(gameId, dealer.PlayerId);
                    totalPoints = await _playerInGameRepository.GetPlayerTotalPoints(gameId, PlayerRole.Dealer);
                }
            }

            await ControlCheck(gameId);

            List<PlayerInGame> playersStates = await _playerInGameRepository.GetPlayersByGameId(gameId);

            CurrentGameGameView newRound = await GetFormedRound(gameId, playersStates);

            Game currentGame = await _gameRepository.Get(gameId);
            currentGame.State = GameState.End;
            await _gameRepository.Update(currentGame);

            return newRound;
        }

    }
}

