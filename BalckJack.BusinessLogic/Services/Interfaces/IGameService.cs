﻿using BlackJack.BusinessLogic.Enums;
using BlackJack.ViewModels.Views.GameViews;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IGameService
    {
        Task<StartGameGameView> StartGame();

        Task<ResponseCreateGameGameView> CreateGame(RequestCreateGameGameView data);

        Task<CurrentGameGameView> CurrentGame(int gameId, GetActionInGame action);
    }
}