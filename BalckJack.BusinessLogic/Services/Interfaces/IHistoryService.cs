﻿using BlackJack.ViewModels.Views.HistoryViews;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IHistoryService
    {
        Task<GetAllGamesHistoryView> GetAllGames();

        Task<GameDetailsHistoryView> GameDetails(int gameId);
    }
}
