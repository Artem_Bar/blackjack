﻿using BlackJack.Entities.Entities;
using BlackJack.ViewModels.Views.HistoryViews;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackJack.Entities.Enums;
using AutoMapper;
using BlackJack.ViewModels.Enums;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;

namespace BlackJack.BusinessLogic.Services

{
    public class HistoryService : IHistoryService
    {
        private IGameRepository _gameRepository;
        private IRoundRepository _roundRepository;
        private IPlayerInGameRepository _playerInGameRepository;

        public HistoryService(IGameRepository gameRepository, IRoundRepository roundRepository, IPlayerInGameRepository playerInGameRepository)
        {
            _gameRepository = gameRepository;
            _roundRepository = roundRepository;
            _playerInGameRepository = playerInGameRepository;
        }

        public async Task<GetAllGamesHistoryView> GetAllGames()
        {
            List<Game> allGames = (await _gameRepository.GetAll())
                .Where(g => g.State == GameState.End)
                .ToList();

            var history = new GetAllGamesHistoryView();
            history.Games = Mapper.Map<List<Game>, List<GameGetAllGamesHistoryViewItem>>(allGames);
            return history;
        }

        public async Task<GameDetailsHistoryView> GameDetails(int gameId)
        {
            var newRound = new GameDetailsHistoryView();
            var players = new List<PlayerGameDetailsHistoryViewItem>();

            List<PlayerInGame> playersStates = (await _playerInGameRepository.GetPlayersByGameId(gameId))
             .ToList();

            List<Player> playersInGame = playersStates.Select(p => p.Player).ToList();

            List<Round> round = await _roundRepository.GetRoundsByGameId(gameId);

            foreach (var item in playersInGame)
            {
                List<Card> cards = round.Where(r => r.PlayerId == item.Id)
                   .Select(r => r.Card)
                   .ToList();

                PlayerGameDetailsHistoryViewItem player = Mapper.Map<Player, PlayerGameDetailsHistoryViewItem>(item);
                List<CardGameDetailsHistoryViewItem> cardsForPlayer = Mapper.Map<List<Card>, List<CardGameDetailsHistoryViewItem>>(cards);
                player.Cards = cardsForPlayer;
                player.State = (PlayerStateEnumView)playersStates.Where(p => p.Player.Id == item.Id).FirstOrDefault().State;
                player.TotalPoints = playersStates.Where(p => p.Player.Id == item.Id).First().TotalPoints;
                players.Add(player);
            }
            newRound.Players = players;
            newRound.GameId = gameId;
            return newRound;
        }
    }
}
