﻿namespace BlackJack.BusinessLogic.Enums
{
    public enum GetActionInGame
    {
        FirstRound = 0,
        NextRound = 1,
        DealerTakes = 2
    }
}
