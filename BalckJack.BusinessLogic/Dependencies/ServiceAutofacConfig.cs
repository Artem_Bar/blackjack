﻿using Autofac;
using BlackJack.BusinessLogic.Services;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Dependencies;

namespace BlackJack.BusinessLogic.Dependencies
{
    public static class ServiceAutofacConfig
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connection)
        {
            builder.RegisterType<GameService>().As<IGameService>();
            builder.RegisterType<HistoryService>().As<IHistoryService>();
            DataAccessAutofacConfig.ConfigureContainer(builder, connection);
        }
    }
}