﻿using BlackJack.Entities.Enums;
using Dapper.Contrib.Extensions;
namespace BlackJack.Entities.Entities
{
    [Table("PlayerInGame")]
    public class PlayerInGame : BaseEntity
    {
        public int GameId { get; set; }
        public int PlayerId { get; set; }
        public PlayerState State { get; set; }
        public int TotalPoints { get; set; }
        [Write(false)]
        public Game Game { get; set; }
        [Write(false)]
        public Player Player { get; set; }
    }
}
