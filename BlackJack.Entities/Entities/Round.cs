﻿using Dapper.Contrib.Extensions;
namespace BlackJack.Entities.Entities
{
    [Table("Round")]
    public class Round : BaseEntity
    {
        public int GameId { get; set; }
        public int PlayerId { get; set; }
        public int CardId { get; set; }
        [Write(false)]
        public Game Game { get; set; }
        [Write(false)]
        public Player Player { get; set; }
        [Write(false)]
        public Card Card { get; set; }
    }
}
