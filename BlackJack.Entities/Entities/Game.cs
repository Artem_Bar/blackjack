﻿using BlackJack.Entities.Enums;
using Dapper.Contrib.Extensions;
namespace BlackJack.Entities.Entities
{
    [Table("Game")]
    public class Game : BaseEntity
    {
        public GameState State { get; set; }
    }
}
