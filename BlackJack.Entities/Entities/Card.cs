﻿using BlackJack.Entities.Enums;
using Dapper.Contrib.Extensions;
namespace BlackJack.Entities.Entities
{
    [Table("Card")]
    public class Card : BaseEntity
    {
        public int Points { get; set; }
        public CardValue Value { get; set; }
        public CardSuit Suit { get; set; }
    }
}

