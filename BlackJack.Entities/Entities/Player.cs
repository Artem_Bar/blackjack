﻿using BlackJack.Entities.Enums;
using Dapper.Contrib.Extensions;
namespace BlackJack.Entities.Entities
{
    [Table("Player")]
    public class Player : BaseEntity
    {
        public string Name { get; set; }
        public PlayerRole Role { get; set; }
    }
}
