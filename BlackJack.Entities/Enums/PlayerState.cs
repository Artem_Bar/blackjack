﻿namespace BlackJack.Entities.Enums
{
    public enum PlayerState
    {
        InGame = 0,
        Win = 1,
        Loss = 2,
        Draw = 3,
        BlackJack = 4
    }
}
