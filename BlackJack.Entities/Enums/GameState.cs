﻿namespace BlackJack.Entities.Enums
{
    public enum GameState
    {
        Continue = 0,
        End = 1,
    }
}
