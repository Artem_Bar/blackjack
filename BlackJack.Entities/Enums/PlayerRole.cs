﻿namespace BlackJack.Entities.Enums
{
    public enum PlayerRole
    {
        Bot = 0,
        Player = 1,
        Dealer = 2,
    }
}
