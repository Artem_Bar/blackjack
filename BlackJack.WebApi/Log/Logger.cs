﻿using System;
using System.IO;

namespace BlackJack.WebApi.Log
{
    public class Logger
    {
        public static void WriteLine(string message)
        {
            string fullPath = @"C:\Users\Aunitex\source\Repos\gameblackjack\BlackJack.WebApi\Log\log.txt";
            FileInfo fileInfo = new FileInfo(fullPath);

            if (!fileInfo.Exists)
            {
                File.Create(fullPath).Close();
            }

            using (StreamWriter sw = new StreamWriter(fullPath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(String.Format("{0,-23} {1}", DateTime.Now.ToString() + ":", message));
            }
        }
    }
}