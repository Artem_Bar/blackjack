﻿using Autofac;
using Autofac.Integration.WebApi;
using BlackJack.BusinessLogic.Dependencies;
using BlackJack.BusinessLogic.Map;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using System.Reflection;
using System.Web.Http;

[assembly: OwinStartup(typeof(BlackJack.WebApi.Startup))]

namespace BlackJack.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            HttpConfiguration config = new HttpConfiguration();

            var builder = new ContainerBuilder();
            var configuration = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            ServiceAutofacConfig.ConfigureContainer(builder, ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString);
            var container = builder.Build();
            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(config);

            WebApiConfig.Register(config);
            appBuilder.UseWebApi(config);
            AutoMapperConfig.Initialize();

        }
    }
}
