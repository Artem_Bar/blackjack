﻿using System.Web.Http;
using System.Threading.Tasks;
using BlackJack.ViewModels.Views.GameViews;
using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Services.Interfaces;
using System;

namespace BlackJack.WebApi.Controllers
{
    [RoutePrefix("game")]
    public class GameController : ApiController
    {
        private IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        [Route("startGame")]
        public async Task<IHttpActionResult> StartGame()
        {
            try
            {
                StartGameGameView data = await _gameService.StartGame();
                return Ok(data);
            }
            catch(Exception ex)
            {
                Log.Logger.WriteLine(ex.Message);
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("createGame")]
        public async Task<IHttpActionResult> CreateGame(RequestCreateGameGameView data)
        {
            try
            {
                ResponseCreateGameGameView result = await _gameService.CreateGame(data);
                return Ok(result);
            }
            catch(Exception ex)
            {
                Log.Logger.WriteLine(ex.Message);
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("currentGame/{gameId}/{param}")]
        public async Task<IHttpActionResult> CurrentGame(int gameId, GetActionInGame param)
        {
            try
            {
                CurrentGameGameView round = await _gameService.CurrentGame(gameId, param);
                return Ok(round);
            }
            catch(Exception ex)
            {
                Log.Logger.WriteLine(ex.Message);
                return InternalServerError();
            }
        }
    }
}
