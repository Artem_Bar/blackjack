﻿using System.Web.Http;
using System.Threading.Tasks;
using BlackJack.BusinessLogic.Services.Interfaces;
using System;

namespace BlackJack.WebApi.Controllers
{
    [RoutePrefix("history")]
    public class HistoryController : ApiController
    {
        private IHistoryService _historyService;

        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpGet]
        [Route("getAllGames")]
        public async Task<IHttpActionResult> GetAllGames()
        {
            try
            {
                var data = await _historyService.GetAllGames();
                return Ok(data);
            }
            catch (Exception ex)
            {
                Log.Logger.WriteLine(ex.Message);
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("gameDetails/{gameId}")]
        public async Task<IHttpActionResult> GameDetails(int gameId)
        {
            try
            {
                var createGame = await _historyService.GameDetails(gameId);
                return Ok(createGame);
            }
            catch (Exception ex)
            {
                Log.Logger.WriteLine(ex.Message);
                return InternalServerError();
            }
        }

    }
}
