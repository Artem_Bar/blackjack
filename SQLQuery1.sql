use master 
go
create database BlackJack
go
use BlackJack
go

create table Card(
Id int primary key identity,
[Value] int not null,
Suit int not null,
Points int not null,
CreationDate Datetime,
)
go
create table Player(
Id int primary key identity,
[Name] nvarchar(15) not null,
[Role] int not null,
CreationDate Datetime
)

go
create table Game(
Id int primary key identity,
CreationDate Datetime,
[State] int not null
)

go
create table Round(
Id int primary key identity,
GameId int references Game(Id)on delete cascade on update cascade,
PlayerId int references Player(Id)on delete cascade on update cascade,
CardId int references Card(Id)on delete cascade on update cascade,
CreationDate Datetime
)

go
create table PlayerInGame(
Id int primary key identity,
GameId int not null,
PlayerId int not null,
[State] int not null,
TotalPoints int,
CreationDate Datetime
)

delete from Game DBCC CHECKIDENT (Game, RESEED, 0)
delete from Round DBCC CHECKIDENT (Round, RESEED, 0)
delete from Player DBCC CHECKIDENT (Player, RESEED, 0)
delete from PlayerInGame DBCC CHECKIDENT (History, RESEED, 0)
delete from Card DBCC CHECKIDENT (Card, RESEED, 0)


select*from Player
select*from Card
select*from Round
select*from PlayerInGame
select * from Game


